﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using Renci.SshNet.Common;

namespace RejestrZabytkówMHKI
{
    public partial class addCategory : Form
    {
        string kategoria,nowa_kategoria,skrot, cmd,rodzic;
        bool dodajRown, dodajPodKat;
        private string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();
        MySqlConnection link;
        
        //MySqlCommand polecenie = new MySqlCommand();
        public addCategory(string cat,string parent)
        {
            kategoria = cat;
            rodzic = parent;
            InitializeComponent();
            eqCatLabel.Text = kategoria;
            subCatLabel.Text = kategoria;
            Console.WriteLine(rodzic);
        }
  
        private bool isChildOfRoot(string skrot) 
        {
            string pytanie = "select d2.skr from mptt_al as d2, mptt_al as d1 where d2.id = d1.rodzic and d1.skr='" + skrot + "'";
            string odpowiedz;
            /*
            PasswordConnectionInfo info = new PasswordConnectionInfo("155.158.108.213", "mhki", "mhki");
            info.Timeout = TimeSpan.FromHours(2d);
            SshClient klient = new SshClient(info);
            try
            {
                klient.Connect();
            }
            catch (SshException e)
            {
                MessageBox.Show(e.Message + "\n" + e.Data);
            }

            if (!klient.IsConnected)
            {
                MessageBox.Show("Nie można nawiązać połączenia SSH");
            }
            var portFwd = new ForwardedPortLocal("127.0.0.1", 10000, "localhost", 3306);
            klient.AddForwardedPort(portFwd);
            portFwd.Start();
            if (portFwd.IsStarted)
            {
                //MessageBox.Show("Tunel SSH aktywny");
            }
            else
            {
                MessageBox.Show("Nie udało się otworzyć tunelu SSH");
            }*/
            MySqlConnection link = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;database=mhki");
            MySqlCommand polecenie = new MySqlCommand();
            polecenie.CommandText = pytanie;
            link.Open();
            odpowiedz = polecenie.ExecuteScalar().ToString();
            link.Close();
            //portFwd.Stop();
            //klient.Disconnect();
            if (String.Equals(odpowiedz, "MHKI")) return true;
            else return false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            nowa_kategoria = newCatName.Text;
            skrot = Abbr.Text;
            Console.WriteLine(nowa_kategoria);
            if (!String.IsNullOrEmpty(nowa_kategoria) && !String.IsNullOrEmpty(skrot))
            {
                try
                {
                    link = new MySqlConnection(danepolaczenia);
                    link.Open();
                    //wspólne
                    cmd = "LOCK TABLE rejestr WRITE";
                    MySqlCommand polecenie = new MySqlCommand(cmd, link);
                    polecenie.ExecuteNonQuery();
                    polecenie.Dispose();
                    if (dodajRown)
                    {
                        polecenie.CommandText = "SELECT lewy FROM rejestr WHERE skr = '" + rodzic + "' ";
                        int myleft = Convert.ToUInt16(polecenie.ExecuteScalar());
                        polecenie.Dispose();
                        polecenie.CommandText = "UPDATE rejestr SET prawy = prawy + 2 WHERE prawy > " + myleft;
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        polecenie.CommandText = "UPDATE rejestr SET lewy = lewy + 2 WHERE lewy >  " + myleft;
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        polecenie.CommandText = "INSERT INTO rejestr(nazwa, skr, lewy, prawy) VALUES('" + nowa_kategoria + "','" + nowa_kategoria + "', " + (myleft + 1) + "," + (myleft + 2) + ") ";
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        polecenie.CommandText = "UNLOCK TABLES";
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        if (MessageBox.Show("Pomyślnie dodano kategorię " + nowa_kategoria + " jako kategorię równorzędną kategorii" + kategoria, "Potwierdzenie", MessageBoxButtons.OK) == DialogResult.OK) this.Close();
                    }

                    if (dodajPodKat)
                    {
                        polecenie.CommandText = "SELECT lewy FROM rejestr WHERE skr = '" + kategoria + "' ";
                        int myleft = Convert.ToUInt16(polecenie.ExecuteScalar());
                        polecenie.Dispose();
                        polecenie.CommandText = "UPDATE rejestr SET prawy = prawy + 2 WHERE prawy > " + myleft;
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        polecenie.CommandText = "UPDATE rejestr SET lewy = lewy + 2 WHERE lewy > " + myleft;
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        polecenie.CommandText = "INSERT INTO rejestr(nazwa, skr, lewy, prawy) VALUES('" + nowa_kategoria + "','" + nowa_kategoria + "', " + (myleft + 1) + "," + (myleft + 2) + ") ";
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        polecenie.CommandText = "UNLOCK TABLES";
                        polecenie.ExecuteNonQuery();
                        polecenie.Dispose();
                        if (MessageBox.Show("Pomyślnie dodano kategorię " + nowa_kategoria + " jako podkategorię kategorii " + kategoria, "Potwierdzenie", MessageBoxButtons.OK) == DialogResult.OK) this.Close();
                    }

                    Console.WriteLine(cmd);
                    link.Close();
                }

                catch (Exception exc)
                {
                    MessageBox.Show("Wyjątek: " + exc.Message + "\n i jego źródło: " + exc.GetBaseException());
                }

            }
            else 
            {
                MessageBox.Show("Proszę wypełnić zarówno pole nazwy jak i pole skrótu dodawanej kategorii", "Ostrzeżenie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
            }
        }

        private void eqCat_CheckedChanged(object sender, EventArgs e)
        {
            if (eqCat.Checked)
            {
                //cmd = makeQueryAddCat(newCatName.Text);
                dodajRown = true;
                //Console.WriteLine(polecenie.CommandText);
            }
        }

        private void addCategory_Load(object sender, EventArgs e)
        {

        }

        private void subCat_CheckedChanged(object sender, EventArgs e)
        {
            if (subCat.Checked)
            {
                dodajPodKat = true;
                //Console.WriteLine(polecenie.CommandText);
            } 
        }
    }
}
