﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using WindowsFormsAero.TaskDialog;

namespace RejestrZabytkówMHKI
{
    public partial class PrzeniesEksponat : Form
    {
        private string expid,catname,nowakategoria;
        private string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();

        public PrzeniesEksponat(string identyfikator,string kategoria)
        {
            expid = identyfikator;
            catname = kategoria;
            Console.WriteLine(catname);
            
            InitializeComponent();
            CurrentCategory.Text = catname;
            fillComboWithCatNames();
        }

        private void fillComboWithCatNames() 
        {
            MySqlConnection link = new MySqlConnection(danepolaczenia);
            MySqlCommand fillcmd = new MySqlCommand("SELECT nazwa FROM rejestr WHERE skr<>'MHKI'", link);
            MySqlDataAdapter fillDA = new MySqlDataAdapter(fillcmd);
            DataTable fillDT = new DataTable();
            fillDA.Fill(fillDT);
            CatNameCombo.DisplayMember = fillDT.Columns[0].ToString();
            CatNameCombo.ValueMember = fillDT.Columns[0].ToString();
            CatNameCombo.DataSource = fillDT.DefaultView;
        }

        private void SaveModBtn_Click(object sender, EventArgs e)
        {
            MySqlConnection link = new MySqlConnection(danepolaczenia);
            string savecmd = "UPDATE eksponaty SET kategoria='" + nowakategoria + "' WHERE identyfikator='" + expid + "'";
            MySqlCommand save = new MySqlCommand(savecmd, link);
            try 
            {
                save.ExecuteNonQuery();
            }
            catch(MySqlException ex) 
            {
                TaskDialog.Show("Treść błędu: " + ex.Message, "Błąd!", "Błąd aktualizacji danych eksponatu w bazie danych", TaskDialogButton.Close, TaskDialogIcon.SecurityError);
            }
        }

        private void CatNameCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            nowakategoria=CatNameCombo.SelectedValue.ToString();
        }

    }
}
