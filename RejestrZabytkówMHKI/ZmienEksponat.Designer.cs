﻿namespace RejestrZabytkówMHKI
{
    partial class ZmienEksponat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NodeValue = new System.Windows.Forms.Label();
            this.NodeName = new System.Windows.Forms.Label();
            this.Path = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.NIDTab = new System.Windows.Forms.TabPage();
            this.Panel_NID = new System.Windows.Forms.Panel();
            this.ZalacznikiLabel = new System.Windows.Forms.Label();
            this.AdnLabel = new System.Windows.Forms.Label();
            this.UwagiLabel = new System.Windows.Forms.Label();
            this.ZrodlaLabel = new System.Windows.Forms.Label();
            this.BibliografiaLabel = new System.Windows.Forms.Label();
            this.AktaLabel = new System.Windows.Forms.Label();
            this.StanLabel = new System.Windows.Forms.Label();
            this.RemontyLabel = new System.Windows.Forms.Label();
            this.PrzeznLabel = new System.Windows.Forms.Label();
            this.CurUseLabel = new System.Windows.Forms.Label();
            this.OpisLabel = new System.Windows.Forms.Label();
            this.HistoriaLabel = new System.Windows.Forms.Label();
            this.OchronaLabel = new System.Windows.Forms.Label();
            this.UdostLabel = new System.Windows.Forms.Label();
            this.MscePcyLabel = new System.Windows.Forms.Label();
            this.UserLabel = new System.Windows.Forms.Label();
            this.OwnerLabel = new System.Windows.Forms.Label();
            this.IloscLabel = new System.Windows.Forms.Label();
            this.CiezarLabel = new System.Windows.Forms.Label();
            this.WymiaryGB = new System.Windows.Forms.GroupBox();
            this.SzerLabel = new System.Windows.Forms.Label();
            this.WysLabel = new System.Windows.Forms.Label();
            this.DlugoscLabel = new System.Windows.Forms.Label();
            this.DimSzerTB = new System.Windows.Forms.TextBox();
            this.DimWysTB = new System.Windows.Forms.TextBox();
            this.DimDlTB = new System.Windows.Forms.TextBox();
            this.DaneFrmLabel = new System.Windows.Forms.Label();
            this.MatPodstLabel = new System.Windows.Forms.Label();
            this.RokProdLabel = new System.Windows.Forms.Label();
            this.ZalacznikiTB = new System.Windows.Forms.TextBox();
            this.OprKartyGB = new System.Windows.Forms.GroupBox();
            this.Opr_foto_Label = new System.Windows.Forms.Label();
            this.Opr_plany_Label = new System.Windows.Forms.Label();
            this.Opr_TekstLabel = new System.Windows.Forms.Label();
            this.FotoOprTB = new System.Windows.Forms.TextBox();
            this.PlanyOprTB = new System.Windows.Forms.TextBox();
            this.OprTekstTB = new System.Windows.Forms.TextBox();
            this.AdnInspTB = new System.Windows.Forms.TextBox();
            this.UwagiTB = new System.Windows.Forms.TextBox();
            this.ZrodlaIkoTB = new System.Windows.Forms.TextBox();
            this.BibGrfTB = new System.Windows.Forms.TextBox();
            this.AktaArchTB = new System.Windows.Forms.TextBox();
            this.StanTB = new System.Windows.Forms.TextBox();
            this.RemontyTB = new System.Windows.Forms.TextBox();
            this.UzObecneTB = new System.Windows.Forms.TextBox();
            this.PrzeznPierwTB = new System.Windows.Forms.TextBox();
            this.OpisTB = new System.Windows.Forms.TextBox();
            this.HistoriaTB = new System.Windows.Forms.TextBox();
            this.FormyOchrTB = new System.Windows.Forms.TextBox();
            this.UdstTB = new System.Windows.Forms.TextBox();
            this.MscePcyTB = new System.Windows.Forms.TextBox();
            this.UzytkownikTB = new System.Windows.Forms.TextBox();
            this.WlascicielTB = new System.Windows.Forms.TextBox();
            this.AdresGB = new System.Windows.Forms.GroupBox();
            this.WojewodztwoCB = new System.Windows.Forms.ComboBox();
            this.PowiatTB = new System.Windows.Forms.TextBox();
            this.GminaTB = new System.Windows.Forms.TextBox();
            this.MiejscowoscTB = new System.Windows.Forms.TextBox();
            this.ZdjecieDodajBtn = new System.Windows.Forms.Button();
            this.ZdjecieSciezkaTB = new System.Windows.Forms.TextBox();
            this.IloscTB = new System.Windows.Forms.TextBox();
            this.CiezarTB = new System.Windows.Forms.TextBox();
            this.DaneFrmTB = new System.Windows.Forms.TextBox();
            this.YearUpDown = new System.Windows.Forms.NumericUpDown();
            this.MatGrafPB = new System.Windows.Forms.PictureBox();
            this.MatPodstTB = new System.Windows.Forms.TextBox();
            this.TechSpecTab = new System.Windows.Forms.TabPage();
            this.NazwaTB = new System.Windows.Forms.TextBox();
            this.NazwaLabel = new System.Windows.Forms.Label();
            this.ExpIDLabel = new System.Windows.Forms.Label();
            this.ExpIDTB = new System.Windows.Forms.TextBox();
            this.ZapiszDaneBtn = new System.Windows.Forms.Button();
            this.QRCodeGen = new System.Windows.Forms.Button();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.StanCombo = new System.Windows.Forms.ComboBox();
            this.StatusTab = new System.Windows.Forms.TabPage();
            this.AdresGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.WojewodztwoWyp_Label = new System.Windows.Forms.Label();
            this.WojewodztwoCombo = new System.Windows.Forms.ComboBox();
            this.GminaWyp = new System.Windows.Forms.TextBox();
            this.GminaWyp_Label = new System.Windows.Forms.Label();
            this.PowiatWyp = new System.Windows.Forms.TextBox();
            this.PowiatWyp_Label = new System.Windows.Forms.Label();
            this.MiastoWyp = new System.Windows.Forms.TextBox();
            this.MiastoWyp_Label = new System.Windows.Forms.Label();
            this.DataZwrotuLabel = new System.Windows.Forms.Label();
            this.DataZwrotu = new System.Windows.Forms.DateTimePicker();
            this.WypKomu = new System.Windows.Forms.TextBox();
            this.WypKomu_Label = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.NIDTab.SuspendLayout();
            this.Panel_NID.SuspendLayout();
            this.WymiaryGB.SuspendLayout();
            this.OprKartyGB.SuspendLayout();
            this.AdresGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YearUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MatGrafPB)).BeginInit();
            this.StatusTab.SuspendLayout();
            this.AdresGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NodeValue
            // 
            this.NodeValue.AutoSize = true;
            this.NodeValue.Location = new System.Drawing.Point(640, 72);
            this.NodeValue.Name = "NodeValue";
            this.NodeValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.NodeValue.Size = new System.Drawing.Size(0, 13);
            this.NodeValue.TabIndex = 0;
            // 
            // NodeName
            // 
            this.NodeName.AutoSize = true;
            this.NodeName.Location = new System.Drawing.Point(632, 184);
            this.NodeName.Name = "NodeName";
            this.NodeName.Size = new System.Drawing.Size(0, 13);
            this.NodeName.TabIndex = 1;
            // 
            // Path
            // 
            this.Path.AutoSize = true;
            this.Path.Location = new System.Drawing.Point(640, 272);
            this.Path.Name = "Path";
            this.Path.Size = new System.Drawing.Size(0, 13);
            this.Path.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.NIDTab);
            this.tabControl1.Controls.Add(this.TechSpecTab);
            this.tabControl1.Controls.Add(this.StatusTab);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 301);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(898, 304);
            this.tabControl1.TabIndex = 3;
            // 
            // NIDTab
            // 
            this.NIDTab.Controls.Add(this.Panel_NID);
            this.NIDTab.Location = new System.Drawing.Point(4, 22);
            this.NIDTab.Name = "NIDTab";
            this.NIDTab.Padding = new System.Windows.Forms.Padding(3);
            this.NIDTab.Size = new System.Drawing.Size(890, 278);
            this.NIDTab.TabIndex = 0;
            this.NIDTab.Text = "Karta ewidencyjna NID";
            this.NIDTab.UseVisualStyleBackColor = true;
            this.NIDTab.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // Panel_NID
            // 
            this.Panel_NID.AutoScroll = true;
            this.Panel_NID.AutoSize = true;
            this.Panel_NID.Controls.Add(this.ZalacznikiLabel);
            this.Panel_NID.Controls.Add(this.AdnLabel);
            this.Panel_NID.Controls.Add(this.UwagiLabel);
            this.Panel_NID.Controls.Add(this.ZrodlaLabel);
            this.Panel_NID.Controls.Add(this.BibliografiaLabel);
            this.Panel_NID.Controls.Add(this.AktaLabel);
            this.Panel_NID.Controls.Add(this.StanLabel);
            this.Panel_NID.Controls.Add(this.RemontyLabel);
            this.Panel_NID.Controls.Add(this.PrzeznLabel);
            this.Panel_NID.Controls.Add(this.CurUseLabel);
            this.Panel_NID.Controls.Add(this.OpisLabel);
            this.Panel_NID.Controls.Add(this.HistoriaLabel);
            this.Panel_NID.Controls.Add(this.OchronaLabel);
            this.Panel_NID.Controls.Add(this.UdostLabel);
            this.Panel_NID.Controls.Add(this.MscePcyLabel);
            this.Panel_NID.Controls.Add(this.UserLabel);
            this.Panel_NID.Controls.Add(this.OwnerLabel);
            this.Panel_NID.Controls.Add(this.IloscLabel);
            this.Panel_NID.Controls.Add(this.CiezarLabel);
            this.Panel_NID.Controls.Add(this.WymiaryGB);
            this.Panel_NID.Controls.Add(this.DaneFrmLabel);
            this.Panel_NID.Controls.Add(this.MatPodstLabel);
            this.Panel_NID.Controls.Add(this.RokProdLabel);
            this.Panel_NID.Controls.Add(this.ZalacznikiTB);
            this.Panel_NID.Controls.Add(this.OprKartyGB);
            this.Panel_NID.Controls.Add(this.AdnInspTB);
            this.Panel_NID.Controls.Add(this.UwagiTB);
            this.Panel_NID.Controls.Add(this.ZrodlaIkoTB);
            this.Panel_NID.Controls.Add(this.BibGrfTB);
            this.Panel_NID.Controls.Add(this.AktaArchTB);
            this.Panel_NID.Controls.Add(this.StanTB);
            this.Panel_NID.Controls.Add(this.RemontyTB);
            this.Panel_NID.Controls.Add(this.UzObecneTB);
            this.Panel_NID.Controls.Add(this.PrzeznPierwTB);
            this.Panel_NID.Controls.Add(this.OpisTB);
            this.Panel_NID.Controls.Add(this.HistoriaTB);
            this.Panel_NID.Controls.Add(this.FormyOchrTB);
            this.Panel_NID.Controls.Add(this.UdstTB);
            this.Panel_NID.Controls.Add(this.MscePcyTB);
            this.Panel_NID.Controls.Add(this.UzytkownikTB);
            this.Panel_NID.Controls.Add(this.WlascicielTB);
            this.Panel_NID.Controls.Add(this.AdresGB);
            this.Panel_NID.Controls.Add(this.ZdjecieDodajBtn);
            this.Panel_NID.Controls.Add(this.ZdjecieSciezkaTB);
            this.Panel_NID.Controls.Add(this.IloscTB);
            this.Panel_NID.Controls.Add(this.CiezarTB);
            this.Panel_NID.Controls.Add(this.DaneFrmTB);
            this.Panel_NID.Controls.Add(this.YearUpDown);
            this.Panel_NID.Controls.Add(this.MatGrafPB);
            this.Panel_NID.Controls.Add(this.MatPodstTB);
            this.Panel_NID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_NID.Location = new System.Drawing.Point(3, 3);
            this.Panel_NID.Margin = new System.Windows.Forms.Padding(0);
            this.Panel_NID.Name = "Panel_NID";
            this.Panel_NID.Size = new System.Drawing.Size(884, 272);
            this.Panel_NID.TabIndex = 0;
            // 
            // ZalacznikiLabel
            // 
            this.ZalacznikiLabel.AutoSize = true;
            this.ZalacznikiLabel.Location = new System.Drawing.Point(63, 1684);
            this.ZalacznikiLabel.Name = "ZalacznikiLabel";
            this.ZalacznikiLabel.Size = new System.Drawing.Size(57, 13);
            this.ZalacznikiLabel.TabIndex = 56;
            this.ZalacznikiLabel.Text = "Załączniki";
            // 
            // AdnLabel
            // 
            this.AdnLabel.AutoSize = true;
            this.AdnLabel.Location = new System.Drawing.Point(44, 1491);
            this.AdnLabel.Name = "AdnLabel";
            this.AdnLabel.Size = new System.Drawing.Size(429, 13);
            this.AdnLabel.TabIndex = 55;
            this.AdnLabel.Text = "Adnotacje o inspekcjach, informacje o zmianach (daty, imiona i nazwiska wypełniaj" +
    "ących)";
            // 
            // UwagiLabel
            // 
            this.UwagiLabel.AutoSize = true;
            this.UwagiLabel.Location = new System.Drawing.Point(485, 1273);
            this.UwagiLabel.Name = "UwagiLabel";
            this.UwagiLabel.Size = new System.Drawing.Size(37, 13);
            this.UwagiLabel.TabIndex = 54;
            this.UwagiLabel.Text = "Uwagi";
            // 
            // ZrodlaLabel
            // 
            this.ZrodlaLabel.AutoSize = true;
            this.ZrodlaLabel.Location = new System.Drawing.Point(60, 1276);
            this.ZrodlaLabel.Name = "ZrodlaLabel";
            this.ZrodlaLabel.Size = new System.Drawing.Size(271, 13);
            this.ZrodlaLabel.TabIndex = 53;
            this.ZrodlaLabel.Text = "Źródła ikonograficzne (rodzaj, miejsce przechowywania)";
            // 
            // BibliografiaLabel
            // 
            this.BibliografiaLabel.AutoSize = true;
            this.BibliografiaLabel.Location = new System.Drawing.Point(497, 1080);
            this.BibliografiaLabel.Name = "BibliografiaLabel";
            this.BibliografiaLabel.Size = new System.Drawing.Size(58, 13);
            this.BibliografiaLabel.TabIndex = 52;
            this.BibliografiaLabel.Text = "Bibliografia";
            // 
            // AktaLabel
            // 
            this.AktaLabel.AutoSize = true;
            this.AktaLabel.Location = new System.Drawing.Point(22, 1081);
            this.AktaLabel.Name = "AktaLabel";
            this.AktaLabel.Size = new System.Drawing.Size(298, 13);
            this.AktaLabel.TabIndex = 51;
            this.AktaLabel.Text = "Akta archiwalne (rodzaj akt, numer i miejsce przechowywania)";
            // 
            // StanLabel
            // 
            this.StanLabel.AutoSize = true;
            this.StanLabel.Location = new System.Drawing.Point(476, 897);
            this.StanLabel.Name = "StanLabel";
            this.StanLabel.Size = new System.Drawing.Size(217, 13);
            this.StanLabel.TabIndex = 50;
            this.StanLabel.Text = "Stan zachowania i potrzeby konserwatorskie";
            // 
            // RemontyLabel
            // 
            this.RemontyLabel.AutoSize = true;
            this.RemontyLabel.Location = new System.Drawing.Point(15, 897);
            this.RemontyLabel.Name = "RemontyLabel";
            this.RemontyLabel.Size = new System.Drawing.Size(224, 13);
            this.RemontyLabel.TabIndex = 49;
            this.RemontyLabel.Text = "Remonty, zmiany konstrukcyjne, modernizacje";
            // 
            // PrzeznLabel
            // 
            this.PrzeznLabel.AutoSize = true;
            this.PrzeznLabel.Location = new System.Drawing.Point(153, 823);
            this.PrzeznLabel.Name = "PrzeznLabel";
            this.PrzeznLabel.Size = new System.Drawing.Size(125, 13);
            this.PrzeznLabel.TabIndex = 48;
            this.PrzeznLabel.Text = "Przeznaczenie pierwotne";
            // 
            // CurUseLabel
            // 
            this.CurUseLabel.AutoSize = true;
            this.CurUseLabel.Location = new System.Drawing.Point(140, 856);
            this.CurUseLabel.Name = "CurUseLabel";
            this.CurUseLabel.Size = new System.Drawing.Size(107, 13);
            this.CurUseLabel.TabIndex = 47;
            this.CurUseLabel.Text = "Użytkowanie obecne";
            // 
            // OpisLabel
            // 
            this.OpisLabel.AutoSize = true;
            this.OpisLabel.Location = new System.Drawing.Point(427, 629);
            this.OpisLabel.Name = "OpisLabel";
            this.OpisLabel.Size = new System.Drawing.Size(166, 13);
            this.OpisLabel.TabIndex = 46;
            this.OpisLabel.Text = "Opis i charakterystyka techniczna";
            // 
            // HistoriaLabel
            // 
            this.HistoriaLabel.AutoSize = true;
            this.HistoriaLabel.Location = new System.Drawing.Point(18, 629);
            this.HistoriaLabel.Name = "HistoriaLabel";
            this.HistoriaLabel.Size = new System.Drawing.Size(80, 13);
            this.HistoriaLabel.TabIndex = 45;
            this.HistoriaLabel.Text = "Historia obiektu";
            // 
            // OchronaLabel
            // 
            this.OchronaLabel.AutoSize = true;
            this.OchronaLabel.Location = new System.Drawing.Point(49, 571);
            this.OchronaLabel.Name = "OchronaLabel";
            this.OchronaLabel.Size = new System.Drawing.Size(76, 13);
            this.OchronaLabel.TabIndex = 44;
            this.OchronaLabel.Text = "Formy ochrony";
            // 
            // UdostLabel
            // 
            this.UdostLabel.AutoSize = true;
            this.UdostLabel.Location = new System.Drawing.Point(46, 518);
            this.UdostLabel.Name = "UdostLabel";
            this.UdostLabel.Size = new System.Drawing.Size(75, 13);
            this.UdostLabel.TabIndex = 43;
            this.UdostLabel.Text = "Udostępnianie";
            // 
            // MscePcyLabel
            // 
            this.MscePcyLabel.AutoSize = true;
            this.MscePcyLabel.Location = new System.Drawing.Point(426, 463);
            this.MscePcyLabel.Name = "MscePcyLabel";
            this.MscePcyLabel.Size = new System.Drawing.Size(160, 13);
            this.MscePcyLabel.TabIndex = 42;
            this.MscePcyLabel.Text = "Miejsce pracy (przechowywania)";
            // 
            // UserLabel
            // 
            this.UserLabel.AutoSize = true;
            this.UserLabel.Location = new System.Drawing.Point(463, 396);
            this.UserLabel.Name = "UserLabel";
            this.UserLabel.Size = new System.Drawing.Size(119, 13);
            this.UserLabel.TabIndex = 41;
            this.UserLabel.Text = "Użytkownik i jego adres";
            // 
            // OwnerLabel
            // 
            this.OwnerLabel.AutoSize = true;
            this.OwnerLabel.Location = new System.Drawing.Point(460, 280);
            this.OwnerLabel.Name = "OwnerLabel";
            this.OwnerLabel.Size = new System.Drawing.Size(114, 13);
            this.OwnerLabel.TabIndex = 40;
            this.OwnerLabel.Text = "Właściciel i jego adres";
            // 
            // IloscLabel
            // 
            this.IloscLabel.AutoSize = true;
            this.IloscLabel.Location = new System.Drawing.Point(328, 233);
            this.IloscLabel.Name = "IloscLabel";
            this.IloscLabel.Size = new System.Drawing.Size(29, 13);
            this.IloscLabel.TabIndex = 39;
            this.IloscLabel.Text = "Ilość";
            // 
            // CiezarLabel
            // 
            this.CiezarLabel.AutoSize = true;
            this.CiezarLabel.Location = new System.Drawing.Point(328, 190);
            this.CiezarLabel.Name = "CiezarLabel";
            this.CiezarLabel.Size = new System.Drawing.Size(51, 13);
            this.CiezarLabel.TabIndex = 38;
            this.CiezarLabel.Text = "Ciężar [g]";
            // 
            // WymiaryGB
            // 
            this.WymiaryGB.Controls.Add(this.SzerLabel);
            this.WymiaryGB.Controls.Add(this.WysLabel);
            this.WymiaryGB.Controls.Add(this.DlugoscLabel);
            this.WymiaryGB.Controls.Add(this.DimSzerTB);
            this.WymiaryGB.Controls.Add(this.DimWysTB);
            this.WymiaryGB.Controls.Add(this.DimDlTB);
            this.WymiaryGB.Location = new System.Drawing.Point(16, 168);
            this.WymiaryGB.Name = "WymiaryGB";
            this.WymiaryGB.Size = new System.Drawing.Size(200, 100);
            this.WymiaryGB.TabIndex = 37;
            this.WymiaryGB.TabStop = false;
            this.WymiaryGB.Text = "Wymiary [mm]";
            // 
            // SzerLabel
            // 
            this.SzerLabel.AutoSize = true;
            this.SzerLabel.Location = new System.Drawing.Point(12, 74);
            this.SzerLabel.Name = "SzerLabel";
            this.SzerLabel.Size = new System.Drawing.Size(57, 13);
            this.SzerLabel.TabIndex = 10;
            this.SzerLabel.Text = "Szerokość";
            // 
            // WysLabel
            // 
            this.WysLabel.AutoSize = true;
            this.WysLabel.Location = new System.Drawing.Point(9, 48);
            this.WysLabel.Name = "WysLabel";
            this.WysLabel.Size = new System.Drawing.Size(57, 13);
            this.WysLabel.TabIndex = 9;
            this.WysLabel.Text = "Wysokość";
            // 
            // DlugoscLabel
            // 
            this.DlugoscLabel.AutoSize = true;
            this.DlugoscLabel.Location = new System.Drawing.Point(6, 29);
            this.DlugoscLabel.Name = "DlugoscLabel";
            this.DlugoscLabel.Size = new System.Drawing.Size(48, 13);
            this.DlugoscLabel.TabIndex = 8;
            this.DlugoscLabel.Text = "Długość";
            // 
            // DimSzerTB
            // 
            this.DimSzerTB.Location = new System.Drawing.Point(162, 74);
            this.DimSzerTB.Name = "DimSzerTB";
            this.DimSzerTB.Size = new System.Drawing.Size(32, 20);
            this.DimSzerTB.TabIndex = 6;
            // 
            // DimWysTB
            // 
            this.DimWysTB.Location = new System.Drawing.Point(148, 48);
            this.DimWysTB.Name = "DimWysTB";
            this.DimWysTB.Size = new System.Drawing.Size(40, 20);
            this.DimWysTB.TabIndex = 5;
            // 
            // DimDlTB
            // 
            this.DimDlTB.Location = new System.Drawing.Point(148, 22);
            this.DimDlTB.Name = "DimDlTB";
            this.DimDlTB.Size = new System.Drawing.Size(32, 20);
            this.DimDlTB.TabIndex = 7;
            // 
            // DaneFrmLabel
            // 
            this.DaneFrmLabel.AutoSize = true;
            this.DaneFrmLabel.Location = new System.Drawing.Point(67, 134);
            this.DaneFrmLabel.Name = "DaneFrmLabel";
            this.DaneFrmLabel.Size = new System.Drawing.Size(72, 13);
            this.DaneFrmLabel.TabIndex = 36;
            this.DaneFrmLabel.Text = "Dane firmowe";
            // 
            // MatPodstLabel
            // 
            this.MatPodstLabel.AutoSize = true;
            this.MatPodstLabel.Location = new System.Drawing.Point(64, 88);
            this.MatPodstLabel.Name = "MatPodstLabel";
            this.MatPodstLabel.Size = new System.Drawing.Size(108, 13);
            this.MatPodstLabel.TabIndex = 35;
            this.MatPodstLabel.Text = "Materiał podstawowy";
            // 
            // RokProdLabel
            // 
            this.RokProdLabel.AutoSize = true;
            this.RokProdLabel.Location = new System.Drawing.Point(61, 48);
            this.RokProdLabel.Name = "RokProdLabel";
            this.RokProdLabel.Size = new System.Drawing.Size(73, 13);
            this.RokProdLabel.TabIndex = 34;
            this.RokProdLabel.Text = "Rok produkcji";
            // 
            // ZalacznikiTB
            // 
            this.ZalacznikiTB.Location = new System.Drawing.Point(63, 1703);
            this.ZalacznikiTB.Multiline = true;
            this.ZalacznikiTB.Name = "ZalacznikiTB";
            this.ZalacznikiTB.Size = new System.Drawing.Size(407, 136);
            this.ZalacznikiTB.TabIndex = 32;
            // 
            // OprKartyGB
            // 
            this.OprKartyGB.Controls.Add(this.Opr_foto_Label);
            this.OprKartyGB.Controls.Add(this.Opr_plany_Label);
            this.OprKartyGB.Controls.Add(this.Opr_TekstLabel);
            this.OprKartyGB.Controls.Add(this.FotoOprTB);
            this.OprKartyGB.Controls.Add(this.PlanyOprTB);
            this.OprKartyGB.Controls.Add(this.OprTekstTB);
            this.OprKartyGB.Location = new System.Drawing.Point(498, 1491);
            this.OprKartyGB.Name = "OprKartyGB";
            this.OprKartyGB.Size = new System.Drawing.Size(407, 151);
            this.OprKartyGB.TabIndex = 31;
            this.OprKartyGB.TabStop = false;
            this.OprKartyGB.Text = "Opracowanie karty";
            // 
            // Opr_foto_Label
            // 
            this.Opr_foto_Label.AutoSize = true;
            this.Opr_foto_Label.Location = new System.Drawing.Point(77, 119);
            this.Opr_foto_Label.Name = "Opr_foto_Label";
            this.Opr_foto_Label.Size = new System.Drawing.Size(51, 13);
            this.Opr_foto_Label.TabIndex = 35;
            this.Opr_foto_Label.Text = "fotografie";
            // 
            // Opr_plany_Label
            // 
            this.Opr_plany_Label.AutoSize = true;
            this.Opr_plany_Label.Location = new System.Drawing.Point(77, 89);
            this.Opr_plany_Label.Name = "Opr_plany_Label";
            this.Opr_plany_Label.Size = new System.Drawing.Size(71, 13);
            this.Opr_plany_Label.TabIndex = 34;
            this.Opr_plany_Label.Text = "plany, rysunki";
            // 
            // Opr_TekstLabel
            // 
            this.Opr_TekstLabel.AutoSize = true;
            this.Opr_TekstLabel.Location = new System.Drawing.Point(77, 57);
            this.Opr_TekstLabel.Name = "Opr_TekstLabel";
            this.Opr_TekstLabel.Size = new System.Drawing.Size(30, 13);
            this.Opr_TekstLabel.TabIndex = 33;
            this.Opr_TekstLabel.Text = "tekst";
            // 
            // FotoOprTB
            // 
            this.FotoOprTB.Location = new System.Drawing.Point(193, 116);
            this.FotoOprTB.Name = "FotoOprTB";
            this.FotoOprTB.Size = new System.Drawing.Size(208, 20);
            this.FotoOprTB.TabIndex = 32;
            // 
            // PlanyOprTB
            // 
            this.PlanyOprTB.Location = new System.Drawing.Point(193, 86);
            this.PlanyOprTB.Name = "PlanyOprTB";
            this.PlanyOprTB.Size = new System.Drawing.Size(208, 20);
            this.PlanyOprTB.TabIndex = 31;
            // 
            // OprTekstTB
            // 
            this.OprTekstTB.Location = new System.Drawing.Point(193, 57);
            this.OprTekstTB.Name = "OprTekstTB";
            this.OprTekstTB.Size = new System.Drawing.Size(208, 20);
            this.OprTekstTB.TabIndex = 30;
            // 
            // AdnInspTB
            // 
            this.AdnInspTB.Location = new System.Drawing.Point(63, 1507);
            this.AdnInspTB.Multiline = true;
            this.AdnInspTB.Name = "AdnInspTB";
            this.AdnInspTB.Size = new System.Drawing.Size(407, 144);
            this.AdnInspTB.TabIndex = 29;
            // 
            // UwagiTB
            // 
            this.UwagiTB.Location = new System.Drawing.Point(485, 1292);
            this.UwagiTB.Multiline = true;
            this.UwagiTB.Name = "UwagiTB";
            this.UwagiTB.Size = new System.Drawing.Size(394, 156);
            this.UwagiTB.TabIndex = 28;
            // 
            // ZrodlaIkoTB
            // 
            this.ZrodlaIkoTB.Location = new System.Drawing.Point(63, 1292);
            this.ZrodlaIkoTB.Multiline = true;
            this.ZrodlaIkoTB.Name = "ZrodlaIkoTB";
            this.ZrodlaIkoTB.Size = new System.Drawing.Size(358, 167);
            this.ZrodlaIkoTB.TabIndex = 27;
            // 
            // BibGrfTB
            // 
            this.BibGrfTB.Location = new System.Drawing.Point(534, 1097);
            this.BibGrfTB.Multiline = true;
            this.BibGrfTB.Name = "BibGrfTB";
            this.BibGrfTB.Size = new System.Drawing.Size(345, 167);
            this.BibGrfTB.TabIndex = 26;
            // 
            // AktaArchTB
            // 
            this.AktaArchTB.Location = new System.Drawing.Point(143, 1097);
            this.AktaArchTB.Multiline = true;
            this.AktaArchTB.Name = "AktaArchTB";
            this.AktaArchTB.Size = new System.Drawing.Size(298, 159);
            this.AktaArchTB.TabIndex = 25;
            // 
            // StanTB
            // 
            this.StanTB.Location = new System.Drawing.Point(601, 884);
            this.StanTB.Multiline = true;
            this.StanTB.Name = "StanTB";
            this.StanTB.Size = new System.Drawing.Size(328, 172);
            this.StanTB.TabIndex = 24;
            // 
            // RemontyTB
            // 
            this.RemontyTB.Location = new System.Drawing.Point(143, 884);
            this.RemontyTB.Multiline = true;
            this.RemontyTB.Name = "RemontyTB";
            this.RemontyTB.Size = new System.Drawing.Size(298, 172);
            this.RemontyTB.TabIndex = 23;
            // 
            // UzObecneTB
            // 
            this.UzObecneTB.Location = new System.Drawing.Point(297, 856);
            this.UzObecneTB.Name = "UzObecneTB";
            this.UzObecneTB.Size = new System.Drawing.Size(213, 20);
            this.UzObecneTB.TabIndex = 22;
            // 
            // PrzeznPierwTB
            // 
            this.PrzeznPierwTB.Location = new System.Drawing.Point(307, 823);
            this.PrzeznPierwTB.Name = "PrzeznPierwTB";
            this.PrzeznPierwTB.Size = new System.Drawing.Size(205, 20);
            this.PrzeznPierwTB.TabIndex = 21;
            // 
            // OpisTB
            // 
            this.OpisTB.Location = new System.Drawing.Point(548, 607);
            this.OpisTB.Multiline = true;
            this.OpisTB.Name = "OpisTB";
            this.OpisTB.Size = new System.Drawing.Size(316, 183);
            this.OpisTB.TabIndex = 20;
            // 
            // HistoriaTB
            // 
            this.HistoriaTB.Location = new System.Drawing.Point(104, 606);
            this.HistoriaTB.Multiline = true;
            this.HistoriaTB.Name = "HistoriaTB";
            this.HistoriaTB.Size = new System.Drawing.Size(302, 184);
            this.HistoriaTB.TabIndex = 19;
            // 
            // FormyOchrTB
            // 
            this.FormyOchrTB.Location = new System.Drawing.Point(143, 565);
            this.FormyOchrTB.Name = "FormyOchrTB";
            this.FormyOchrTB.Size = new System.Drawing.Size(177, 20);
            this.FormyOchrTB.TabIndex = 18;
            // 
            // UdstTB
            // 
            this.UdstTB.Location = new System.Drawing.Point(143, 518);
            this.UdstTB.Name = "UdstTB";
            this.UdstTB.Size = new System.Drawing.Size(177, 20);
            this.UdstTB.TabIndex = 17;
            // 
            // MscePcyTB
            // 
            this.MscePcyTB.Location = new System.Drawing.Point(592, 456);
            this.MscePcyTB.Multiline = true;
            this.MscePcyTB.Name = "MscePcyTB";
            this.MscePcyTB.Size = new System.Drawing.Size(204, 103);
            this.MscePcyTB.TabIndex = 16;
            // 
            // UzytkownikTB
            // 
            this.UzytkownikTB.Location = new System.Drawing.Point(592, 370);
            this.UzytkownikTB.Multiline = true;
            this.UzytkownikTB.Name = "UzytkownikTB";
            this.UzytkownikTB.Size = new System.Drawing.Size(208, 71);
            this.UzytkownikTB.TabIndex = 15;
            // 
            // WlascicielTB
            // 
            this.WlascicielTB.Location = new System.Drawing.Point(592, 267);
            this.WlascicielTB.Multiline = true;
            this.WlascicielTB.Name = "WlascicielTB";
            this.WlascicielTB.Size = new System.Drawing.Size(204, 84);
            this.WlascicielTB.TabIndex = 14;
            // 
            // AdresGB
            // 
            this.AdresGB.Controls.Add(this.WojewodztwoCB);
            this.AdresGB.Controls.Add(this.PowiatTB);
            this.AdresGB.Controls.Add(this.GminaTB);
            this.AdresGB.Controls.Add(this.MiejscowoscTB);
            this.AdresGB.Location = new System.Drawing.Point(16, 331);
            this.AdresGB.Name = "AdresGB";
            this.AdresGB.Size = new System.Drawing.Size(304, 145);
            this.AdresGB.TabIndex = 13;
            this.AdresGB.TabStop = false;
            this.AdresGB.Text = "Adres";
            // 
            // WojewodztwoCB
            // 
            this.WojewodztwoCB.FormattingEnabled = true;
            this.WojewodztwoCB.Items.AddRange(new object[] {
            "dolnośląskie",
            "kujawsko-pomorskie",
            "lubelskie",
            "lubuskie",
            "łódzkie",
            "małopolskie",
            "mazowieckie",
            "opolskie",
            "podkarpackie",
            "podlaskie",
            "pomorskie",
            "śląskie",
            "świętokrzyskie",
            "warmińsko-mazurskie",
            "wielkopolskie",
            "zachodniopomorskie"});
            this.WojewodztwoCB.Location = new System.Drawing.Point(80, 112);
            this.WojewodztwoCB.Name = "WojewodztwoCB";
            this.WojewodztwoCB.Size = new System.Drawing.Size(105, 21);
            this.WojewodztwoCB.TabIndex = 15;
            this.WojewodztwoCB.SelectedIndexChanged += new System.EventHandler(this.WojewodztwoCB_SelectedIndexChanged);
            // 
            // PowiatTB
            // 
            this.PowiatTB.Location = new System.Drawing.Point(80, 80);
            this.PowiatTB.Name = "PowiatTB";
            this.PowiatTB.Size = new System.Drawing.Size(100, 20);
            this.PowiatTB.TabIndex = 14;
            // 
            // GminaTB
            // 
            this.GminaTB.Location = new System.Drawing.Point(80, 48);
            this.GminaTB.Name = "GminaTB";
            this.GminaTB.Size = new System.Drawing.Size(100, 20);
            this.GminaTB.TabIndex = 13;
            // 
            // MiejscowoscTB
            // 
            this.MiejscowoscTB.Location = new System.Drawing.Point(80, 16);
            this.MiejscowoscTB.Name = "MiejscowoscTB";
            this.MiejscowoscTB.Size = new System.Drawing.Size(100, 20);
            this.MiejscowoscTB.TabIndex = 12;
            // 
            // ZdjecieDodajBtn
            // 
            this.ZdjecieDodajBtn.Location = new System.Drawing.Point(673, 228);
            this.ZdjecieDodajBtn.Name = "ZdjecieDodajBtn";
            this.ZdjecieDodajBtn.Size = new System.Drawing.Size(92, 23);
            this.ZdjecieDodajBtn.TabIndex = 11;
            this.ZdjecieDodajBtn.Text = "Wstaw zdjęcie";
            this.ZdjecieDodajBtn.UseVisualStyleBackColor = true;
            this.ZdjecieDodajBtn.Click += new System.EventHandler(this.ZdjecieDodajBtn_Click);
            // 
            // ZdjecieSciezkaTB
            // 
            this.ZdjecieSciezkaTB.Location = new System.Drawing.Point(561, 228);
            this.ZdjecieSciezkaTB.Name = "ZdjecieSciezkaTB";
            this.ZdjecieSciezkaTB.Size = new System.Drawing.Size(100, 20);
            this.ZdjecieSciezkaTB.TabIndex = 10;
            // 
            // IloscTB
            // 
            this.IloscTB.Location = new System.Drawing.Point(406, 228);
            this.IloscTB.Name = "IloscTB";
            this.IloscTB.Size = new System.Drawing.Size(100, 20);
            this.IloscTB.TabIndex = 9;
            // 
            // CiezarTB
            // 
            this.CiezarTB.Location = new System.Drawing.Point(406, 190);
            this.CiezarTB.Name = "CiezarTB";
            this.CiezarTB.Size = new System.Drawing.Size(100, 20);
            this.CiezarTB.TabIndex = 8;
            // 
            // DaneFrmTB
            // 
            this.DaneFrmTB.Location = new System.Drawing.Point(200, 128);
            this.DaneFrmTB.Name = "DaneFrmTB";
            this.DaneFrmTB.Size = new System.Drawing.Size(100, 20);
            this.DaneFrmTB.TabIndex = 4;
            // 
            // YearUpDown
            // 
            this.YearUpDown.Location = new System.Drawing.Point(200, 48);
            this.YearUpDown.Maximum = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            this.YearUpDown.Minimum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.YearUpDown.Name = "YearUpDown";
            this.YearUpDown.Size = new System.Drawing.Size(120, 20);
            this.YearUpDown.TabIndex = 3;
            this.YearUpDown.Value = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.YearUpDown.ValueChanged += new System.EventHandler(this.YearUpDown_ValueChanged);
            // 
            // MatGrafPB
            // 
            this.MatGrafPB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MatGrafPB.Location = new System.Drawing.Point(545, -25);
            this.MatGrafPB.Name = "MatGrafPB";
            this.MatGrafPB.Size = new System.Drawing.Size(305, 235);
            this.MatGrafPB.TabIndex = 2;
            this.MatGrafPB.TabStop = false;
            // 
            // MatPodstTB
            // 
            this.MatPodstTB.Location = new System.Drawing.Point(200, 88);
            this.MatPodstTB.Name = "MatPodstTB";
            this.MatPodstTB.Size = new System.Drawing.Size(100, 20);
            this.MatPodstTB.TabIndex = 1;
            // 
            // TechSpecTab
            // 
            this.TechSpecTab.Location = new System.Drawing.Point(4, 22);
            this.TechSpecTab.Name = "TechSpecTab";
            this.TechSpecTab.Padding = new System.Windows.Forms.Padding(3);
            this.TechSpecTab.Size = new System.Drawing.Size(890, 278);
            this.TechSpecTab.TabIndex = 1;
            this.TechSpecTab.Text = "Specyfikacja techniczna";
            this.TechSpecTab.UseVisualStyleBackColor = true;
            // 
            // NazwaTB
            // 
            this.NazwaTB.Location = new System.Drawing.Point(195, 66);
            this.NazwaTB.Name = "NazwaTB";
            this.NazwaTB.Size = new System.Drawing.Size(100, 20);
            this.NazwaTB.TabIndex = 0;
            // 
            // NazwaLabel
            // 
            this.NazwaLabel.AutoSize = true;
            this.NazwaLabel.Location = new System.Drawing.Point(71, 69);
            this.NazwaLabel.Name = "NazwaLabel";
            this.NazwaLabel.Size = new System.Drawing.Size(93, 13);
            this.NazwaLabel.TabIndex = 33;
            this.NazwaLabel.Text = "Nazwa eksponatu";
            // 
            // ExpIDLabel
            // 
            this.ExpIDLabel.AutoSize = true;
            this.ExpIDLabel.Location = new System.Drawing.Point(419, 66);
            this.ExpIDLabel.Name = "ExpIDLabel";
            this.ExpIDLabel.Size = new System.Drawing.Size(118, 13);
            this.ExpIDLabel.TabIndex = 34;
            this.ExpIDLabel.Text = "Identyfikator eksponatu";
            // 
            // ExpIDTB
            // 
            this.ExpIDTB.Location = new System.Drawing.Point(543, 63);
            this.ExpIDTB.Name = "ExpIDTB";
            this.ExpIDTB.Size = new System.Drawing.Size(129, 20);
            this.ExpIDTB.TabIndex = 35;
            // 
            // ZapiszDaneBtn
            // 
            this.ZapiszDaneBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ZapiszDaneBtn.Location = new System.Drawing.Point(655, 234);
            this.ZapiszDaneBtn.Name = "ZapiszDaneBtn";
            this.ZapiszDaneBtn.Size = new System.Drawing.Size(81, 38);
            this.ZapiszDaneBtn.TabIndex = 37;
            this.ZapiszDaneBtn.Text = "Zapisz dane";
            this.ZapiszDaneBtn.UseVisualStyleBackColor = true;
            this.ZapiszDaneBtn.Click += new System.EventHandler(this.ZapiszDaneBtn_Click);
            // 
            // QRCodeGen
            // 
            this.QRCodeGen.Location = new System.Drawing.Point(437, 234);
            this.QRCodeGen.Name = "QRCodeGen";
            this.QRCodeGen.Size = new System.Drawing.Size(86, 38);
            this.QRCodeGen.TabIndex = 38;
            this.QRCodeGen.Text = "Generuj kod QR eksponatu";
            this.QRCodeGen.UseVisualStyleBackColor = true;
            this.QRCodeGen.Click += new System.EventHandler(this.QRCodeGen_Click);
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(36, 37);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(29, 13);
            this.StatusLabel.TabIndex = 40;
            this.StatusLabel.Text = "Stan";
            // 
            // StanCombo
            // 
            this.StanCombo.FormattingEnabled = true;
            this.StanCombo.Items.AddRange(new object[] {
            "Wypożyczony",
            "W magazynie",
            "Ekspozycja stała",
            "Ekspozycja czasowa",
            "W serwisie"});
            this.StanCombo.Location = new System.Drawing.Point(36, 56);
            this.StanCombo.Name = "StanCombo";
            this.StanCombo.Size = new System.Drawing.Size(121, 21);
            this.StanCombo.TabIndex = 39;
            this.StanCombo.SelectedIndexChanged += new System.EventHandler(this.StanCombo_SelectedIndexChanged);
            // 
            // StatusTab
            // 
            this.StatusTab.Controls.Add(this.AdresGroupBox);
            this.StatusTab.Controls.Add(this.StatusLabel);
            this.StatusTab.Controls.Add(this.StanCombo);
            this.StatusTab.Location = new System.Drawing.Point(4, 22);
            this.StatusTab.Name = "StatusTab";
            this.StatusTab.Padding = new System.Windows.Forms.Padding(3);
            this.StatusTab.Size = new System.Drawing.Size(890, 278);
            this.StatusTab.TabIndex = 2;
            this.StatusTab.Text = "Status eksponatu";
            this.StatusTab.UseVisualStyleBackColor = true;
            // 
            // AdresGroupBox
            // 
            this.AdresGroupBox.Controls.Add(this.groupBox1);
            this.AdresGroupBox.Controls.Add(this.DataZwrotuLabel);
            this.AdresGroupBox.Controls.Add(this.DataZwrotu);
            this.AdresGroupBox.Controls.Add(this.WypKomu);
            this.AdresGroupBox.Controls.Add(this.WypKomu_Label);
            this.AdresGroupBox.Location = new System.Drawing.Point(306, 9);
            this.AdresGroupBox.Name = "AdresGroupBox";
            this.AdresGroupBox.Size = new System.Drawing.Size(278, 261);
            this.AdresGroupBox.TabIndex = 41;
            this.AdresGroupBox.TabStop = false;
            this.AdresGroupBox.Text = "Wypożyczono do";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WojewodztwoWyp_Label);
            this.groupBox1.Controls.Add(this.WojewodztwoCombo);
            this.groupBox1.Controls.Add(this.GminaWyp);
            this.groupBox1.Controls.Add(this.GminaWyp_Label);
            this.groupBox1.Controls.Add(this.PowiatWyp);
            this.groupBox1.Controls.Add(this.PowiatWyp_Label);
            this.groupBox1.Controls.Add(this.MiastoWyp);
            this.groupBox1.Controls.Add(this.MiastoWyp_Label);
            this.groupBox1.Location = new System.Drawing.Point(9, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(263, 156);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Adres";
            // 
            // WojewodztwoWyp_Label
            // 
            this.WojewodztwoWyp_Label.AutoSize = true;
            this.WojewodztwoWyp_Label.Location = new System.Drawing.Point(6, 123);
            this.WojewodztwoWyp_Label.Name = "WojewodztwoWyp_Label";
            this.WojewodztwoWyp_Label.Size = new System.Drawing.Size(74, 13);
            this.WojewodztwoWyp_Label.TabIndex = 7;
            this.WojewodztwoWyp_Label.Text = "Województwo";
            // 
            // WojewodztwoCombo
            // 
            this.WojewodztwoCombo.FormattingEnabled = true;
            this.WojewodztwoCombo.Items.AddRange(new object[] {
            "",
            "dolnośląskie",
            "kujawsko-pomorskie",
            "lubelskie",
            "lubuskie",
            "łódzkie",
            "małopolskie",
            "mazowieckie",
            "opolskie",
            "podkarpackie",
            "podlaskie",
            "pomorskie",
            "śląskie",
            "świętokrzyskie",
            "warmińsko-mazurskie",
            "wielkopolskie",
            "zachodniopomorskie"});
            this.WojewodztwoCombo.Location = new System.Drawing.Point(93, 120);
            this.WojewodztwoCombo.Name = "WojewodztwoCombo";
            this.WojewodztwoCombo.Size = new System.Drawing.Size(145, 21);
            this.WojewodztwoCombo.TabIndex = 6;
            // 
            // GminaWyp
            // 
            this.GminaWyp.Location = new System.Drawing.Point(93, 89);
            this.GminaWyp.Name = "GminaWyp";
            this.GminaWyp.Size = new System.Drawing.Size(145, 20);
            this.GminaWyp.TabIndex = 5;
            // 
            // GminaWyp_Label
            // 
            this.GminaWyp_Label.AutoSize = true;
            this.GminaWyp_Label.Location = new System.Drawing.Point(6, 92);
            this.GminaWyp_Label.Name = "GminaWyp_Label";
            this.GminaWyp_Label.Size = new System.Drawing.Size(37, 13);
            this.GminaWyp_Label.TabIndex = 4;
            this.GminaWyp_Label.Text = "Gmina";
            // 
            // PowiatWyp
            // 
            this.PowiatWyp.Location = new System.Drawing.Point(93, 59);
            this.PowiatWyp.Name = "PowiatWyp";
            this.PowiatWyp.Size = new System.Drawing.Size(145, 20);
            this.PowiatWyp.TabIndex = 3;
            // 
            // PowiatWyp_Label
            // 
            this.PowiatWyp_Label.AutoSize = true;
            this.PowiatWyp_Label.Location = new System.Drawing.Point(6, 59);
            this.PowiatWyp_Label.Name = "PowiatWyp_Label";
            this.PowiatWyp_Label.Size = new System.Drawing.Size(39, 13);
            this.PowiatWyp_Label.TabIndex = 2;
            this.PowiatWyp_Label.Text = "Powiat";
            // 
            // MiastoWyp
            // 
            this.MiastoWyp.Location = new System.Drawing.Point(93, 30);
            this.MiastoWyp.Name = "MiastoWyp";
            this.MiastoWyp.Size = new System.Drawing.Size(145, 20);
            this.MiastoWyp.TabIndex = 1;
            // 
            // MiastoWyp_Label
            // 
            this.MiastoWyp_Label.AutoSize = true;
            this.MiastoWyp_Label.Location = new System.Drawing.Point(8, 30);
            this.MiastoWyp_Label.Name = "MiastoWyp_Label";
            this.MiastoWyp_Label.Size = new System.Drawing.Size(38, 13);
            this.MiastoWyp_Label.TabIndex = 0;
            this.MiastoWyp_Label.Text = "Miasto";
            // 
            // DataZwrotuLabel
            // 
            this.DataZwrotuLabel.AutoSize = true;
            this.DataZwrotuLabel.Location = new System.Drawing.Point(15, 240);
            this.DataZwrotuLabel.Name = "DataZwrotuLabel";
            this.DataZwrotuLabel.Size = new System.Drawing.Size(64, 13);
            this.DataZwrotuLabel.TabIndex = 9;
            this.DataZwrotuLabel.Text = "Data zwrotu";
            // 
            // DataZwrotu
            // 
            this.DataZwrotu.Location = new System.Drawing.Point(102, 238);
            this.DataZwrotu.Name = "DataZwrotu";
            this.DataZwrotu.Size = new System.Drawing.Size(145, 20);
            this.DataZwrotu.TabIndex = 7;
            // 
            // WypKomu
            // 
            this.WypKomu.Location = new System.Drawing.Point(29, 41);
            this.WypKomu.Name = "WypKomu";
            this.WypKomu.Size = new System.Drawing.Size(218, 20);
            this.WypKomu.TabIndex = 0;
            // 
            // WypKomu_Label
            // 
            this.WypKomu_Label.AutoSize = true;
            this.WypKomu_Label.Location = new System.Drawing.Point(6, 25);
            this.WypKomu_Label.Name = "WypKomu_Label";
            this.WypKomu_Label.Size = new System.Drawing.Size(147, 13);
            this.WypKomu_Label.TabIndex = 1;
            this.WypKomu_Label.Text = "Nazwa instytucji / wydarzenia";
            // 
            // ZmienEksponat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 605);
            this.Controls.Add(this.QRCodeGen);
            this.Controls.Add(this.ZapiszDaneBtn);
            this.Controls.Add(this.ExpIDTB);
            this.Controls.Add(this.ExpIDLabel);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Path);
            this.Controls.Add(this.NodeName);
            this.Controls.Add(this.NodeValue);
            this.Controls.Add(this.NazwaLabel);
            this.Controls.Add(this.NazwaTB);
            this.Name = "ZmienEksponat";
            this.Text = "ZmienEksponat";
            this.tabControl1.ResumeLayout(false);
            this.NIDTab.ResumeLayout(false);
            this.NIDTab.PerformLayout();
            this.Panel_NID.ResumeLayout(false);
            this.Panel_NID.PerformLayout();
            this.WymiaryGB.ResumeLayout(false);
            this.WymiaryGB.PerformLayout();
            this.OprKartyGB.ResumeLayout(false);
            this.OprKartyGB.PerformLayout();
            this.AdresGB.ResumeLayout(false);
            this.AdresGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YearUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MatGrafPB)).EndInit();
            this.StatusTab.ResumeLayout(false);
            this.StatusTab.PerformLayout();
            this.AdresGroupBox.ResumeLayout(false);
            this.AdresGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NodeValue;
        private System.Windows.Forms.Label NodeName;
        private System.Windows.Forms.Label Path;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage NIDTab;
        private System.Windows.Forms.TabPage TechSpecTab;
        private System.Windows.Forms.Panel Panel_NID;
        private System.Windows.Forms.TextBox OpisTB;
        private System.Windows.Forms.TextBox HistoriaTB;
        private System.Windows.Forms.TextBox FormyOchrTB;
        private System.Windows.Forms.TextBox UdstTB;
        private System.Windows.Forms.TextBox MscePcyTB;
        private System.Windows.Forms.TextBox UzytkownikTB;
        private System.Windows.Forms.TextBox WlascicielTB;
        private System.Windows.Forms.GroupBox AdresGB;
        private System.Windows.Forms.ComboBox WojewodztwoCB;
        private System.Windows.Forms.TextBox PowiatTB;
        private System.Windows.Forms.TextBox GminaTB;
        private System.Windows.Forms.TextBox MiejscowoscTB;
        private System.Windows.Forms.Button ZdjecieDodajBtn;
        private System.Windows.Forms.TextBox ZdjecieSciezkaTB;
        private System.Windows.Forms.TextBox IloscTB;
        private System.Windows.Forms.TextBox CiezarTB;
        private System.Windows.Forms.TextBox DimDlTB;
        private System.Windows.Forms.TextBox DimSzerTB;
        private System.Windows.Forms.TextBox DimWysTB;
        private System.Windows.Forms.TextBox DaneFrmTB;
        private System.Windows.Forms.NumericUpDown YearUpDown;
        private System.Windows.Forms.PictureBox MatGrafPB;
        private System.Windows.Forms.TextBox MatPodstTB;
        private System.Windows.Forms.TextBox NazwaTB;
        private System.Windows.Forms.TextBox ZalacznikiTB;
        private System.Windows.Forms.GroupBox OprKartyGB;
        private System.Windows.Forms.TextBox FotoOprTB;
        private System.Windows.Forms.TextBox PlanyOprTB;
        private System.Windows.Forms.TextBox OprTekstTB;
        private System.Windows.Forms.TextBox AdnInspTB;
        private System.Windows.Forms.TextBox UwagiTB;
        private System.Windows.Forms.TextBox ZrodlaIkoTB;
        private System.Windows.Forms.TextBox BibGrfTB;
        private System.Windows.Forms.TextBox AktaArchTB;
        private System.Windows.Forms.TextBox StanTB;
        private System.Windows.Forms.TextBox RemontyTB;
        private System.Windows.Forms.TextBox UzObecneTB;
        private System.Windows.Forms.TextBox PrzeznPierwTB;
        private System.Windows.Forms.GroupBox WymiaryGB;
        private System.Windows.Forms.Label SzerLabel;
        private System.Windows.Forms.Label WysLabel;
        private System.Windows.Forms.Label DlugoscLabel;
        private System.Windows.Forms.Label DaneFrmLabel;
        private System.Windows.Forms.Label MatPodstLabel;
        private System.Windows.Forms.Label RokProdLabel;
        private System.Windows.Forms.Label NazwaLabel;
        private System.Windows.Forms.Label ExpIDLabel;
        private System.Windows.Forms.TextBox ExpIDTB;
        private System.Windows.Forms.Label UserLabel;
        private System.Windows.Forms.Label OwnerLabel;
        private System.Windows.Forms.Label IloscLabel;
        private System.Windows.Forms.Label CiezarLabel;
        private System.Windows.Forms.Label ZalacznikiLabel;
        private System.Windows.Forms.Label AdnLabel;
        private System.Windows.Forms.Label UwagiLabel;
        private System.Windows.Forms.Label ZrodlaLabel;
        private System.Windows.Forms.Label BibliografiaLabel;
        private System.Windows.Forms.Label AktaLabel;
        private System.Windows.Forms.Label StanLabel;
        private System.Windows.Forms.Label RemontyLabel;
        private System.Windows.Forms.Label PrzeznLabel;
        private System.Windows.Forms.Label CurUseLabel;
        private System.Windows.Forms.Label OpisLabel;
        private System.Windows.Forms.Label HistoriaLabel;
        private System.Windows.Forms.Label OchronaLabel;
        private System.Windows.Forms.Label UdostLabel;
        private System.Windows.Forms.Label MscePcyLabel;
        private System.Windows.Forms.Label Opr_foto_Label;
        private System.Windows.Forms.Label Opr_plany_Label;
        private System.Windows.Forms.Label Opr_TekstLabel;
        private System.Windows.Forms.Button ZapiszDaneBtn;
        private System.Windows.Forms.Button QRCodeGen;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.ComboBox StanCombo;
        private System.Windows.Forms.TabPage StatusTab;
        private System.Windows.Forms.GroupBox AdresGroupBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label WojewodztwoWyp_Label;
        private System.Windows.Forms.ComboBox WojewodztwoCombo;
        private System.Windows.Forms.TextBox GminaWyp;
        private System.Windows.Forms.Label GminaWyp_Label;
        private System.Windows.Forms.TextBox PowiatWyp;
        private System.Windows.Forms.Label PowiatWyp_Label;
        private System.Windows.Forms.TextBox MiastoWyp;
        private System.Windows.Forms.Label MiastoWyp_Label;
        private System.Windows.Forms.Label DataZwrotuLabel;
        private System.Windows.Forms.DateTimePicker DataZwrotu;
        private System.Windows.Forms.TextBox WypKomu;
        private System.Windows.Forms.Label WypKomu_Label;
    }
}