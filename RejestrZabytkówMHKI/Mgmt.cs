﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
//using System.Configuration;
//using Emgu.CV;
using MySql.Data;
using MySql.Data.MySqlClient;
//using ZXing.Common;
//using ZXing.QrCode;
//using ZXing.Rendering;
//using PdfSharp;
//using Renci.SshNet;
//using Renci.SshNet.Common;
using WindowsFormsAero.TaskDialog;
namespace RejestrZabytkówMHKI
{
    public partial class RejestrZabytków : Form
    {
        private string nodevalue, nodename,user,host,rodzic,producent,typ,nazwa,identyfikator,pelnasciezka,selectedid;
        private DataTable warianty = new DataTable();
        private DataTable tabela = new DataTable();
        private DataTable PytaniaOdpowiedzi = new DataTable();
        private bool isdbconnected;
        private DataTable DataSource = new DataTable();
        private bool wierszklikniety;
        private DataTable dt_preview = new DataTable();
        //private string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();
        public RejestrZabytków()
        {
            
            InitializeComponent();
            //string key = "e3:79:bd:ef:f9:34:37:a9:52:92:a0:fe:45:e5:51:3a";
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            MySqlCommand polecenie = new MySqlCommand("set net_write_timeout=99999; set net_read_timeout=99999; set names 'cp1250'", link);
            //ListaEkspo.CheckBoxes = true;
            
            try
            {
                link.Open();
                polecenie.ExecuteNonQuery();
                link.Close();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.ToString());
            }
            
            loadTreeMenu();
            drzewo.PathSeparator = "\\";
            delCat.Visible = moveCat.Visible = addCat.Visible = true;
            
            
            
        }
        #region Funkcje zapełniające drzewo kategorii

        public void loadTreeMenu()
        {
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            MySqlCommand polecenie = new MySqlCommand();
            polecenie.CommandText = "SELECT * FROM mptt_al";
            polecenie.Connection = link;
            MySqlDataAdapter da = new MySqlDataAdapter(polecenie);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow menu in dt.Select("rodzic=0"))
                {
                    TreeNode ParentNode = new TreeNode();
                    ParentNode.Text = menu["skr"].ToString();
                    ParentNode.Name = menu["skr"].ToString();
                    drzewo.Nodes.Add(ParentNode);
                    loadTreeSubMenu(ref ParentNode, int.Parse(menu["id"].ToString()), dt);
                }
            }
            drzewo.Show();
        }
        private void loadTreeSubMenu(ref TreeNode ParentNode, int ParentId, DataTable hierarchia)
        {
            DataRow[] dzieci = hierarchia.Select("rodzic='" + ParentId + "'");
            foreach (DataRow wiersz in dzieci)
            {
                TreeNode child = new TreeNode();
                child.Text = wiersz["nazwa"].ToString();
                child.Name = wiersz["skr"].ToString();
                ParentNode.Nodes.Add(child);
                loadTreeSubMenu(ref child, int.Parse(wiersz["id"].ToString()), hierarchia);
            }
        }
        
        #endregion
        private void RejestrZabytków_Load(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void zakończToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz zamknąć program?", "Potwierdzenie zamknięcia aplikacji", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) Application.Exit();
        }

        private void drzewo_AfterSelect(object sender, TreeViewEventArgs e)
        {
         
        }
        private void drzewo_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show(drzewo.SelectedNode.FullPath);
            //Console.WriteLine("Kliknięto: " + drzewo.SelectedNode.Name.ToString());
        }
        private bool checkForChildren(string wezel) 
        {

            string podkategorie = "SELECT wezel.skr, (COUNT(rodzic.skr) - (poddrzewo.glebokosc + 1)) AS glebokosc ";
            podkategorie += "FROM rejestr AS wezel, rejestr AS rodzic, rejestr AS sub_rodzic,(";
            podkategorie += "SELECT wezel.skr, (COUNT(rodzic.skr) - 1) AS glebokosc ";
            podkategorie += "FROM rejestr AS wezel, rejestr AS rodzic WHERE wezel.lewy BETWEEN rodzic.lewy AND rodzic.prawy ";
            podkategorie += "AND wezel.skr = '" + wezel + "' GROUP BY wezel.skr ORDER BY wezel.lewy)AS poddrzewo ";
            podkategorie += "WHERE wezel.lewy BETWEEN rodzic.lewy AND rodzic.prawy AND wezel.lewy BETWEEN sub_rodzic.lewy AND sub_rodzic.prawy ";
            podkategorie += "AND sub_rodzic.skr = poddrzewo.skr GROUP BY wezel.skr HAVING glebokosc = 1 ORDER BY wezel.lewy";
            
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            link.Open();
            MySqlCommand polecenie = new MySqlCommand(podkategorie,link);
            MySqlDataAdapter da = new MySqlDataAdapter(polecenie);
            DataTable dt = new DataTable();
            da.Fill(dt);
            da.Dispose();
            polecenie.Dispose();
            string chk = "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'mhki' AND table_name = '" + nodename + "'";
            polecenie.CommandText = chk;
            Console.WriteLine("chk: " + chk);
            int spr = Convert.ToInt32(polecenie.ExecuteScalar().ToString());
            Console.WriteLine("spr = " + spr + "\n dt.Rows.Count = " + dt.Rows.Count);
            link.Close();
            if (dt.Rows.Count != 0) return true;
            else return false;
            
        }
        #region Tunel SSH
        /*private bool checkIfTableExists(string nodename) 
        {
            PasswordConnectionInfo info = new PasswordConnectionInfo("155.158.108.213", "mhki", "mhki");
            info.Timeout = TimeSpan.FromHours(2d);
            SshClient klient = new SshClient(info);
            try
            {
                klient.Connect();
            }
            catch (SshException e)
            {
                MessageBox.Show(e.Message + "\n" + e.Data);
            }

            if (!klient.IsConnected)
            {
                MessageBox.Show("Nie można nawiązać połączenia SSH");
            }
            var portFwd = new ForwardedPortLocal("127.0.0.1", 10000, "localhost", 3306);
            klient.AddForwardedPort(portFwd);
            portFwd.Start();
            if (!portFwd.IsStarted)
            {
                MessageBox.Show("Nie udało się otworzyć tunelu SSH", "Uwaga!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            MySqlConnection link = new MySqlConnection("server=127.0.0.1;port=10000;uid=mhki;database=mhki;password=mhki");
            MySqlCommand polecenie = new MySqlCommand();
            string chk = "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'mhki' AND table_name = '"+nodename+"'";
            polecenie.CommandText=chk;
            polecenie.Connection = link;
            link.Open();
            int spr = Convert.ToInt32(polecenie.ExecuteScalar().ToString());
            link.Close();
            klient.Disconnect();
            if (spr == 0) return false;
            else return true;
        }
        */
        #endregion

        private void WypelnijImageList()
        {
            imageList.Images.Clear();
            MySqlConnection polaczenie1 = new MySqlConnection(login.danepolaczenia);
            string pobierzobraz = "SELECT matgraficzne FROM eksponaty WHERE kategoria='" + nodevalue + "' ORDER BY identyfikator ASC";
            //Console.WriteLine(pobierzobraz);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(new MySqlCommand(pobierzobraz, polaczenie1));
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            Console.WriteLine("Obrazów dla kategorii " + nodevalue + " jest " + dataSet.Tables[0].Rows.Count);
            for (int i = 0; i < dataSet.Tables[0].Rows.Count;i++ )
            {
                
                Byte[] data = new Byte[0];
                data = (Byte[])(dataSet.Tables[0].Rows[i]["matgraficzne"]);
                MemoryStream mem = new MemoryStream(data);
                Image img = Image.FromStream(mem);
                
                imageList.Images.Add(img);
            }
        }

        private void drzewo_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) 
        {
            PathLabel.Text = "Pełna ścieżka: ";
            wybranapozycja.Text = e.Node.Text;

            PathLabel.Text += e.Node.FullPath.ToString();
            pelnasciezka = e.Node.FullPath.ToString();
            nodevalue = e.Node.Text;
            nodename = e.Node.Name;
            if (TreeNode.Equals(e.Node.Parent, null)) rodzic = "MHKI";
            else rodzic = e.Node.Parent.Text;
            if (nodevalue != "MHKI")
            {
                dt_preview.Clear();
                ListaEksp.Clear();
                //WypelnijImageList();
                string previewquery = "SELECT identyfikator, nazwa FROM eksponaty WHERE kategoria='" + nodevalue + "' ORDER BY identyfikator ASC";
                //Console.WriteLine(previewquery);
                MySqlConnection polaczenie = new MySqlConnection(login.danepolaczenia);
                MySqlCommand prevcomm = new MySqlCommand();
                prevcomm.CommandText = previewquery;
                prevcomm.Connection = polaczenie;
                MySqlDataAdapter da_preview = new MySqlDataAdapter(prevcomm);
                da_preview.Fill(dt_preview);
                
                MySqlDataReader dr_lista;
                try 
                {
                    polaczenie.Open();
                    dr_lista = prevcomm.ExecuteReader();
                    while (dr_lista.Read()) 
                    {
                        string nazwa = dr_lista.GetString("nazwa");
                        string id = dr_lista.GetString("identyfikator");
                        ListaEksp.Items.Add(id + "\n" + nazwa);
                    }
                }
                catch 
                {
                
                }

            }
            /*if (checkForChildren(nodename))
            {
                addCat.Visible = true;
                delCat.Visible = true;
                moveCat.Visible = true;
            }
            else 
            { 
                addCat.Visible = delCat.Visible = moveCat.Visible = false; 
            }*/
        }

        private void delCat_Click(object sender, EventArgs e)
        {
            deleteCategory delCatForm = new deleteCategory(nodename);
            delCatForm.Show();
        }

        private void moveCat_Click(object sender, EventArgs e)
        {
            moveCategory moveCatForm = new moveCategory();
            moveCatForm.Show();
        }

        private void addCat_Click(object sender, EventArgs e)
        {
            addCategory addCatForm = new addCategory(nodename,rodzic);
            addCatForm.Show();
        }


        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        

        private void ShowFullExpCard_Click(object sender, EventArgs e)
        {
            
                FullCard pokazpelnakarte = new FullCard(selectedid);
                pokazpelnakarte.Show();
            
        }

        private void AddExpBtn_Click(object sender, EventArgs e)
        {
            DodajEksponat AddExp = new DodajEksponat(nodename, nodevalue, pelnasciezka);
            AddExp.Show();
        }

        private void ModifyExp_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(selectedid))
            {
                ZmienEksponat modify = new ZmienEksponat(selectedid, nodevalue,pelnasciezka);
                
                modify.Show();
            }
            else MessageBox.Show("Wybierz eksponat, którego dane chcesz zmienić", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void ListaEksp_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedid=ListaEksp.FocusedItem.Text.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[0];
            
            
        }

        private void ModExpoCat_Click(object sender, EventArgs e)
        {
            PrzeniesEksponat modExpoCat = new PrzeniesEksponat(selectedid,nodevalue);
            modExpoCat.Show();
        }
       
    }
}