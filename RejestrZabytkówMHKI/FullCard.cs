﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PdfSharp;
using OfficeOpenXml;
using MySql.Data.Common;
using MySql.Data.MySqlClient;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using ZXing.QrCode;
namespace RejestrZabytkówMHKI
{
    public partial class FullCard : Form
    {
        private string identyfikator, nazwa, typ, producent;
        //private string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();
        DataTable tablica = new DataTable();

        public FullCard(string id)
        {
            identyfikator = id;
            
            InitializeComponent();
            WypelnijPictureBox();
            wypelnijTabele();
        }
        private string makequery() 
        {
            string query = "SELECT nazwa,kategoria,czaspowstania,matpodstawowy,danefirmowe,wymiary,ciezar,ilosc,wlasciciel,uzytkownik,mscepracy,"+
            "udostepnianie,ochrona,historia,opis,pierwprzeznaczenie,obuzytkowanie,remonty,stan,akta,bibliografia,ikonografia,uwagi,inspekcje,oprkarty,zalaczniki ";
            query+=" FROM eksponaty WHERE identyfikator='"+identyfikator+"'";
            return query;
        }

        //Wypełnianie adresu eksponatu z tablicy adres
        private void adreseksponatu() 
        {
            string getaddress = "SELECT miasto, powiat, gmina, wojewodztwo FROM `adres` WHERE identyfikator='"+identyfikator+"'";
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            MySqlCommand getadr = new MySqlCommand(getaddress, link);
            DataTable tabadr = new DataTable();
            MySqlDataAdapter adrDA = new MySqlDataAdapter(getadr);
            adrDA.Fill(tabadr);
            AdresTextBox.Text = "Miasto: " + tabadr.Rows[0]["miasto"].ToString() + "\n Gmina: " + tabadr.Rows[0]["gmina"].ToString() + "\n Powiat: " + tabadr.Rows[0]["powiat"].ToString() + "\n Województwo: " + tabadr.Rows[0]["wojewodztwo"].ToString();
        }
        //i danych o sporządającym kartę z tablicy stan
        private void spkarte() 
        {
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            string ktozmienilkarte = "SELECT userid from `stan` WHERE identyfikator='" + identyfikator +"'";
            MySqlCommand kto = new MySqlCommand(ktozmienilkarte, link);
            string userid=Convert.ToString(kto.ExecuteScalar());
            kto.CommandText = "SELECT imienazw FROM users WHERE uid=" + userid;

        }

        private void WypelnijPictureBox() 
        {
            MySqlConnection polaczenie1 = new MySqlConnection(login.danepolaczenia);
            string pobierzobraz = "SELECT matgraficzne FROM eksponaty WHERE identyfikator='" + identyfikator + "'";
            //Console.WriteLine(pobierzobraz);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(new MySqlCommand(pobierzobraz, polaczenie1));
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count == 1)
            {
                //Console.WriteLine("Znalazłem obraz, wypełniam picturebox");
                Byte[] data = new Byte[0];
                data = (Byte[])(dataSet.Tables[0].Rows[0]["matgraficzne"]);
                MemoryStream mem = new MemoryStream(data);
                MatGrafPictBox.Image = Image.FromStream(mem);
            } 
        }
        private void wypelnijTabele() 
        {
            MySqlConnection polaczenie = new MySqlConnection(login.danepolaczenia);
            MySqlDataAdapter adapterTablicy = new MySqlDataAdapter(new MySqlCommand(makequery(),polaczenie));
            
            adapterTablicy.Fill(tablica);
            #region Wypełnienie pól szablonu pełnej karty eksponatu w kolejności pól

            NazwaLabel.Text = tablica.Rows[0]["nazwa"].ToString();//1
            CzasPowstLabel.Text = tablica.Rows[0]["czaspowstania"].ToString();//2
            MatPodstLabel.Text = tablica.Rows[0]["matpodstawowy"].ToString();//3
            DaneFirmoweLabel.Text = tablica.Rows[0]["danefirmowe"].ToString(); ;//4
            dimTxtBox.Text = tablica.Rows[0]["wymiary"].ToString();//5
            CiezarLabel.Text = tablica.Rows[0]["ciezar"].ToString();//6
            IloscLabel.Text = tablica.Rows[0]["ilosc"].ToString();//7

            //Zdjęcie wrzuca do szablonu osobna funkcja - WypelnijPictrureBox() //8

            adreseksponatu();//9
            WlascicielTextBox.Text = tablica.Rows[0]["wlasciciel"].ToString();//10
            UzytkownikTextBox.Text= tablica.Rows[0]["uzytkownik"].ToString();//11
            MscePracyTextBox.Text = tablica.Rows[0]["mscepracy"].ToString();//12
            UdostepnianieLabel.Text = tablica.Rows[0]["udostepnianie"].ToString();//13
            OchronaLabel.Text = tablica.Rows[0]["ochrona"].ToString();//14
            HistoriaTextBox.Text = tablica.Rows[0]["historia"].ToString();//15
            opisTextBox.Text = tablica.Rows[0]["opis"].ToString();//16
            PrzeznTextBox.Text = tablica.Rows[0]["pierwprzeznaczenie"].ToString();//17
            UzytkowanieTextBox.Text = tablica.Rows[0]["obuzytkowanie"].ToString();//18
            RemontyTextBox.Text = tablica.Rows[0]["remonty"].ToString();//19
            StanTextBox.Text = tablica.Rows[0]["stan"].ToString();//20
            AktaTextBox.Text = tablica.Rows[0]["akta"].ToString();//21
            BibliografiaTextBox.Text = tablica.Rows[0]["bibliografia"].ToString();//22
            ZrodlaIkoTextBox.Text = tablica.Rows[0]["ikonografia"].ToString();//23
            UwagiTextBox.Text = tablica.Rows[0]["uwagi"].ToString();//24
            AdnotacjaTextBox.Text = tablica.Rows[0]["inspekcje"].ToString();//25
            OpracowanieTextBox.Text = tablica.Rows[0]["oprkarty"].ToString();//26
            ZalacznikiTextBox.Text = tablica.Rows[0]["zalaczniki"].ToString();//27

            #endregion
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void str1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FullCard_Load(object sender, EventArgs e)
        {
            //Console.WriteLine(makequery());
            //WypelnijPictureBox();
        }
        
        #region puste funkcje
        private void DaneFrmGrpBox_Enter(object sender, EventArgs e)
        {

        }

        private void CzasPowstGrpBox_Enter(object sender, EventArgs e)
        {

        }

        private void UdostGrpBox_Enter(object sender, EventArgs e)
        {

        }
        #endregion 
        
        private void PDFExportBtn_Click(object sender, EventArgs e)
        {
            ExportPDFDialog.ShowDialog();
        }

        private void XLSExportBtn_Click(object sender, EventArgs e)
        {
            ExportXLSDialog.ShowDialog();
        }

        private void ExportPDFDialog_FileOk(object sender, CancelEventArgs e)
        {
            FileInfo plik = new FileInfo(ExportPDFDialog.FileName);
            
            using (PdfDocument document = new PdfDocument())
            {

                XFont hdrfont = new XFont("Arial CE", 9, XFontStyle.Regular);
                XFont font = new XFont("Arial CE", 12, XFontStyle.Bold);
                //XFont test = new XFont("Arial", 12, XFontStyle.Regular);
                //gfx.DrawString(nazwa, font, XBrushes.Black, 10, 10);
                XPen pioro = new XPen(XColors.Black, 1);

                #region siatka strony 1
                PdfPage str1 = document.AddPage();
                str1.Size = PageSize.A4;
                str1.Orientation = PageOrientation.Landscape;
                XGraphics gfx = XGraphics.FromPdfPage(str1);

                #region Prostokąty na tekst
                XRect Tytul1 = new XRect(0, 0, 270, 53);
                XRect Tytul2 = new XRect(270, 0, 354, 53);
                XRect Pole1 = new XRect(new XPoint(10, 82.78), new XPoint(354, 105 + 82.78));
                XRect Pole2 = new XRect(new XPoint(10 + 354, 82.78), new XSize(267, 48));
                XRect Pole3 = new XRect(new XPoint(10 + 354, 82.78 + 48), new XSize(Pole2.Width, 59));
                XRect Pole4 = new XRect(new XPoint(10, 82.78 + 105), new XSize(Pole1.Width, 101));
                XRect Pole5 = new XRect(new XPoint(10 + 354, 82.78 + 48 + 59), new XPoint(354 + 200, 82.78 + 48 + 59 + 101));
                XRect Pole6 = new XRect(new XPoint(10 + 354 + 200, 59 + 48 + 82.78), new XPoint(354 + 200 + 68, 82.78 + 59 + 48 + 71));
                XRect Pole7 = new XRect(new XPoint(10 + 354 + 200, 82.78 + 48 + 59 + 71), new XPoint(623 + 10, 27 + 71 + 59 + 48 + 82.78));
                //XRect Pole8 = new XRect(new XPoint(10, Pole4.Bottom), new XPoint(Pole14.Left, Pole14.Bottom));
                XRect Pole9 = new XRect(new XPoint(622.72 + 10, 20), new XPoint(str1.Width - 10, 158.52));
                XRect Pole10 = new XRect(new XPoint(Pole9.Left, Pole9.Bottom), new XSize(Pole9.Width, 104));
                XRect Pole11 = new XRect(new XPoint(Pole10.Left, Pole10.Bottom), new XSize(Pole9.Width, 120));
                XRect Pole12 = new XRect(new XPoint(Pole11.Left, Pole11.Bottom), new XSize(Pole9.Width, 88));
                XRect Pole13 = new XRect(new XPoint(Pole12.Left, Pole12.Bottom), new XSize(Pole9.Width, 45));
                XRect Pole14 = new XRect(new XPoint(Pole13.Left, Pole13.Bottom), new XSize(Pole9.Width, 80));
                XRect Pole8 = new XRect(new XPoint(10, Pole4.Bottom), new XPoint(Pole14.Left-10, Pole14.Bottom));
                //gfx.DrawRectangle(XBrushes.Navy, Pole8);
                XRect[] pola_str1 = new XRect[] { Pole1, Pole2, Pole3, Pole4, Pole5, Pole6, Pole7, Pole8, Pole9, Pole10, Pole11, Pole12, Pole13, Pole14 };
                string[] nazwa_pola_s1 = new string[] { "1. Nazwa", "2. Czas powstania", "3. Materiał podstawowy", "4. Dane firmowe", "5. Wymiary", "6. Ciężar", "7. Ilość", "8. Materiały graficzne", "9. Adres", "10. Właściciel i jego adres", "11. Użytkownik i jego adres", "12. Miejsce pracy", "13. Udostępnienie", "14. Formy ochrony" };
                #endregion

                #region nagłówek pierwszej strony

                gfx.DrawLine(pioro, new XPoint(10, 75), new XPoint(270 + 353, 75));
                gfx.DrawLine(pioro, new XPoint(270, 20), new XPoint(270, 75));

                #endregion
                #region linie siatki
                //linie poziome
                gfx.DrawLine(pioro, new XPoint(10, 82.78 + 105), new XPoint(622.72, 82.78 + 105));
                gfx.DrawLine(pioro, new XPoint(10, 82.78 + 105 + 101), new XPoint(622.72, 82.78 + 105 + 101));
                gfx.DrawLine(pioro, new XPoint(354, 82.78 + 48), new XPoint(622.72, 82.78 + 48));
                gfx.DrawLine(pioro, new XPoint(622.72, 158.52), new XPoint(str1.Width - 10, 158.52));
                gfx.DrawLine(pioro, new XPoint(354 + 200, 68 + 59 + 48 + 82.78), new XPoint(str1.Width - 10, 68 + 59 + 48 + 82.78));
                gfx.DrawLine(pioro, new XPoint(622.72, 10 + 158.52 + 104 + 120), new XPoint(str1.Width - 10, 10 + 158.52 + 104 + 120));
                gfx.DrawLine(pioro, new XPoint(622.72, 10 + 158.52 + 104 + 120 + 88), new XPoint(str1.Width - 10, 10 + 158.52 + 104 + 120 + 88));
                gfx.DrawLine(pioro, new XPoint(622.72, 10 + 158.52 + 104 + 120 + 88 + 45), new XPoint(str1.Width - 10, 10 + 158.52 + 104 + 120 + 88 + 45));
                //linie pionowe
                gfx.DrawLine(pioro, new XPoint(622.72, 20), new XPoint(622.72, str1.Height - 20));
                gfx.DrawLine(pioro, new XPoint(354, 82.78), new XPoint(354, 82.78 + 101 + 59 + 48));
                gfx.DrawLine(pioro, new XPoint(354 + 200, 59 + 48 + 82.78), new XPoint(354 + 200, 59 + 48 + 82.78 + 98));
                #endregion
                string[] zaw_pola_s1 = new string[] {NazwaLabel.Text,CzasPowstLabel.Text,MatPodstLabel.Text,DaneFirmoweLabel.Text,dimTxtBox.Text,CiezarLabel.Text,IloscLabel.Text,"",AdresTextBox.Text,WlascicielTextBox.Text,UzytkownikTextBox.Text,MscePracyTextBox.Text,UdostepnianieLabel.Text,OchronaLabel.Text };
                for (int i = 0; i < 14; i++)
                {
                    gfx.DrawString(nazwa_pola_s1[i], hdrfont, XBrushes.Black, pola_str1[i], XStringFormats.TopLeft);
                    gfx.DrawString(zaw_pola_s1[i],font,XBrushes.Black,pola_str1[i],XStringFormats.Center);
                                        
                }
                //double x = (MatGrafPictBox.Image.Size.Width * 72 / MatGrafPictBox.Image.HorizontalResolution) / 2;
                double a = Pole8.Height / MatGrafPictBox.Image.Height;
                 
               // gfx.ScaleTransform(a);
                gfx.DrawImage(MatGrafPictBox.Image,new XRect(new XPoint(Pole8.Left+10,Pole8.Top+20), new XPoint(a*MatGrafPictBox.Image.Width,MatGrafPictBox.Image.Height/a)));

                #endregion

                #region siatka strony 2
                PdfPage str2 = document.AddPage();
                str2.Size = PageSize.A4;
                str2.Orientation = PageOrientation.Landscape;
                XGraphics gfx2 = XGraphics.FromPdfPage(str2);
                XRect Pole15 = new XRect(new XPoint(10, 20), new XSize(325, str2.Height - 20));
                XRect Pole16 = new XRect(new XPoint(Pole15.Right, Pole15.Top), new XSize(515, str2.Height - 20));
                XRect[] pola_str2 = new XRect[] { Pole15, Pole16 };
                string[] nazwa_pola_s2 = new string[] { "15. Historia obiektu", "16. Opis i charakterystyka techniczna" };
                string[] zaw_pola_s2 = new string[] { HistoriaTextBox.Text,opisTextBox.Text};
                gfx2.DrawLine(pioro, new XPoint(325 + 10, 20), new XPoint(325 + 10, str2.Height - 20));

                for (int i = 0; i < 2; i++)
                {
                    gfx2.DrawString(nazwa_pola_s2[i], hdrfont, XBrushes.Black, pola_str2[i], XStringFormats.TopLeft);
                    gfx2.DrawString(zaw_pola_s2[i], font, XBrushes.Black, pola_str2[i], XStringFormats.Center);
                }

                #endregion

                #region siatka strony 3
                PdfPage str3 = document.AddPage();
                str3.Size = PageSize.A4;
                str3.Orientation = PageOrientation.Landscape;
                XGraphics gfx3 = XGraphics.FromPdfPage(str3);
                XRect Pole17 = new XRect(new XPoint(10, 20), new XSize(399 - 198.18, 71.33));
                XRect Pole18 = new XRect(new XPoint(Pole17.Right, Pole17.Top), new XSize(198.18, 71.33));
                XRect Pole19 = new XRect(new XPoint(Pole17.Left, Pole17.Bottom), new XSize(399, 494.93));
                XRect Pole20 = new XRect(new XPoint(Pole18.Right, Pole18.Top), new XSize(str3.Width - Pole19.Width, str3.Height - 20));
                //pionowe
                gfx3.DrawLine(pioro, new XPoint(399 + 10, 20), new XPoint(399 + 10, str3.Height - 20));
                gfx3.DrawLine(pioro, new XPoint(399 - 198.18 + 10, 20), new XPoint(399 - 198.18 + 10, 20 + 71.33));
                //poziome
                gfx3.DrawLine(pioro, new XPoint(10, 20 + 71.33), new XPoint(399 + 10, 20 + 71.33));
                XRect[] pola_str3 = new XRect[] { Pole17, Pole18, Pole19, Pole20 };
                string[] nazwa_pola_s3 = new string[] { "17. Przeznaczenie pierwotne", "18. Użytkowanie obecne", "19. Remonty, zmiany konstrukcyjne, modernizacje", "20. Stan zachowania i potrzeby konserwatorskie" };
                string[] zaw_pola_s3 = new string[] {PrzeznTextBox.Text,UzytkowanieTextBox.Text,RemontyTextBox.Text,StanTextBox.Text };
                for (int i = 0; i < 4; i++)
                {
                    gfx3.DrawString(nazwa_pola_s3[i], hdrfont, XBrushes.Black, pola_str3[i], XStringFormats.TopLeft);
                    gfx3.DrawString(zaw_pola_s3[i], hdrfont, XBrushes.Black, pola_str3[i], XStringFormats.Center);
                }

                #endregion

                #region siatka strony 4
                PdfPage str4 = document.AddPage();
                str4.Size = PageSize.A4;
                str4.Orientation = PageOrientation.Landscape;
                XGraphics gfx4 = XGraphics.FromPdfPage(str4);
                XRect Pole21 = new XRect(new XPoint(10, 20), new XSize(397.24, 188.46));
                XRect Pole22 = new XRect(new XPoint(Pole21.Left, Pole21.Bottom), new XSize(Pole21.Width, 199.03));
                XRect Pole23 = new XRect(new XPoint(Pole22.Left, Pole22.Bottom), new XSize(Pole22.Width, 129.46));
                XRect Pole24 = new XRect(new XPoint(Pole21.Right, Pole21.Top), new XSize(str4.Width - Pole21.Width, 137.38));
                XRect Pole25 = new XRect(new XPoint(Pole24.Left, Pole24.Bottom), new XSize(Pole24.Width, 215.76));
                XRect Pole26 = new XRect(new XPoint(Pole25.Left, Pole25.Bottom), new XSize(Pole25.Width, 94.23));
                XRect Pole27 = new XRect(new XPoint(Pole26.Left, Pole26.Bottom), new XSize(Pole26.Width, 67.81));
                XRect[] pola_str4 = new XRect[] { Pole21, Pole22, Pole23, Pole24, Pole25, Pole26, Pole27 };
                string[] nazwa_pola_s4 = new string[] { "21. Akta archiwalne (rodzaj akt, numer i miejsce przechowywania)", "22. Bibliografia", "23. Źródła ikonograficzne (rodzaj, miejsce przechowywania)", "24. Uwagi", "25. Adnotacje o inspekcjach, informacje o zmianach (daty imiona i nazwiska wypełniających)", "26. Opracowanie karty ewidencyjnej (autor, data i podpis)", "27. Załączniki" };
                string[] zaw_pola_s4 = new string[] {AktaTextBox.Text, BibliografiaTextBox.Text,ZrodlaIkoTextBox.Text,UwagiTextBox.Text,AdnotacjaTextBox.Text,OpracowanieTextBox.Text,ZalacznikiTextBox.Text };
                for (int i = 0; i < 7; i++)
                {
                    gfx4.DrawString(nazwa_pola_s4[i], hdrfont, XBrushes.Black, pola_str4[i], XStringFormats.TopLeft);
                    gfx4.DrawString(zaw_pola_s4[i], hdrfont, XBrushes.Black, pola_str4[i], XStringFormats.Center);
                }

                //poziome
                //kolumna1
                gfx4.DrawLine(pioro, new XPoint(10, 20), new XPoint(str4.Width - 10, 20));
                gfx4.DrawLine(pioro, new XPoint(10, 188.46 + 20), new XPoint(10 + 397.24, 188.46 + 20));
                gfx4.DrawLine(pioro, new XPoint(10, 188.46 + 20 + 199.03), new XPoint(10 + 397.24, 188.46 + 20 + 199.03));
                //kolumna2
                gfx4.DrawLine(pioro, new XPoint(10 + 397.24, 137.38 + 20), new XPoint(str4.Width - 10, 137.38 + 20));
                gfx4.DrawLine(pioro, new XPoint(10 + 397.24, 137.38 + 20 + 215.73), new XPoint(str4.Width - 10, 137.38 + 20 + 215.73));
                gfx4.DrawLine(pioro, new XPoint(10 + 397.24, 137.38 + 20 + 215.73 + 94.23), new XPoint(str4.Width - 10, 137.38 + 20 + 215.73 + 94.23));

                //pionowe
                gfx4.DrawLine(pioro, new XPoint(397.24 + 10, 20), new XPoint(397.24 + 10, str4.Height - 20));
                #endregion

                document.Save(ExportPDFDialog.FileName);
            }
        }

        private void ExportXLSDialog_FileOk(object sender, CancelEventArgs e)
        {
            FileInfo plikxls = new FileInfo(ExportXLSDialog.FileName);
            using (ExcelPackage excel = new ExcelPackage(plikxls))
            {
                //ePaperSize formatpapieru = ePaperSize.A4Transverse;
                ExcelWorksheet worksheet = excel.Workbook.Worksheets.Add("Inventory");
                ExcelPrinterSettings ustwydruku;
                
                excel.Save();
            }
        }
    }
}
