﻿namespace RejestrZabytkówMHKI
{
    partial class deleteCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Usun = new System.Windows.Forms.Button();
            this.UsunRazemZLiscmi = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CategoryName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Usun
            // 
            this.Usun.Location = new System.Drawing.Point(104, 102);
            this.Usun.Name = "Usun";
            this.Usun.Size = new System.Drawing.Size(75, 23);
            this.Usun.TabIndex = 0;
            this.Usun.Text = "Usuń kategorię";
            this.Usun.UseVisualStyleBackColor = true;
            this.Usun.Click += new System.EventHandler(this.Usun_Click);
            // 
            // UsunRazemZLiscmi
            // 
            this.UsunRazemZLiscmi.AutoSize = true;
            this.UsunRazemZLiscmi.Location = new System.Drawing.Point(194, 102);
            this.UsunRazemZLiscmi.Name = "UsunRazemZLiscmi";
            this.UsunRazemZLiscmi.Size = new System.Drawing.Size(98, 30);
            this.UsunRazemZLiscmi.TabIndex = 1;
            this.UsunRazemZLiscmi.Text = "Usuń razem z\npodkategoriami";
            this.UsunRazemZLiscmi.UseVisualStyleBackColor = true;
            this.UsunRazemZLiscmi.CheckedChanged += new System.EventHandler(this.UsunRazemZLiscmi_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Kategoria wybrana do usunięcia: ";
            // 
            // CategoryName
            // 
            this.CategoryName.AutoSize = true;
            this.CategoryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CategoryName.Location = new System.Drawing.Point(67, 69);
            this.CategoryName.Name = "CategoryName";
            this.CategoryName.Size = new System.Drawing.Size(0, 16);
            this.CategoryName.TabIndex = 3;
            // 
            // deleteCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 144);
            this.Controls.Add(this.CategoryName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UsunRazemZLiscmi);
            this.Controls.Add(this.Usun);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "deleteCategory";
            this.Text = "Usuń kategorię";
            this.Load += new System.EventHandler(this.deleteCategory_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Usun;
        private System.Windows.Forms.CheckBox UsunRazemZLiscmi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label CategoryName;
    }
}