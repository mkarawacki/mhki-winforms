﻿namespace RejestrZabytkówMHKI
{
    partial class GenerujQR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenerujQR));
            this.QRCode = new System.Windows.Forms.PictureBox();
            this.QRContents = new System.Windows.Forms.Label();
            this.ZapiszQR = new System.Windows.Forms.SaveFileDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.PrinPreviewBtn = new System.Windows.Forms.Button();
            this.SaveQRBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.QRCode)).BeginInit();
            this.SuspendLayout();
            // 
            // QRCode
            // 
            this.QRCode.Location = new System.Drawing.Point(77, 42);
            this.QRCode.Name = "QRCode";
            this.QRCode.Size = new System.Drawing.Size(256, 256);
            this.QRCode.TabIndex = 0;
            this.QRCode.TabStop = false;
            // 
            // QRContents
            // 
            this.QRContents.AutoSize = true;
            this.QRContents.Location = new System.Drawing.Point(198, 13);
            this.QRContents.Name = "QRContents";
            this.QRContents.Size = new System.Drawing.Size(0, 13);
            this.QRContents.TabIndex = 1;
            // 
            // ZapiszQR
            // 
            this.ZapiszQR.Filter = "Obraz w formacie JPG|*.jpg|Obraz w formacie PNG|*.png|Grafika wektorowa w formaci" +
    "e SVG|*.svg|Dokument PDF|*.pdf";
            this.ZapiszQR.FileOk += new System.ComponentModel.CancelEventHandler(this.ZapiszQR_FileOk);
            // 
            // pageSetupDialog1
            // 
            this.pageSetupDialog1.Document = this.printDocument1;
            this.pageSetupDialog1.ShowHelp = true;
            // 
            // printDocument1
            // 
            this.printDocument1.DocumentName = "";
            this.printDocument1.OriginAtMargins = true;
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.AllowSelection = true;
            this.printDialog1.UseEXDialog = true;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.UseAntiAlias = true;
            this.printPreviewDialog1.Visible = false;
            // 
            // PrintBtn
            // 
            this.PrintBtn.Location = new System.Drawing.Point(227, 326);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(75, 23);
            this.PrintBtn.TabIndex = 2;
            this.PrintBtn.Text = "Drukuj";
            this.PrintBtn.UseVisualStyleBackColor = true;
            this.PrintBtn.Click += new System.EventHandler(this.PrintBtn_Click);
            // 
            // PrinPreviewBtn
            // 
            this.PrinPreviewBtn.Location = new System.Drawing.Point(122, 326);
            this.PrinPreviewBtn.Name = "PrinPreviewBtn";
            this.PrinPreviewBtn.Size = new System.Drawing.Size(75, 23);
            this.PrinPreviewBtn.TabIndex = 3;
            this.PrinPreviewBtn.Text = "Podgląd wydruku";
            this.PrinPreviewBtn.UseVisualStyleBackColor = true;
            this.PrinPreviewBtn.Click += new System.EventHandler(this.PrinPreviewBtn_Click);
            // 
            // SaveQRBtn
            // 
            this.SaveQRBtn.Location = new System.Drawing.Point(308, 326);
            this.SaveQRBtn.Name = "SaveQRBtn";
            this.SaveQRBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveQRBtn.TabIndex = 4;
            this.SaveQRBtn.Text = "Zapisz";
            this.SaveQRBtn.UseVisualStyleBackColor = true;
            this.SaveQRBtn.Click += new System.EventHandler(this.SaveQRBtn_Click);
            // 
            // GenerujQR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 366);
            this.Controls.Add(this.SaveQRBtn);
            this.Controls.Add(this.PrinPreviewBtn);
            this.Controls.Add(this.PrintBtn);
            this.Controls.Add(this.QRContents);
            this.Controls.Add(this.QRCode);
            this.Name = "GenerujQR";
            this.Text = "GenerujQR";
            this.Load += new System.EventHandler(this.GenerujQR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.QRCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox QRCode;
        private System.Windows.Forms.Label QRContents;
        private System.Windows.Forms.SaveFileDialog ZapiszQR;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.Button PrinPreviewBtn;
        private System.Windows.Forms.Button SaveQRBtn;
    }
}