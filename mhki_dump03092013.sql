CREATE DATABASE  IF NOT EXISTS `mhki` /*!40100 DEFAULT CHARACTER SET cp1250 */;
USE `mhki`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: mhki
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `lewyprawy`
--

DROP TABLE IF EXISTS `lewyprawy`;
/*!50001 DROP VIEW IF EXISTS `lewyprawy`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `lewyprawy` (
  `wizyta` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `luki`
--

DROP TABLE IF EXISTS `luki`;
/*!50001 DROP VIEW IF EXISTS `luki`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `luki` (
  `poczatek` bigint(12),
  `koniec` bigint(12),
  `rozpr` bigint(22)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `mptt_al`
--

DROP TABLE IF EXISTS `mptt_al`;
/*!50001 DROP VIEW IF EXISTS `mptt_al`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mptt_al` (
  `id` int(11),
  `skr` varchar(45),
  `nazwa` varchar(255),
  `rodzic` bigint(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ostwizyta`
--

DROP TABLE IF EXISTS `ostwizyta`;
/*!50001 DROP VIEW IF EXISTS `ostwizyta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ostwizyta` (
  `wizyta` bigint(12)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pwizyta`
--

DROP TABLE IF EXISTS `pwizyta`;
/*!50001 DROP VIEW IF EXISTS `pwizyta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pwizyta` (
  `wizyta` bigint(12)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `rejestr`
--

DROP TABLE IF EXISTS `rejestr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rejestr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skr` varchar(45) NOT NULL,
  `lewy` int(11) NOT NULL,
  `prawy` int(11) NOT NULL,
  `nazwa` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=cp1250;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rejestr`
--

LOCK TABLES `rejestr` WRITE;
/*!40000 ALTER TABLE `rejestr` DISABLE KEYS */;
INSERT INTO `rejestr` VALUES (1,'MHKI',1,112,'Muzeum Historii Komputerów i Informatyki'),(2,'Komp',6,7,'Komputery'),(3,'Kons',8,9,'Konsole'),(4,'Opr',10,11,'Oprogramowanie'),(5,'Lit',12,13,'Literatura'),(6,'Dok',22,23,'Dokumentacja techniczna'),(7,'Akc',24,25,'Akcesoria'),(8,'Tk',26,27,'Telekomunikacja'),(11,'WeWy',14,21,'Urządzenia Wejścia/Wyjścia'),(13,'UObl',4,5,'Urządzenia Obliczeniowe'),(14,'NoP',2,3,'Nośniki Pamięci'),(15,'Druk',19,20,'Drukarki'),(16,'Ekr',17,18,'Ekrany'),(17,'Skan',15,16,'Skanery');
/*!40000 ALTER TABLE `rejestr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `haslo` varchar(255) NOT NULL,
  `rfid_hash` varchar(255) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `grupa` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COMMENT='Tabela uzytkownikow ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (0,'admin','admin@mhki.edu.pl','21232f297a57a5a743894a0e4a801fc3',NULL,NULL,'root');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'mhki'
--
/*!50003 DROP PROCEDURE IF EXISTS `X1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `X1`()
    DETERMINISTIC
WHILE EXISTS (SELECT * FROM luki)
	DO UPDATE rejestr
		SET prawy = CASE
					WHEN prawy > (SELECT MIN(poczatek) FROM luki)
					THEN prawy - 1 ELSE prawy 
				END,
			lewy = CASE
					WHEN lewy > (SELECT MIN(poczatek) FROM luki)
					THEN lewy - 1 ELSE lewy 
					END;
END WHILE */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `X2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `X2`()
    DETERMINISTIC
WHILE EXISTS (SELECT 
*
FROM luki)
DO UPDATE rejestr
SET prawy = CASE
WHEN prawy > (SELECT MIN(poczatek) FROM luki)
THEN prawy - 1 ELSE prawy END,
lewy = CASE
WHEN lewy > (SELECT MIN(poczatek) FROM luki)
THEN lewy - 1 ELSE lewy END;
END WHILE */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `lewyprawy`
--

/*!50001 DROP TABLE IF EXISTS `lewyprawy`*/;
/*!50001 DROP VIEW IF EXISTS `lewyprawy`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `lewyprawy` AS select `rejestr`.`lewy` AS `wizyta` from `rejestr` union select `rejestr`.`prawy` AS `prawy` from `rejestr` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `luki`
--

/*!50001 DROP TABLE IF EXISTS `luki`*/;
/*!50001 DROP VIEW IF EXISTS `luki`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `luki` AS select `a1`.`wizyta` AS `poczatek`,`l1`.`wizyta` AS `koniec`,((`l1`.`wizyta` - `a1`.`wizyta`) + 1) AS `rozpr` from (`pwizyta` `a1` join `ostwizyta` `l1`) where (`l1`.`wizyta` = (select min(`l2`.`wizyta`) from `ostwizyta` `l2` where (`a1`.`wizyta` <= `l2`.`wizyta`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mptt_al`
--

/*!50001 DROP TABLE IF EXISTS `mptt_al`*/;
/*!50001 DROP VIEW IF EXISTS `mptt_al`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `mptt_al` AS (select `a`.`id` AS `id`,`a`.`skr` AS `skr`,`a`.`nazwa` AS `nazwa`,if(isnull(`b`.`id`),0,`b`.`id`) AS `rodzic` from (`rejestr` `a` left join `rejestr` `b` on((`b`.`lewy` = (select max(`c`.`lewy`) from `rejestr` `c` where ((`a`.`lewy` > `c`.`lewy`) and (`a`.`lewy` < `c`.`prawy`))))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ostwizyta`
--

/*!50001 DROP TABLE IF EXISTS `ostwizyta`*/;
/*!50001 DROP VIEW IF EXISTS `ostwizyta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ostwizyta` AS select (`lewyprawy`.`wizyta` - 1) AS `wizyta` from `lewyprawy` where ((not((`lewyprawy`.`wizyta` - 1) in (select `lewyprawy`.`wizyta` from `lewyprawy`))) and ((`lewyprawy`.`wizyta` - 1) < (2 * (select count(0) from `lewyprawy`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pwizyta`
--

/*!50001 DROP TABLE IF EXISTS `pwizyta`*/;
/*!50001 DROP VIEW IF EXISTS `pwizyta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pwizyta` AS select (`lewyprawy`.`wizyta` + 1) AS `wizyta` from `lewyprawy` where ((not((`lewyprawy`.`wizyta` + 1) in (select `lewyprawy`.`wizyta` from `lewyprawy`))) and ((`lewyprawy`.`wizyta` + 1) > 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-03  7:48:42
