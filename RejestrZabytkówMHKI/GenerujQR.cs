﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;
using ZXing.QrCode.Internal;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp;
using WindowsFormsAero.TaskDialog;

namespace RejestrZabytkówMHKI
{
    public partial class GenerujQR : Form
    {
        private string sciezka, identyfikator,nazwa;
        private EncodingOptions EncodingOptions { get; set; }
        public GenerujQR(string path,string id,string expname)
        {
            sciezka = path;
            nazwa = expname;
            identyfikator = id;
            InitializeComponent();
            IBarcodeWriter qrwr = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = EncodingOptions ?? new EncodingOptions
                {
                    Height = 256,
                    Width = 256
                }
            };
            Bitmap qrbitmap;
            Hashtable hints = new Hashtable();

            hints.Add(EncodeHintType.ERROR_CORRECTION, ZXing.QrCode.Internal.ErrorCorrectionLevel.M);
            hints.Add("Version", "7");
            var result = qrwr.Write(sciezka + "\\" + identyfikator);
            qrbitmap = new Bitmap(result);
            QRCode.Image = qrbitmap;
            QRContents.Text = sciezka + "\\" + identyfikator;
            Console.WriteLine(sciezka + "\\" + identyfikator);
        }

        private void GenerujQR_Load(object sender, EventArgs e)
        {
            //ZXing.QrCode.QRCodeWriter qrwr = new ZXing.QrCode.QRCodeWriter();
           
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(QRCode.Image,100,100);
            Pen blackPen = new Pen(Color.Black,5);
            blackPen.DashStyle = DashStyle.Dash;
            Rectangle rect = new Rectangle(100,100,QRCode.Image.Width,QRCode.Image.Height);
            e.Graphics.DrawRectangle(blackPen, rect);
            e.Graphics.DrawString(sciezka + "\\" + identyfikator, new Font("Verdana", 16), new SolidBrush(Color.Black), 100, 50);
            e.Graphics.DrawString("Tnij tutaj", new Font("Consolas", 12), new SolidBrush(Color.Black), rect.Width/2 + 100,rect.Bottom+5);
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            //printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            try 
            { 
                printDocument1.Print(); 
            }
            catch(SystemException sysexc)
            {
                TaskDialog.Show(sysexc.Message);
            }
        }

        private void PrinPreviewBtn_Click(object sender, EventArgs e)
        {
            try 
            { 
                printPreviewDialog1.Show(); 
            }
            catch (SystemException sysexc) { TaskDialog.Show(sysexc.Message); }
        }

        private void SaveQRBtn_Click(object sender, EventArgs e)
        {
            ZapiszQR.Title = "Zapisz kod QR eksponatu";
            ZapiszQR.ShowDialog();
        }

        private void ZapiszQR_FileOk(object sender, CancelEventArgs e)
        {
            var nazwapliku = ZapiszQR.FileName;
            var extension = Path.GetExtension(nazwapliku).ToLower();
            var bmp = (Bitmap)QRCode.Image;
            switch (extension)
            {

                case ".pdf": 
                    {
                        using (PdfDocument QRdokument = new PdfDocument())
                        {
                            XFont font = new XFont("Arial CE", 12, XFontStyle.Bold);
                            XPen pioro = new XPen(XColors.Black, 1);
                                
                            PdfPage str1 = QRdokument.AddPage();
                            str1.Size = PageSize.A4;
                            str1.Orientation = PageOrientation.Portrait;
                            XGraphics gfx = XGraphics.FromPdfPage(str1);
                            gfx.DrawString("Pełny identyfikator eksponatu w rejestrze: "+sciezka + "\\" + identyfikator,new XFont("Arial",12),XBrushes.Black,new XPoint(50,80),XStringFormats.TopLeft);
                            XPen blackPen = new Pen(Color.Black, 5);
                            blackPen.DashStyle = XDashStyle.Dash;
                            XRect rect = new XRect((str1.Width - QRCode.Width) / 2, 100, QRCode.Image.Width, QRCode.Image.Height);
                            gfx.DrawRectangle(blackPen, rect);

                            gfx.DrawImage(QRCode.Image, new XRect(new XPoint((str1.Width  - QRCode.Width)/2, 100), new XSize(QRCode.Image.Width, QRCode.Image.Height)));



                            QRdokument.Save(nazwapliku);
                        }
                    
                    }
                    break;
                case ".jpg":
                  bmp.Save(nazwapliku, ImageFormat.Jpeg);
                  break;
               case ".svg":
                  {
                     var writer = new BarcodeWriterSvg
                                     {
                                        Format = BarcodeFormat.QR_CODE,
                                        Options = EncodingOptions ?? new EncodingOptions
                                                                        {
                                                                           Height = QRCode.Height,
                                                                           Width = QRCode.Width
                                                                        }
                                     };
                     var svgImage = writer.Write(sciezka + "\\" + identyfikator);
                     File.WriteAllText(nazwapliku, svgImage.Content, System.Text.Encoding.UTF8);
                  }
                  break;
               default:
                  bmp.Save(nazwapliku, ImageFormat.Png);
                  break;
            }
        }




    }
}
