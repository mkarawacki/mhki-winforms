﻿namespace RejestrZabytkówMHKI
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.regInv = new System.Windows.Forms.Button();
            this.regManager = new System.Windows.Forms.Button();
            this.regSearch = new System.Windows.Forms.Button();
            this.UserAdmBtn = new System.Windows.Forms.Button();
            this.LoggedInUserPanel = new System.Windows.Forms.Panel();
            this.ChangeAvatarLink = new System.Windows.Forms.LinkLabel();
            this.ChangePasswdLink = new System.Windows.Forms.LinkLabel();
            this.ImieNazwiskoLabel = new System.Windows.Forms.Label();
            this.AvatarPictureBox = new System.Windows.Forms.PictureBox();
            this.ChangePasswordPanel = new System.Windows.Forms.Panel();
            this.RepeatNewPasswd = new System.Windows.Forms.TextBox();
            this.NewPasswd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SaveNewPasswdBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.ChangeAvatarPanel = new System.Windows.Forms.Panel();
            this.SaveNewAvatar = new System.Windows.Forms.Button();
            this.AvatarPath = new System.Windows.Forms.TextBox();
            this.ChangeAvatarBtn = new System.Windows.Forms.Button();
            this.NewAvatar = new System.Windows.Forms.PictureBox();
            this.NewAvatarDialog = new System.Windows.Forms.OpenFileDialog();
            this.CancelAvatarChange = new System.Windows.Forms.Button();
            this.LoggedInUserPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).BeginInit();
            this.ChangePasswordPanel.SuspendLayout();
            this.ChangeAvatarPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NewAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // regInv
            // 
            this.regInv.FlatAppearance.BorderSize = 0;
            this.regInv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regInv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.regInv.Location = new System.Drawing.Point(516, 97);
            this.regInv.Name = "regInv";
            this.regInv.Size = new System.Drawing.Size(134, 82);
            this.regInv.TabIndex = 3;
            this.regInv.Text = "Inwentaryzacja";
            this.regInv.UseVisualStyleBackColor = true;
            this.regInv.Click += new System.EventHandler(this.regInv_Click);
            // 
            // regManager
            // 
            this.regManager.FlatAppearance.BorderSize = 0;
            this.regManager.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight;
            this.regManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.regManager.Image = global::RejestrZabytkówMHKI.Properties.Resources.MidnightCommander;
            this.regManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.regManager.Location = new System.Drawing.Point(287, 97);
            this.regManager.Name = "regManager";
            this.regManager.Size = new System.Drawing.Size(151, 82);
            this.regManager.TabIndex = 2;
            this.regManager.Text = "Zarządzaj\nrejestrem";
            this.regManager.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.regManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.regManager.UseVisualStyleBackColor = true;
            this.regManager.Click += new System.EventHandler(this.button2_Click);
            // 
            // regSearch
            // 
            this.regSearch.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.regSearch.FlatAppearance.BorderSize = 0;
            this.regSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.regSearch.Image = global::RejestrZabytkówMHKI.Properties.Resources.panel_searchtool;
            this.regSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.regSearch.Location = new System.Drawing.Point(77, 97);
            this.regSearch.Name = "regSearch";
            this.regSearch.Size = new System.Drawing.Size(137, 82);
            this.regSearch.TabIndex = 1;
            this.regSearch.Text = "Przeszukaj rejestr";
            this.regSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.regSearch.UseVisualStyleBackColor = false;
            this.regSearch.Click += new System.EventHandler(this.button1_Click);
            // 
            // UserAdmBtn
            // 
            this.UserAdmBtn.FlatAppearance.BorderSize = 0;
            this.UserAdmBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UserAdmBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UserAdmBtn.Location = new System.Drawing.Point(250, 200);
            this.UserAdmBtn.Name = "UserAdmBtn";
            this.UserAdmBtn.Size = new System.Drawing.Size(188, 82);
            this.UserAdmBtn.TabIndex = 5;
            this.UserAdmBtn.Text = "Zarządzanie Użytkownikami";
            this.UserAdmBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.UserAdmBtn.UseVisualStyleBackColor = true;
            this.UserAdmBtn.Click += new System.EventHandler(this.UserAdmBtn_Click);
            // 
            // LoggedInUserPanel
            // 
            this.LoggedInUserPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LoggedInUserPanel.BackColor = System.Drawing.Color.RoyalBlue;
            this.LoggedInUserPanel.Controls.Add(this.ChangeAvatarLink);
            this.LoggedInUserPanel.Controls.Add(this.ChangePasswdLink);
            this.LoggedInUserPanel.Controls.Add(this.ImieNazwiskoLabel);
            this.LoggedInUserPanel.Controls.Add(this.AvatarPictureBox);
            this.LoggedInUserPanel.Location = new System.Drawing.Point(592, 0);
            this.LoggedInUserPanel.Name = "LoggedInUserPanel";
            this.LoggedInUserPanel.Size = new System.Drawing.Size(184, 96);
            this.LoggedInUserPanel.TabIndex = 7;
            // 
            // ChangeAvatarLink
            // 
            this.ChangeAvatarLink.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ChangeAvatarLink.AutoSize = true;
            this.ChangeAvatarLink.DisabledLinkColor = System.Drawing.Color.Silver;
            this.ChangeAvatarLink.LinkColor = System.Drawing.Color.White;
            this.ChangeAvatarLink.Location = new System.Drawing.Point(8, 72);
            this.ChangeAvatarLink.Name = "ChangeAvatarLink";
            this.ChangeAvatarLink.Size = new System.Drawing.Size(127, 13);
            this.ChangeAvatarLink.TabIndex = 3;
            this.ChangeAvatarLink.TabStop = true;
            this.ChangeAvatarLink.Text = "Zmień obraz użytkownika";
            this.ChangeAvatarLink.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ChangeAvatarLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ChangeAvatarLink_LinkClicked);
            // 
            // ChangePasswdLink
            // 
            this.ChangePasswdLink.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ChangePasswdLink.AutoSize = true;
            this.ChangePasswdLink.DisabledLinkColor = System.Drawing.Color.Silver;
            this.ChangePasswdLink.LinkColor = System.Drawing.Color.White;
            this.ChangePasswdLink.Location = new System.Drawing.Point(8, 56);
            this.ChangePasswdLink.Name = "ChangePasswdLink";
            this.ChangePasswdLink.Size = new System.Drawing.Size(66, 13);
            this.ChangePasswdLink.TabIndex = 2;
            this.ChangePasswdLink.TabStop = true;
            this.ChangePasswdLink.Text = "Zmień hasło";
            this.ChangePasswdLink.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ChangePasswdLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ChangePasswdLink_LinkClicked);
            // 
            // ImieNazwiskoLabel
            // 
            this.ImieNazwiskoLabel.AutoSize = true;
            this.ImieNazwiskoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ImieNazwiskoLabel.Location = new System.Drawing.Point(8, 8);
            this.ImieNazwiskoLabel.Name = "ImieNazwiskoLabel";
            this.ImieNazwiskoLabel.Size = new System.Drawing.Size(0, 15);
            this.ImieNazwiskoLabel.TabIndex = 1;
            // 
            // AvatarPictureBox
            // 
            this.AvatarPictureBox.InitialImage = global::RejestrZabytkówMHKI.Properties.Resources.MidnightCommander;
            this.AvatarPictureBox.Location = new System.Drawing.Point(112, 8);
            this.AvatarPictureBox.Name = "AvatarPictureBox";
            this.AvatarPictureBox.Size = new System.Drawing.Size(64, 64);
            this.AvatarPictureBox.TabIndex = 0;
            this.AvatarPictureBox.TabStop = false;
            // 
            // ChangePasswordPanel
            // 
            this.ChangePasswordPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ChangePasswordPanel.Controls.Add(this.RepeatNewPasswd);
            this.ChangePasswordPanel.Controls.Add(this.NewPasswd);
            this.ChangePasswordPanel.Controls.Add(this.label2);
            this.ChangePasswordPanel.Controls.Add(this.label1);
            this.ChangePasswordPanel.Controls.Add(this.SaveNewPasswdBtn);
            this.ChangePasswordPanel.Controls.Add(this.CancelBtn);
            this.ChangePasswordPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChangePasswordPanel.Location = new System.Drawing.Point(0, 0);
            this.ChangePasswordPanel.Name = "ChangePasswordPanel";
            this.ChangePasswordPanel.Size = new System.Drawing.Size(775, 338);
            this.ChangePasswordPanel.TabIndex = 8;
            this.ChangePasswordPanel.Visible = false;
            this.ChangePasswordPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ChangePasswordPanel_Paint);
            // 
            // RepeatNewPasswd
            // 
            this.RepeatNewPasswd.Location = new System.Drawing.Point(328, 144);
            this.RepeatNewPasswd.Name = "RepeatNewPasswd";
            this.RepeatNewPasswd.Size = new System.Drawing.Size(208, 20);
            this.RepeatNewPasswd.TabIndex = 5;
            // 
            // NewPasswd
            // 
            this.NewPasswd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NewPasswd.Location = new System.Drawing.Point(328, 88);
            this.NewPasswd.Name = "NewPasswd";
            this.NewPasswd.Size = new System.Drawing.Size(208, 22);
            this.NewPasswd.TabIndex = 4;
            this.NewPasswd.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(168, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Powtórz nowe hasło";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(168, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nowe hasło";
            // 
            // SaveNewPasswdBtn
            // 
            this.SaveNewPasswdBtn.Location = new System.Drawing.Point(288, 280);
            this.SaveNewPasswdBtn.Name = "SaveNewPasswdBtn";
            this.SaveNewPasswdBtn.Size = new System.Drawing.Size(75, 31);
            this.SaveNewPasswdBtn.TabIndex = 1;
            this.SaveNewPasswdBtn.Text = "Zapisz";
            this.SaveNewPasswdBtn.UseVisualStyleBackColor = true;
            this.SaveNewPasswdBtn.Click += new System.EventHandler(this.SaveNewPasswdBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(384, 280);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(80, 32);
            this.CancelBtn.TabIndex = 0;
            this.CancelBtn.Text = "Anuluj";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // ChangeAvatarPanel
            // 
            this.ChangeAvatarPanel.Controls.Add(this.CancelAvatarChange);
            this.ChangeAvatarPanel.Controls.Add(this.SaveNewAvatar);
            this.ChangeAvatarPanel.Controls.Add(this.AvatarPath);
            this.ChangeAvatarPanel.Controls.Add(this.ChangeAvatarBtn);
            this.ChangeAvatarPanel.Controls.Add(this.NewAvatar);
            this.ChangeAvatarPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChangeAvatarPanel.Location = new System.Drawing.Point(0, 0);
            this.ChangeAvatarPanel.Name = "ChangeAvatarPanel";
            this.ChangeAvatarPanel.Size = new System.Drawing.Size(775, 338);
            this.ChangeAvatarPanel.TabIndex = 9;
            // 
            // SaveNewAvatar
            // 
            this.SaveNewAvatar.Location = new System.Drawing.Point(352, 216);
            this.SaveNewAvatar.Name = "SaveNewAvatar";
            this.SaveNewAvatar.Size = new System.Drawing.Size(75, 23);
            this.SaveNewAvatar.TabIndex = 3;
            this.SaveNewAvatar.Text = "Zapisz";
            this.SaveNewAvatar.UseVisualStyleBackColor = true;
            this.SaveNewAvatar.Click += new System.EventHandler(this.SaveNewAvatar_Click);
            // 
            // AvatarPath
            // 
            this.AvatarPath.Location = new System.Drawing.Point(320, 128);
            this.AvatarPath.Name = "AvatarPath";
            this.AvatarPath.Size = new System.Drawing.Size(140, 20);
            this.AvatarPath.TabIndex = 2;
            // 
            // ChangeAvatarBtn
            // 
            this.ChangeAvatarBtn.Location = new System.Drawing.Point(480, 120);
            this.ChangeAvatarBtn.Name = "ChangeAvatarBtn";
            this.ChangeAvatarBtn.Size = new System.Drawing.Size(88, 40);
            this.ChangeAvatarBtn.TabIndex = 1;
            this.ChangeAvatarBtn.Text = "Zmień obrazek konta";
            this.ChangeAvatarBtn.UseVisualStyleBackColor = true;
            this.ChangeAvatarBtn.Click += new System.EventHandler(this.ChangeAvatarBtn_Click);
            // 
            // NewAvatar
            // 
            this.NewAvatar.Location = new System.Drawing.Point(360, 48);
            this.NewAvatar.Name = "NewAvatar";
            this.NewAvatar.Size = new System.Drawing.Size(64, 64);
            this.NewAvatar.TabIndex = 0;
            this.NewAvatar.TabStop = false;
            // 
            // CancelAvatarChange
            // 
            this.CancelAvatarChange.Location = new System.Drawing.Point(440, 216);
            this.CancelAvatarChange.Name = "CancelAvatarChange";
            this.CancelAvatarChange.Size = new System.Drawing.Size(75, 23);
            this.CancelAvatarChange.TabIndex = 4;
            this.CancelAvatarChange.Text = "Anuluj";
            this.CancelAvatarChange.UseVisualStyleBackColor = true;
            this.CancelAvatarChange.Click += new System.EventHandler(this.CancelAvatarChange_Click);
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(775, 338);
            this.Controls.Add(this.ChangeAvatarPanel);
            this.Controls.Add(this.ChangePasswordPanel);
            this.Controls.Add(this.LoggedInUserPanel);
            this.Controls.Add(this.UserAdmBtn);
            this.Controls.Add(this.regSearch);
            this.Controls.Add(this.regInv);
            this.Controls.Add(this.regManager);
            this.Name = "ControlPanel";
            this.Text = "Rejestr Zabytków Muzeum Historii Komputerów i Informatyki - Panel kontrolny";
            this.Load += new System.EventHandler(this.ControlPanel_Load);
            this.LoggedInUserPanel.ResumeLayout(false);
            this.LoggedInUserPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).EndInit();
            this.ChangePasswordPanel.ResumeLayout(false);
            this.ChangePasswordPanel.PerformLayout();
            this.ChangeAvatarPanel.ResumeLayout(false);
            this.ChangeAvatarPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NewAvatar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button regSearch;
        private System.Windows.Forms.Button regManager;
        private System.Windows.Forms.Button regInv;
        private System.Windows.Forms.Button UserAdmBtn;
        private System.Windows.Forms.Panel LoggedInUserPanel;
        private System.Windows.Forms.LinkLabel ChangePasswdLink;
        private System.Windows.Forms.Label ImieNazwiskoLabel;
        private System.Windows.Forms.PictureBox AvatarPictureBox;
        private System.Windows.Forms.LinkLabel ChangeAvatarLink;
        private System.Windows.Forms.Panel ChangePasswordPanel;
        private System.Windows.Forms.Button SaveNewPasswdBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.TextBox RepeatNewPasswd;
        private System.Windows.Forms.TextBox NewPasswd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel ChangeAvatarPanel;
        private System.Windows.Forms.Button ChangeAvatarBtn;
        private System.Windows.Forms.PictureBox NewAvatar;
        private System.Windows.Forms.OpenFileDialog NewAvatarDialog;
        private System.Windows.Forms.TextBox AvatarPath;
        private System.Windows.Forms.Button SaveNewAvatar;
        private System.Windows.Forms.Button CancelAvatarChange;
    }
}