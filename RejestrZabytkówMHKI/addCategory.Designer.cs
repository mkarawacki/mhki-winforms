﻿namespace RejestrZabytkówMHKI
{
    partial class addCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newCatName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.eqCat = new System.Windows.Forms.RadioButton();
            this.subCat = new System.Windows.Forms.RadioButton();
            this.eqCatLabel = new System.Windows.Forms.Label();
            this.subCatLabel = new System.Windows.Forms.Label();
            this.Abbr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // newCatName
            // 
            this.newCatName.Location = new System.Drawing.Point(124, 21);
            this.newCatName.Name = "newCatName";
            this.newCatName.Size = new System.Drawing.Size(131, 20);
            this.newCatName.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(107, 135);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // eqCat
            // 
            this.eqCat.AutoSize = true;
            this.eqCat.Location = new System.Drawing.Point(43, 81);
            this.eqCat.Name = "eqCat";
            this.eqCat.Size = new System.Drawing.Size(144, 17);
            this.eqCat.TabIndex = 2;
            this.eqCat.TabStop = true;
            this.eqCat.Text = "jako równorzędną wobec";
            this.eqCat.UseVisualStyleBackColor = true;
            this.eqCat.CheckedChanged += new System.EventHandler(this.eqCat_CheckedChanged);
            // 
            // subCat
            // 
            this.subCat.AutoSize = true;
            this.subCat.Location = new System.Drawing.Point(43, 104);
            this.subCat.Name = "subCat";
            this.subCat.Size = new System.Drawing.Size(127, 17);
            this.subCat.TabIndex = 3;
            this.subCat.TabStop = true;
            this.subCat.Text = "jako podkategorię dla";
            this.subCat.UseVisualStyleBackColor = true;
            this.subCat.CheckedChanged += new System.EventHandler(this.subCat_CheckedChanged);
            // 
            // eqCatLabel
            // 
            this.eqCatLabel.AutoSize = true;
            this.eqCatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.eqCatLabel.Location = new System.Drawing.Point(187, 81);
            this.eqCatLabel.Name = "eqCatLabel";
            this.eqCatLabel.Size = new System.Drawing.Size(0, 15);
            this.eqCatLabel.TabIndex = 4;
            // 
            // subCatLabel
            // 
            this.subCatLabel.AutoSize = true;
            this.subCatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.subCatLabel.Location = new System.Drawing.Point(170, 106);
            this.subCatLabel.Name = "subCatLabel";
            this.subCatLabel.Size = new System.Drawing.Size(0, 15);
            this.subCatLabel.TabIndex = 5;
            // 
            // Abbr
            // 
            this.Abbr.Location = new System.Drawing.Point(124, 58);
            this.Abbr.Name = "Abbr";
            this.Abbr.Size = new System.Drawing.Size(131, 20);
            this.Abbr.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nazwa nowej kategorii";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Skrót nazwy";
            // 
            // addCategory
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 342);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Abbr);
            this.Controls.Add(this.subCatLabel);
            this.Controls.Add(this.eqCatLabel);
            this.Controls.Add(this.subCat);
            this.Controls.Add(this.eqCat);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.newCatName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "addCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodaj kategorię";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.Load += new System.EventHandler(this.addCategory_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox newCatName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton eqCat;
        private System.Windows.Forms.RadioButton subCat;
        private System.Windows.Forms.Label eqCatLabel;
        private System.Windows.Forms.Label subCatLabel;
        private System.Windows.Forms.TextBox Abbr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}