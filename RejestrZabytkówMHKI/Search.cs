﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data.Common;
using Renci.SshNet.Common;
using Renci.SshNet;
//using WindowsFormsAero;
using System.Runtime.InteropServices;
using System.Xml;
using System.Configuration;
namespace RejestrZabytkówMHKI
{
    public partial class Search : Form
    {
        string nazwa, typ, producent, rok;
        bool TypZazn, NazwaZazn, ProducentZazn, RokZazn;
        private string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();
         /*
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern void DwmExtendFrameIntoClientArea(
            IntPtr hWnd, MARGINS pMargins);

        [StructLayout(LayoutKind.Sequential)]
        public class MARGINS
        {
            public int cxLeftWidth, cxRightWidth,
                       cyTopHeight, cyBottomHeight;
        }
        */
        public Search()
        {
            InitializeComponent();
            autouzupelnianie();
            
        }
        
        private string AdvancedSearchQuery() 
        {
            string query="";
            if (NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && !(ProducentZazn || TypZazn || RokZazn))
            {
                query = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text + "%'";
            }
            if (NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && !(ProducentZazn || RokZazn))
            {
                query = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text + "%' AND kategoria LIKE '%" + TypSearchBox.Text + "%'";
            }
            if (NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && !(TypZazn || RokZazn))
            {
                query = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text + "%' AND producent LIKE '%"+ProducentSearchBox.Text+"%'";
            }
            if (NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && RokZazn && !String.IsNullOrEmpty(rok) && !(ProducentZazn || TypZazn) ) 
            {
                query ="SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text +"%' AND czaspowstania='"+rok+"'";
            }
            if (NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && !RokZazn)
            {
                query = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text + "%' AND kategoria LIKE '%" + TypSearchBox.Text + "%' AND producent LIKE '%" + ProducentSearchBox.Text + "%'";
            }
            if (NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && RokZazn && !String.IsNullOrEmpty(rok) && !ProducentZazn)
            {
                query = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text + "%' AND kategoria LIKE '%" + TypSearchBox.Text + "%' AND czaspowstania='"+rok+"'";
            }
            if(NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && RokZazn && !String.IsNullOrEmpty(rok) && !TypZazn)
            {
                query = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text + "%' AND producent LIKE '%" + ProducentSearchBox.Text + "%' AND czaspowstania='" + rok + "'";
            }
            if (NazwaZazn && !String.IsNullOrEmpty(NazwaSearchBox.Text) && TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && RokZazn && !String.IsNullOrEmpty(rok))
            {
                query = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + NazwaSearchBox.Text + "%' AND kategoria LIKE '%" + TypSearchBox.Text + "%' AND producent LIKE '%" + ProducentSearchBox.Text + "%' AND czaspowstania='" + rok + "'";
            }
            //2
            if (TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && ! (NazwaZazn || ProducentZazn || RokZazn)) 
            {
                query = "SELECT * FROM eksponaty WHERE kategoria LIKE '%" + TypSearchBox.Text + "%'";
            }
            if (TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && !(NazwaZazn || RokZazn))
            {
                query = "SELECT * FROM eksponaty WHERE kategoria LIKE '%" + TypSearchBox.Text + "%' AND producent LIKE '%" + ProducentSearchBox.Text + "%'";
            }
            if (TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && RokZazn && !String.IsNullOrEmpty(rok) && !(NazwaZazn || ProducentZazn))
            {
                query = "SELECT * FROM eksponaty WHERE kategoria LIKE '%" + TypSearchBox.Text + "%' AND czaspowstania='" + rok + "'";
            }
            if (TypZazn && !String.IsNullOrEmpty(TypSearchBox.Text) && ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && RokZazn && !String.IsNullOrEmpty(rok) && !NazwaZazn)
            {
                query = "SELECT * FROM eksponaty WHERE kategoria LIKE '%" + TypSearchBox.Text + "%' AND producent LIKE '%" + ProducentSearchBox.Text + "%' AND czaspowstania='" + rok + "'";
            }
            //3
            if (ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && !(NazwaZazn || TypZazn || RokZazn)) 
            {
                query = "SELECT * FROM eksponaty WHERE producent LIKE '%" + ProducentSearchBox.Text + "%'";
            }
            if (ProducentZazn && !String.IsNullOrEmpty(ProducentSearchBox.Text) && RokZazn && !String.IsNullOrEmpty(rok) && !(NazwaZazn || TypZazn)) 
            {
                query = "SELECT * FROM eksponaty WHERE producent LIKE '%" + ProducentSearchBox.Text + "%' AND czaspowstania='"+rok+"'";
            }
           
            //4
            if(RokZazn && !String.IsNullOrEmpty(rok) && !(NazwaZazn || TypZazn || ProducentZazn))
            {
                query = "SELECT * FROM eksponaty WHERE czaspowstania='" + rok + "'";
            }
            Console.WriteLine(query);
            return query;




        }

        private void BasicSearchQuery()
        {
            string query = "";
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void autouzupelnianie() 
        {
            searchbox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            searchbox1.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection zbiorpodp = new AutoCompleteStringCollection();
            MySqlConnection link = new MySqlConnection(danepolaczenia);
            MySqlCommand podpowiedzi = new MySqlCommand("SELECT nazwa, kategoria FROM eksponaty",link);
            //podpowiedzi.CommandText = "SELECT nazwa, kategoria,danefirmowe,czaspowstania FROM eksponaty";
            MySqlDataReader dataread;
            try 
            {
                link.Open();
                dataread = podpowiedzi.ExecuteReader();
                while (dataread.Read()) 
                {
                    for (int i = 0; i < dataread.FieldCount;i++ ) 
                    {
                        string podp = dataread.GetString(i);
                        if(!String.IsNullOrEmpty(podp)){zbiorpodp.Add(podp);}
                    }
                }
            }
            catch (MySqlException dberror)
            {
                MessageBox.Show(dberror.Message);
            }
            searchbox1.AutoCompleteCustomSource = zbiorpodp;

        }
        private void searchbox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Search_Load(object sender, EventArgs e)
        {
            /*this.BackColor = Color.Black;
            MARGINS m = new MARGINS();
            m.cxLeftWidth = 20;
            m.cxRightWidth = 20;
            m.cyBottomHeight = -1;
            m.cyTopHeight = 10;
            //set all value -1 to apply glass effect to the all of visible window
            DwmExtendFrameIntoClientArea(this.Handle, m);*/
        }

        private void AdvancedSearch_Click(object sender, EventArgs e)
        {
            //szukaj();
        }
        private void AdvancedSearch_Enter(object sender, EventArgs e) 
        {
            NazwaSearchBox.Enabled = false;
            TypSearchBox.Enabled = false;
            ProducentSearchBox.Enabled = false;
            RokProdCombo.Enabled = false;            
        }

        private void NazwaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (NazwaCheckBox.Checked)
            {
                NazwaSearchBox.Enabled = NazwaZazn =true;
                
            }
            else NazwaZazn = NazwaSearchBox.Enabled = false;
        }

        private void TypCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (TypCheckBox.Checked) TypSearchBox.Enabled = TypZazn = true;
            else TypSearchBox.Enabled = TypZazn = false;
        }

        private void ProducentCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ProducentCheckBox.Checked) ProducentSearchBox.Enabled =ProducentZazn= true;
            else ProducentSearchBox.Enabled = ProducentZazn = false;
        }

        private void RokProdCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (RokProdCheckBox.Checked) RokProdCombo.Enabled =RokZazn= true;
            else RokProdCombo.Enabled = RokZazn = false;
        }

        private void AdvSearchButton_Click(object sender, EventArgs e)
        {
            
            if (!NazwaZazn && !TypZazn && !ProducentZazn && !RokZazn)
            {
                MessageBox.Show("Proszę wybrać któryś z filtrów", "Błąd");
            }
            string zapytanie = AdvancedSearchQuery();
            MySqlConnection link = new MySqlConnection(danepolaczenia);
            MySqlCommand advquery = new MySqlCommand(zapytanie,link);
            
            MySqlDataAdapter advda = new MySqlDataAdapter(advquery);
            DataTable advdt = new DataTable();
            advda.Fill(advdt);
            WynikiGrid.DataSource = advdt;
        }

        private void searchbutton1_Click(object sender, EventArgs e)
        {
            //BasicSearchQuery();
            
            string input = searchbox1.Text;
            MySqlConnection link = new MySqlConnection(danepolaczenia);
            string searchcmd = "SELECT * FROM eksponaty WHERE nazwa LIKE '%" + input + "%' OR producent LIKE '%" + input + "%' OR opis LIKE '%" + input + "%'";
            MySqlCommand search = new MySqlCommand(searchcmd, link);
            MySqlDataAdapter search_da = new MySqlDataAdapter(search);
            DataTable search_dt = new DataTable();
            search_da.Fill(search_dt);
            WynikiGrid.DataSource = search_dt;
        }

        

        private void RokProdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            rok = (string)RokProdCombo.SelectedItem;
            Console.WriteLine(rok);
        }
        
    }
}
