﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RejestrZabytkówMHKI
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            
            InitializeComponent();
        }

        private void progressBar_Click(object sender, EventArgs e)
        {

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            progressBar.Increment(1);
            if (progressBar.Value == 100) timer.Stop();
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            this.SetStyle(System.Windows.Forms.ControlStyles.SupportsTransparentBackColor,true);
            this.BackColor = System.Drawing.Color.White;
            this.TransparencyKey = Color.White;
        }
    }
}
