﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ZXing.QrCode;
using WindowsFormsAero;
using WindowsFormsAero.TaskDialog;
using MySql.Data.MySqlClient;
namespace RejestrZabytkówMHKI
{
    public partial class ZmienEksponat : Form
    {
        private string nazwakategorii, sciezka, rok, wojewodztwo,id,stan;
        string[] defaddress = { "Katowice", "", "Katowice", "śląskie" };
        DateTime datadzis;

        /// <summary>
        /// Konstruktor okna "Zmień dane eksponatu" wywoływany z identyfikatorem eksponatu, 
        /// nazwą kategorii, do której należy i  położeniem eksponatu w drzewie rejestru
        /// </summary>
        /// <param name="identyfikator">identyfikator eksponatu</param>
        /// <param name="catname"> nazwa kategorii, do której należy eksponat</param>
        /// <param name="path">położenie eksponatu w drzewie kategorii rejestru</param>
        public ZmienEksponat(string identyfikator,string catname,string path)
        {
            nazwakategorii = catname;
            sciezka = path;
            id = identyfikator;
            InitializeComponent();
            wstawPobraneNID();
            WypelnijPictureBox();
           // Path.Text = sciezka;
          //  NodeValue.Text = nazwakategorii;
            //NodeName.Text = nazwakategorii;
           // NodeValue.BackColor = Color.Azure;
            ExpIDTB.Enabled = false;
            AdresGroupBox.Visible = false;
            StanCombo.Enabled = false;
            datadzis = DateTime.Today;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void WypelnijPictureBox()
        {
            MySqlConnection polaczenie1 = new MySqlConnection(login.danepolaczenia);
            string pobierzobraz = "SELECT matgraficzne FROM eksponaty WHERE identyfikator='" + id + "'";
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(new MySqlCommand(pobierzobraz, polaczenie1));
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count == 1)
            {
                
                Byte[] data = new Byte[0];
                data = (Byte[])(dataSet.Tables[0].Rows[0]["matgraficzne"]);
                MemoryStream mem = new MemoryStream(data);
                MatGrafPB.Image = Image.FromStream(mem);
            }
        }


        private string pobierzDaneNID() 
        {
            string query = "SELECT `identyfikator`, `nazwa`, `kategoria`, `czaspowstania`, `matpodstawowy`, `danefirmowe`, `wymiary`, `ciezar`, `ilosc`, `wlasciciel`, `uzytkownik`, `mscepracy`, `udostepnianie`, `ochrona`, `historia`, `opis`, `pierwprzeznaczenie`, `obuzytkowanie`, `remonty`, `stan`, `akta`, `bibliografia`, `ikonografia`, `uwagi`, `inspekcje`, `oprkarty`, `zalaczniki` FROM eksponaty WHERE `identyfikator`='"+id+"'";
            return query;
        }

        private void wstawPobraneNID() 
        {
            string[] polaDoWyp = new string[] { ExpIDTB.Text, NazwaTB.Text, nazwakategorii, rok, MatPodstTB.Text, DaneFrmTB.Text,DimDlTB.Text,  DimSzerTB.Text,  DimWysTB.Text, CiezarTB.Text, IloscTB.Text, MiejscowoscTB.Text, GminaTB.Text,  PowiatTB.Text,  wojewodztwo, WlascicielTB.Text, UzytkownikTB.Text, MscePcyTB.Text, UdstTB.Text, FormyOchrTB.Text, HistoriaTB.Text, OpisTB.Text, PrzeznPierwTB.Text, UzObecneTB.Text, RemontyTB.Text, StanTB.Text, AktaArchTB.Text, BibGrfTB.Text, ZrodlaIkoTB.Text, UwagiTB.Text, AdnInspTB.Text, OprTekstTB.Text,  PlanyOprTB.Text, FotoOprTB.Text, ZalacznikiTB.Text };
            string[] kolumny = new string[] {"identyfikator", "nazwa", "kategoria", "czaspowstania", "matpodstawowy", "danefirmowe", "wymiary", "ciezar", "ilosc",  "wlasciciel", "uzytkownik", "mscepracy", "udostepnianie", "ochrona", "historia", "opis", "pierwprzeznaczenie", "obuzytkowanie", "remonty", "stan", "akta", "bibliografia", "ikonografia", "uwagi", "inspekcje", "oprkarty", "zalaczniki"};
           
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            MySqlCommand readcmd = new MySqlCommand(pobierzDaneNID(), link);
            Console.WriteLine(pobierzDaneNID());
            MySqlDataAdapter readDA = new MySqlDataAdapter(readcmd);
            DataTable readDT = new DataTable();
            readDA.Fill(readDT);
            for (int i = 0; i < readDT.Columns.Count; i++) 
            {
                
                polaDoWyp[i]=readDT.Rows[0][i].ToString();
            }
            if (!String.IsNullOrEmpty(polaDoWyp[0]))
                ExpIDTB.Text = polaDoWyp[0];
            else ExpIDTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[1]))
                NazwaTB.Text = polaDoWyp[1];
            else polaDoWyp[1] = "";
            if (!String.IsNullOrEmpty(polaDoWyp[2]))
                nazwakategorii = polaDoWyp[2];
            else nazwakategorii = "";
            if (!String.IsNullOrEmpty(polaDoWyp[3]))
                YearUpDown.Value = Convert.ToUInt16(polaDoWyp[3]);
            else YearUpDown.Value = 1950;
            if (!String.IsNullOrEmpty(polaDoWyp[4]))
                MatPodstTB.Text = polaDoWyp[4];
            else MatPodstTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[5]))
                DaneFrmTB.Text = polaDoWyp[5];
            else DaneFrmTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[6]))
            {
                DimDlTB.Text = polaDoWyp[6].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[0];
                DimSzerTB.Text = polaDoWyp[6].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[1];
                DimWysTB.Text = polaDoWyp[6].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[2];
            }
            else DimDlTB.Text = DimSzerTB.Text = DimWysTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[7]))
                CiezarTB.Text = polaDoWyp[7];
            else CiezarTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[8]))
                IloscTB.Text = polaDoWyp[8];
            else IloscTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[9]))
            {
                MiejscowoscTB.Text = polaDoWyp[9].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[0];
                GminaTB.Text = polaDoWyp[9].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[1];
                PowiatTB.Text = polaDoWyp[9].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[2];
                WojewodztwoCB.SelectedItem = polaDoWyp[9].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[3];
            }
            else 
            {
                MiejscowoscTB.Text = GminaTB.Text = PowiatTB.Text = ""; WojewodztwoCB.SelectedItem = "śląskie"; 
            }
            if (!String.IsNullOrEmpty(polaDoWyp[10]))
                WlascicielTB.Text = polaDoWyp[10];
            else WlascicielTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[11]))
                UzytkownikTB.Text = polaDoWyp[11];
            else UzytkownikTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[12]))
                MscePcyTB.Text = polaDoWyp[12];
            else MscePcyTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[13]))
                UdstTB.Text = polaDoWyp[13];
            else UdstTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[14])) FormyOchrTB.Text = polaDoWyp[14];
            else FormyOchrTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[15])) HistoriaTB.Text = polaDoWyp[15];
            else HistoriaTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[16])) OpisTB.Text = polaDoWyp[16];
            else OpisTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[17])) PrzeznPierwTB.Text = polaDoWyp[17];
            else PrzeznPierwTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[18])) UzObecneTB.Text = polaDoWyp[18];
            else UzObecneTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[19])) RemontyTB.Text = polaDoWyp[19];
            else RemontyTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[20])) StanTB.Text = polaDoWyp[20];
            else StanTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[21])) AktaArchTB.Text = polaDoWyp[21];
            else AktaArchTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[22])) BibGrfTB.Text = polaDoWyp[22];
            else BibGrfTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[23])) ZrodlaIkoTB.Text = polaDoWyp[23];
            else ZrodlaIkoTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[24])) UwagiTB.Text = polaDoWyp[24];
            else UwagiTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[25])) AdnInspTB.Text = polaDoWyp[25];
            else AdnInspTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[26]))
            {
                OprTekstTB.Text = polaDoWyp[26].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[0];
                PlanyOprTB.Text = polaDoWyp[26].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[0];
                FotoOprTB.Text = polaDoWyp[26].Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)[0];
            }
            else OprTekstTB.Text = PlanyOprTB.Text = FotoOprTB.Text = "";
            if (!String.IsNullOrEmpty(polaDoWyp[27])) ZalacznikiTB.Text = polaDoWyp[27];
            else ZalacznikiTB.Text = "";
        }

        private bool sprawdzpolaNID()
        {
            System.Windows.Forms.TextBox[] tablicapol = new System.Windows.Forms.TextBox[] { };

            if (String.IsNullOrEmpty(NazwaTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Nazwa eksponatu\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                NazwaTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(ExpIDTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Identyfikator eksponatu\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                ExpIDTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(rok))
            {
                TaskDialog.Show("Wypełnij pole \"Czas powstania\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                YearUpDown.Focus(); return false;
            }
            if (String.IsNullOrEmpty(MatPodstTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Materiał podstawowy\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                MatPodstTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(DaneFrmTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Dane firmowe\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                DaneFrmTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(DimDlTB.Text) || String.IsNullOrEmpty(DimSzerTB.Text) || String.IsNullOrEmpty(DimWysTB.Text))
            {
                TaskDialog.Show("Wypełnij wszystkie pola rubryki \"Wymiary\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                if (String.IsNullOrEmpty(DimDlTB.Text)) { DimDlTB.Focus(); return false; }
                if (String.IsNullOrEmpty(DimSzerTB.Text)) { DimSzerTB.Focus(); return false; }
                if (String.IsNullOrEmpty(DimWysTB.Text)) { DimWysTB.Focus(); return false; }
            }
            if (String.IsNullOrEmpty(CiezarTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Ciężar\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                CiezarTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(IloscTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Ilość\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                IloscTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(ZdjecieSciezkaTB.Text))
            {
                TaskDialog.Show("Wstaw zdjęcie", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                ZdjecieDodajBtn.Focus(); return false;
            }
            if (String.IsNullOrEmpty(MiejscowoscTB.Text) || String.IsNullOrEmpty(GminaTB.Text) || String.IsNullOrEmpty(PowiatTB.Text) || String.IsNullOrEmpty(wojewodztwo))
            {
                TaskDialog.Show("Wypełnij wszystkie pola rubryki \"Adres\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                if (String.IsNullOrEmpty(MiejscowoscTB.Text)) { MiejscowoscTB.Focus(); return false; }
                if (String.IsNullOrEmpty(GminaTB.Text)) { GminaTB.Focus(); return false; }
                if (String.IsNullOrEmpty(PowiatTB.Text)) { PowiatTB.Focus(); return false; }
                if (String.IsNullOrEmpty(wojewodztwo)) { WojewodztwoCB.Focus(); return false; }

            }
            if (String.IsNullOrEmpty(WlascicielTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Właściciel i jego adres\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                WlascicielTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(UzytkownikTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Użytkownik i jego adres\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                UzytkownikTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(MscePcyTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Miejsce pracy (przechowywania)\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                MscePcyTB.Focus(); return false;
            }

            if (String.IsNullOrEmpty(UdstTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Udostępnianie\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                UdstTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(FormyOchrTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Formy ochrony\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                FormyOchrTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(HistoriaTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Historia obiektu\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                HistoriaTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(OpisTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Opis i charakterystyka techniczna\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                OpisTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(PrzeznPierwTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Przeznaczenie pierwotne\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                PrzeznPierwTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(UzObecneTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Użytkowanie obecne\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                UzObecneTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(RemontyTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Remonty, zmiany konstrukcyjne, modernizacje\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                RemontyTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(StanTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Stan zachowania o potrzeby konserwatorskie\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                StanTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(AktaArchTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Akta archiwalne\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                AktaArchTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(BibGrfTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Bibliografia\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                BibGrfTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(ZrodlaIkoTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Źródła ikonograficzne\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                ZrodlaIkoTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(UwagiTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Uwagi\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                UwagiTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(AdnInspTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Adnotacje o inspekcjach, informacje o zmianach\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                AdnInspTB.Focus(); return false;
            }
            if (String.IsNullOrEmpty(OprTekstTB.Text) || String.IsNullOrEmpty(PlanyOprTB.Text) || String.IsNullOrEmpty(FotoOprTB.Text))
            {
                TaskDialog.Show("Wypełnij wszystkie pola rubryki \"Opracowanie karty ewidencyjnej\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);

                if (String.IsNullOrEmpty(OprTekstTB.Text))
                {
                    OprTekstTB.Focus(); return false;
                }
                if (String.IsNullOrEmpty(PlanyOprTB.Text))
                {
                    PlanyOprTB.Focus(); return false;
                }
                if (String.IsNullOrEmpty(FotoOprTB.Text))
                {
                    FotoOprTB.Focus(); return false;
                }
            }
            if (String.IsNullOrEmpty(ZalacznikiTB.Text))
            {
                TaskDialog.Show("Wypełnij pole \"Załączniki\"", "Błąd", "Nie można zapisać danych eksponatu jeśli są niepełne", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                ZalacznikiTB.Focus(); return false;
            }
            return true;
        }


        private void ZdjecieDodajBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog WczytajZdjecieDialog = new OpenFileDialog();
            WczytajZdjecieDialog.Filter = "Obraz w formacie JPG (*.jpg)|*.jpg|Obraz w formacie PNG (*.png)|*.png";
            WczytajZdjecieDialog.Title = "Wybierz zdjęcie dodawanego eksponatu";

            if (WczytajZdjecieDialog.ShowDialog() == DialogResult.OK)
            {
                string polozenie = WczytajZdjecieDialog.FileName;
                ZdjecieSciezkaTB.Text = polozenie;
                MatGrafPB.ImageLocation = WczytajZdjecieDialog.FileName;//.Replace("\\", "\\\\");
            }


        }

        private void WczytajZdjecieDialog_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void ZapiszDaneBtn_Click(object sender, EventArgs e)
        {

            if (true)
            {
                byte[] zdjbajt = null;
                FileStream strumien = new FileStream(ZdjecieSciezkaTB.Text, FileMode.Open, FileAccess.Read);
                BinaryReader czytbajt = new BinaryReader(strumien);
                zdjbajt = czytbajt.ReadBytes((int)strumien.Length);

                string[] daneNID = new string[] { ExpIDTB.Text, NazwaTB.Text, nazwakategorii, rok, MatPodstTB.Text, DaneFrmTB.Text, String.Concat(DimDlTB.Text, "\\n", DimSzerTB.Text, "\\n", DimWysTB.Text), CiezarTB.Text, IloscTB.Text, WlascicielTB.Text, UzytkownikTB.Text, MscePcyTB.Text, UdstTB.Text, FormyOchrTB.Text, HistoriaTB.Text, OpisTB.Text, PrzeznPierwTB.Text, UzObecneTB.Text, RemontyTB.Text, StanTB.Text, AktaArchTB.Text, BibGrfTB.Text, ZrodlaIkoTB.Text, UwagiTB.Text, AdnInspTB.Text, String.Concat(OprTekstTB.Text, "\\n", PlanyOprTB.Text, "\\n", FotoOprTB.Text), ZalacznikiTB.Text };
                string[] kolumny = new string[] {"identyfikator", "nazwa", "kategoria", "czaspowstania", "matpodstawowy", "danefirmowe", "wymiary", "ciezar", "ilosc","matgraficzne", "wlasciciel", "uzytkownik", "mscepracy", "udostepnianie", "ochrona", "historia", "opis", "pierwprzeznaczenie", "obuzytkowanie", "remonty", "stan", "akta", "bibliografia", "ikonografia", "uwagi", "inspekcje", "oprkarty", "zalaczniki", "typ", "producent"};
                string query = "UPDATE `eksponaty` SET ";
                for (int i = 1; i < (int)daneNID.Length; i++) 
                {
                    query += kolumny[i] + "='" + daneNID[i] + "',";
                }
                query +=  "matgraficzne=@IMG WHERE identyfikator='" + daneNID[0] + "'";
                string adresquery = "UPDATE `adres` SET miasto='" + MiejscowoscTB.Text + "', wojewodztwo='" + wojewodztwo + "', gmina='" + GminaTB.Text + "', powiat='" + PowiatTB.Text + "' WHERE identyfikator='"+daneNID[0]+"'";

                string statusquery = "UPDATE `stan` SET `status`='"+stan+"' WHERE identyfikator='"+daneNID[0]+"'";

                //Console.WriteLine(query);
                MySqlConnection link = new MySqlConnection(login.danepolaczenia);
                MySqlCommand zapiszcmd = new MySqlCommand(query, link);
                MySqlDataReader czytnikSQL;
                try
                {
                    link.Open();
                    zapiszcmd.Parameters.Add(new MySqlParameter("@IMG", zdjbajt));
                    czytnikSQL = zapiszcmd.ExecuteReader();
                    while (czytnikSQL.Read()) { }
                    zapiszcmd.CommandText = adresquery;
                    zapiszcmd.ExecuteNonQuery();
                    zapiszcmd.CommandText = statusquery;
                    zapiszcmd.ExecuteNonQuery();
                    MessageBox.Show("Zmieniono dane eksponatu \"" + NazwaTB.Text + "\".");
                }
                catch (MySqlException sqlexc)
                {
                    TaskDialog.Show("Skontaktuj się z administratorem", "Błąd operacji bazodanowych", "Wystąpił błąd bazy danych\n" + sqlexc.Message, TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                }
                finally 
                {
                    link.Close();
                }

            }



        }
        private void YearUpDown_ValueChanged(object sender, EventArgs e)
        {
            rok = Convert.ToString(YearUpDown.Value);
        }

        private void WojewodztwoCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            wojewodztwo = WojewodztwoCB.SelectedItem.ToString();
            //Console.WriteLine(wojewodztwo);
        }

        private void QRCodeGen_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ExpIDTB.Text))
            {
                GenerujQR QRgen = new GenerujQR(sciezka, ExpIDTB.Text, NazwaTB.Text);
                QRgen.Show();
            }
            else
            {
                TaskDialog.Show("Puste pole \"Identyfikator eksponatu\"", "Błąd!", "Kod QR eksponatu nie może zostać wygenerowany gdy eksponat nie posiada identyfikatora.\n\nWypełnij pole \"Identyfikator eksponatu\"", TaskDialogButton.OK, TaskDialogIcon.SecurityWarning);
                ExpIDTB.Focus();
            }
        }

        private void StanCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            stan = StanCombo.SelectedItem.ToString();
            switch (stan)
            {
                case "Wypożyczony":
                    {

                        AdresGroupBox.Visible = true;

                    }
                    break;
                case "W magazynie":
                    {

                        StanCombo.SelectedItem = "W magazynie";
                        AdresGroupBox.Text = "Adres magazynu";
                        AdresGroupBox.Visible = true;
                        WypKomu.Visible = WypKomu_Label.Visible = false;
                        MiastoWyp.Text = defaddress[0];
                        GminaWyp.Text = defaddress[1];
                        PowiatWyp.Text = defaddress[2];
                        WojewodztwoCombo.SelectedItem = defaddress[3];
                        DataZwrotu.Visible = DataZwrotuLabel.Visible = false;
                    }
                    break;
                case "Ekspozycja stała":
                    {

                        StanCombo.SelectedItem = "Ekspozycja stała";
                        AdresGroupBox.Text = "Muzeum Historii Komputerów i Informatyki";
                        AdresGroupBox.Visible = true;
                        WypKomu.Visible = WypKomu_Label.Visible = false;
                        MiastoWyp.Text = defaddress[0];
                        GminaWyp.Text = defaddress[1];
                        PowiatWyp.Text = defaddress[2];
                        WojewodztwoCombo.SelectedItem = defaddress[3];
                        DataZwrotu.Visible = DataZwrotuLabel.Visible = false;
                    }
                    break;
                case "Ekspozycja czasowa":
                    {

                        StanCombo.SelectedItem = "Ekspozycja czasowa";
                        AdresGroupBox.Visible = true;
                        AdresGroupBox.Text = "Ekspozycja czasowa";
                        WypKomu.Visible = WypKomu_Label.Visible = true;
                        DataZwrotu.Visible = true;
                    }
                    break;

            }
        }


    }
}
