﻿namespace RejestrZabytkówMHKI
{
    partial class UserAdm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddUserBtn = new System.Windows.Forms.Button();
            this.ModifyUserBtn = new System.Windows.Forms.Button();
            this.DelUserBtn = new System.Windows.Forms.Button();
            this.UserList = new System.Windows.Forms.ListView();
            this.ViewMode = new System.Windows.Forms.ComboBox();
            this.ModUserPanel = new System.Windows.Forms.Panel();
            this.SurnameLabel = new System.Windows.Forms.Label();
            this.ImieLabel = new System.Windows.Forms.Label();
            this.UserInfo = new System.Windows.Forms.GroupBox();
            this.GrupyUpr = new System.Windows.Forms.ComboBox();
            this.Imię = new System.Windows.Forms.Label();
            this.NazwiskoLabel = new System.Windows.Forms.Label();
            this.AdresEMail = new System.Windows.Forms.TextBox();
            this.Nazwisko = new System.Windows.Forms.TextBox();
            this.EMailAddress = new System.Windows.Forms.Label();
            this.Imie = new System.Windows.Forms.TextBox();
            this.PowtorzNoweHaslo = new System.Windows.Forms.Label();
            this.NoweHaslo = new System.Windows.Forms.Label();
            this.SaveNewAvatar = new System.Windows.Forms.Button();
            this.CloseModUsrPanel = new System.Windows.Forms.Button();
            this.AvatarPath = new System.Windows.Forms.TextBox();
            this.ModifyUsrData = new System.Windows.Forms.Button();
            this.ZmienHaslo = new System.Windows.Forms.LinkLabel();
            this.ZmienAvatar = new System.Windows.Forms.LinkLabel();
            this.ZmienDane = new System.Windows.Forms.LinkLabel();
            this.RepeatNewPasswd = new System.Windows.Forms.TextBox();
            this.NewPasswd = new System.Windows.Forms.TextBox();
            this.Avatar = new System.Windows.Forms.PictureBox();
            this.SaveNewPasswdBtn = new System.Windows.Forms.Button();
            this.ChangeAvatarBtn = new System.Windows.Forms.Button();
            this.AddUserPanel = new System.Windows.Forms.Panel();
            this.CloseUserAddPanel = new System.Windows.Forms.Button();
            this.SaveNewUser = new System.Windows.Forms.Button();
            this.RepeatPasswordLabel = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.LoginLabel = new System.Windows.Forms.Label();
            this.NewUser_Grupa = new System.Windows.Forms.ComboBox();
            this.NewUserRepeatPasswd = new System.Windows.Forms.TextBox();
            this.NewUserPasswd = new System.Windows.Forms.TextBox();
            this.NewUserName = new System.Windows.Forms.TextBox();
            this.DodatkoweInfo = new System.Windows.Forms.GroupBox();
            this.AddUser_InsertImg = new System.Windows.Forms.Button();
            this.AddUser_Avatar = new System.Windows.Forms.Label();
            this.AddUser_EMail = new System.Windows.Forms.Label();
            this.AddUser_SurnameLabel = new System.Windows.Forms.Label();
            this.AddUser_NameLabel = new System.Windows.Forms.Label();
            this.AddAvatar = new System.Windows.Forms.PictureBox();
            this.NewUser_EMail = new System.Windows.Forms.TextBox();
            this.NewUser_Nazwisko = new System.Windows.Forms.TextBox();
            this.NewUser_Imie = new System.Windows.Forms.TextBox();
            this.NewAvatarDialog = new System.Windows.Forms.OpenFileDialog();
            this.ModUserPanel.SuspendLayout();
            this.UserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).BeginInit();
            this.AddUserPanel.SuspendLayout();
            this.DodatkoweInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // AddUserBtn
            // 
            this.AddUserBtn.Location = new System.Drawing.Point(480, 104);
            this.AddUserBtn.Name = "AddUserBtn";
            this.AddUserBtn.Size = new System.Drawing.Size(75, 23);
            this.AddUserBtn.TabIndex = 0;
            this.AddUserBtn.Text = "Dodaj Użytkownika";
            this.AddUserBtn.UseVisualStyleBackColor = true;
            this.AddUserBtn.Click += new System.EventHandler(this.AddUserBtn_Click);
            // 
            // ModifyUserBtn
            // 
            this.ModifyUserBtn.Location = new System.Drawing.Point(480, 152);
            this.ModifyUserBtn.Name = "ModifyUserBtn";
            this.ModifyUserBtn.Size = new System.Drawing.Size(75, 23);
            this.ModifyUserBtn.TabIndex = 1;
            this.ModifyUserBtn.Text = "Zmień dane użytkownika";
            this.ModifyUserBtn.UseVisualStyleBackColor = true;
            this.ModifyUserBtn.Click += new System.EventHandler(this.ModifyUserBtn_Click);
            // 
            // DelUserBtn
            // 
            this.DelUserBtn.Location = new System.Drawing.Point(480, 192);
            this.DelUserBtn.Name = "DelUserBtn";
            this.DelUserBtn.Size = new System.Drawing.Size(75, 23);
            this.DelUserBtn.TabIndex = 2;
            this.DelUserBtn.Text = "Usuń użytkownika";
            this.DelUserBtn.UseVisualStyleBackColor = true;
            this.DelUserBtn.Click += new System.EventHandler(this.DelUserBtn_Click);
            // 
            // UserList
            // 
            this.UserList.Location = new System.Drawing.Point(40, 56);
            this.UserList.Name = "UserList";
            this.UserList.Size = new System.Drawing.Size(424, 280);
            this.UserList.TabIndex = 3;
            this.UserList.UseCompatibleStateImageBehavior = false;
            this.UserList.SelectedIndexChanged += new System.EventHandler(this.UserList_SelectedIndexChanged);
            // 
            // ViewMode
            // 
            this.ViewMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ViewMode.FormattingEnabled = true;
            this.ViewMode.Items.AddRange(new object[] {
            "Duże ikony",
            "Małe ikony",
            "Lista",
            "Szczegóły"});
            this.ViewMode.Location = new System.Drawing.Point(480, 56);
            this.ViewMode.Name = "ViewMode";
            this.ViewMode.Size = new System.Drawing.Size(121, 21);
            this.ViewMode.TabIndex = 4;
            this.ViewMode.SelectedIndexChanged += new System.EventHandler(this.ViewMode_SelectedIndexChanged);
            // 
            // ModUserPanel
            // 
            this.ModUserPanel.BackColor = System.Drawing.Color.LawnGreen;
            this.ModUserPanel.Controls.Add(this.SurnameLabel);
            this.ModUserPanel.Controls.Add(this.ImieLabel);
            this.ModUserPanel.Controls.Add(this.UserInfo);
            this.ModUserPanel.Controls.Add(this.PowtorzNoweHaslo);
            this.ModUserPanel.Controls.Add(this.NoweHaslo);
            this.ModUserPanel.Controls.Add(this.SaveNewAvatar);
            this.ModUserPanel.Controls.Add(this.CloseModUsrPanel);
            this.ModUserPanel.Controls.Add(this.AvatarPath);
            this.ModUserPanel.Controls.Add(this.ModifyUsrData);
            this.ModUserPanel.Controls.Add(this.ZmienHaslo);
            this.ModUserPanel.Controls.Add(this.ZmienAvatar);
            this.ModUserPanel.Controls.Add(this.ZmienDane);
            this.ModUserPanel.Controls.Add(this.RepeatNewPasswd);
            this.ModUserPanel.Controls.Add(this.NewPasswd);
            this.ModUserPanel.Controls.Add(this.Avatar);
            this.ModUserPanel.Controls.Add(this.SaveNewPasswdBtn);
            this.ModUserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ModUserPanel.Location = new System.Drawing.Point(0, 0);
            this.ModUserPanel.Name = "ModUserPanel";
            this.ModUserPanel.Size = new System.Drawing.Size(635, 382);
            this.ModUserPanel.TabIndex = 5;
            this.ModUserPanel.Visible = false;
            // 
            // SurnameLabel
            // 
            this.SurnameLabel.AutoSize = true;
            this.SurnameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SurnameLabel.Location = new System.Drawing.Point(480, 136);
            this.SurnameLabel.Name = "SurnameLabel";
            this.SurnameLabel.Size = new System.Drawing.Size(0, 20);
            this.SurnameLabel.TabIndex = 22;
            // 
            // ImieLabel
            // 
            this.ImieLabel.AutoSize = true;
            this.ImieLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ImieLabel.Location = new System.Drawing.Point(480, 104);
            this.ImieLabel.Name = "ImieLabel";
            this.ImieLabel.Size = new System.Drawing.Size(0, 20);
            this.ImieLabel.TabIndex = 21;
            // 
            // UserInfo
            // 
            this.UserInfo.Controls.Add(this.GrupyUpr);
            this.UserInfo.Controls.Add(this.Imię);
            this.UserInfo.Controls.Add(this.NazwiskoLabel);
            this.UserInfo.Controls.Add(this.AdresEMail);
            this.UserInfo.Controls.Add(this.Nazwisko);
            this.UserInfo.Controls.Add(this.EMailAddress);
            this.UserInfo.Controls.Add(this.Imie);
            this.UserInfo.Location = new System.Drawing.Point(168, 96);
            this.UserInfo.Name = "UserInfo";
            this.UserInfo.Size = new System.Drawing.Size(200, 184);
            this.UserInfo.TabIndex = 20;
            this.UserInfo.TabStop = false;
            this.UserInfo.Text = "Informacje o użytkowniku";
            // 
            // GrupyUpr
            // 
            this.GrupyUpr.FormattingEnabled = true;
            this.GrupyUpr.Items.AddRange(new object[] {
            "Grupa uprawnień:",
            "Przeglądanie (browse)",
            "Inwentaryzacja (invmgr)",
            "Administracja (root)"});
            this.GrupyUpr.Location = new System.Drawing.Point(24, 152);
            this.GrupyUpr.Name = "GrupyUpr";
            this.GrupyUpr.Size = new System.Drawing.Size(136, 21);
            this.GrupyUpr.TabIndex = 20;
            this.GrupyUpr.SelectedIndexChanged += new System.EventHandler(this.GrupyUpr_SelectedIndexChanged);
            // 
            // Imię
            // 
            this.Imię.AutoSize = true;
            this.Imię.Location = new System.Drawing.Point(24, 24);
            this.Imię.Name = "Imię";
            this.Imię.Size = new System.Drawing.Size(29, 13);
            this.Imię.TabIndex = 17;
            this.Imię.Text = "Imię:";
            // 
            // NazwiskoLabel
            // 
            this.NazwiskoLabel.AutoSize = true;
            this.NazwiskoLabel.Location = new System.Drawing.Point(24, 64);
            this.NazwiskoLabel.Name = "NazwiskoLabel";
            this.NazwiskoLabel.Size = new System.Drawing.Size(53, 13);
            this.NazwiskoLabel.TabIndex = 19;
            this.NazwiskoLabel.Text = "Nazwisko";
            // 
            // AdresEMail
            // 
            this.AdresEMail.Location = new System.Drawing.Point(24, 120);
            this.AdresEMail.Name = "AdresEMail";
            this.AdresEMail.Size = new System.Drawing.Size(136, 20);
            this.AdresEMail.TabIndex = 14;
            // 
            // Nazwisko
            // 
            this.Nazwisko.Location = new System.Drawing.Point(24, 80);
            this.Nazwisko.Name = "Nazwisko";
            this.Nazwisko.Size = new System.Drawing.Size(136, 20);
            this.Nazwisko.TabIndex = 18;
            // 
            // EMailAddress
            // 
            this.EMailAddress.AutoSize = true;
            this.EMailAddress.Location = new System.Drawing.Point(24, 104);
            this.EMailAddress.Name = "EMailAddress";
            this.EMailAddress.Size = new System.Drawing.Size(67, 13);
            this.EMailAddress.TabIndex = 15;
            this.EMailAddress.Text = "Adres e-mail:";
            // 
            // Imie
            // 
            this.Imie.Location = new System.Drawing.Point(24, 40);
            this.Imie.Name = "Imie";
            this.Imie.Size = new System.Drawing.Size(136, 20);
            this.Imie.TabIndex = 16;
            // 
            // PowtorzNoweHaslo
            // 
            this.PowtorzNoweHaslo.AutoSize = true;
            this.PowtorzNoweHaslo.Location = new System.Drawing.Point(192, 136);
            this.PowtorzNoweHaslo.Name = "PowtorzNoweHaslo";
            this.PowtorzNoweHaslo.Size = new System.Drawing.Size(107, 13);
            this.PowtorzNoweHaslo.TabIndex = 13;
            this.PowtorzNoweHaslo.Text = "Powtórz nowe hasło:";
            // 
            // NoweHaslo
            // 
            this.NoweHaslo.AutoSize = true;
            this.NoweHaslo.Location = new System.Drawing.Point(192, 96);
            this.NoweHaslo.Name = "NoweHaslo";
            this.NoweHaslo.Size = new System.Drawing.Size(68, 13);
            this.NoweHaslo.TabIndex = 12;
            this.NoweHaslo.Text = "Nowe hasło:";
            // 
            // SaveNewAvatar
            // 
            this.SaveNewAvatar.Location = new System.Drawing.Point(512, 216);
            this.SaveNewAvatar.Name = "SaveNewAvatar";
            this.SaveNewAvatar.Size = new System.Drawing.Size(56, 40);
            this.SaveNewAvatar.TabIndex = 11;
            this.SaveNewAvatar.Text = "Zapisz obrazek";
            this.SaveNewAvatar.UseVisualStyleBackColor = true;
            this.SaveNewAvatar.Click += new System.EventHandler(this.SaveNewAvatar_Click);
            // 
            // CloseModUsrPanel
            // 
            this.CloseModUsrPanel.Location = new System.Drawing.Point(280, 344);
            this.CloseModUsrPanel.Name = "CloseModUsrPanel";
            this.CloseModUsrPanel.Size = new System.Drawing.Size(75, 23);
            this.CloseModUsrPanel.TabIndex = 10;
            this.CloseModUsrPanel.Text = "Zamknij";
            this.CloseModUsrPanel.UseVisualStyleBackColor = true;
            this.CloseModUsrPanel.Click += new System.EventHandler(this.CloseModUsrPanel_Click);
            // 
            // AvatarPath
            // 
            this.AvatarPath.Enabled = false;
            this.AvatarPath.Location = new System.Drawing.Point(400, 184);
            this.AvatarPath.Name = "AvatarPath";
            this.AvatarPath.Size = new System.Drawing.Size(160, 20);
            this.AvatarPath.TabIndex = 9;
            this.AvatarPath.Visible = false;
            // 
            // ModifyUsrData
            // 
            this.ModifyUsrData.Location = new System.Drawing.Point(216, 296);
            this.ModifyUsrData.Name = "ModifyUsrData";
            this.ModifyUsrData.Size = new System.Drawing.Size(96, 23);
            this.ModifyUsrData.TabIndex = 8;
            this.ModifyUsrData.Text = "Zmień dane";
            this.ModifyUsrData.UseVisualStyleBackColor = true;
            this.ModifyUsrData.Click += new System.EventHandler(this.ModifyUsrData_Click);
            // 
            // ZmienHaslo
            // 
            this.ZmienHaslo.AutoSize = true;
            this.ZmienHaslo.Location = new System.Drawing.Point(40, 192);
            this.ZmienHaslo.Name = "ZmienHaslo";
            this.ZmienHaslo.Size = new System.Drawing.Size(66, 13);
            this.ZmienHaslo.TabIndex = 6;
            this.ZmienHaslo.TabStop = true;
            this.ZmienHaslo.Text = "Zmień hasło";
            this.ZmienHaslo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ZmienHaslo_LinkClicked);
            // 
            // ZmienAvatar
            // 
            this.ZmienAvatar.AutoSize = true;
            this.ZmienAvatar.Location = new System.Drawing.Point(40, 152);
            this.ZmienAvatar.Name = "ZmienAvatar";
            this.ZmienAvatar.Size = new System.Drawing.Size(107, 13);
            this.ZmienAvatar.TabIndex = 5;
            this.ZmienAvatar.TabStop = true;
            this.ZmienAvatar.Text = "Zmień obrazek konta";
            this.ZmienAvatar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ZmienAvatar_LinkClicked);
            // 
            // ZmienDane
            // 
            this.ZmienDane.AutoSize = true;
            this.ZmienDane.Location = new System.Drawing.Point(40, 112);
            this.ZmienDane.Name = "ZmienDane";
            this.ZmienDane.Size = new System.Drawing.Size(63, 13);
            this.ZmienDane.TabIndex = 4;
            this.ZmienDane.TabStop = true;
            this.ZmienDane.Text = "Zmień dane";
            this.ZmienDane.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ZmienDane_LinkClicked);
            // 
            // RepeatNewPasswd
            // 
            this.RepeatNewPasswd.Location = new System.Drawing.Point(192, 152);
            this.RepeatNewPasswd.Name = "RepeatNewPasswd";
            this.RepeatNewPasswd.Size = new System.Drawing.Size(136, 20);
            this.RepeatNewPasswd.TabIndex = 3;
            this.RepeatNewPasswd.UseSystemPasswordChar = true;
            // 
            // NewPasswd
            // 
            this.NewPasswd.Location = new System.Drawing.Point(192, 112);
            this.NewPasswd.Name = "NewPasswd";
            this.NewPasswd.Size = new System.Drawing.Size(136, 20);
            this.NewPasswd.TabIndex = 2;
            this.NewPasswd.UseSystemPasswordChar = true;
            // 
            // Avatar
            // 
            this.Avatar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Avatar.Location = new System.Drawing.Point(400, 96);
            this.Avatar.Name = "Avatar";
            this.Avatar.Size = new System.Drawing.Size(64, 64);
            this.Avatar.TabIndex = 1;
            this.Avatar.TabStop = false;
            // 
            // SaveNewPasswdBtn
            // 
            this.SaveNewPasswdBtn.Location = new System.Drawing.Point(216, 296);
            this.SaveNewPasswdBtn.Name = "SaveNewPasswdBtn";
            this.SaveNewPasswdBtn.Size = new System.Drawing.Size(96, 23);
            this.SaveNewPasswdBtn.TabIndex = 0;
            this.SaveNewPasswdBtn.Text = "Zmień hasło";
            this.SaveNewPasswdBtn.UseVisualStyleBackColor = true;
            this.SaveNewPasswdBtn.Click += new System.EventHandler(this.SaveNewPasswdBtn_Click);
            // 
            // ChangeAvatarBtn
            // 
            this.ChangeAvatarBtn.Location = new System.Drawing.Point(216, 296);
            this.ChangeAvatarBtn.Name = "ChangeAvatarBtn";
            this.ChangeAvatarBtn.Size = new System.Drawing.Size(96, 40);
            this.ChangeAvatarBtn.TabIndex = 7;
            this.ChangeAvatarBtn.Text = "Zmień obrazek konta";
            this.ChangeAvatarBtn.UseVisualStyleBackColor = true;
            this.ChangeAvatarBtn.Click += new System.EventHandler(this.ChangeAvatarBtn_Click);
            // 
            // AddUserPanel
            // 
            this.AddUserPanel.BackColor = System.Drawing.Color.Cyan;
            this.AddUserPanel.Controls.Add(this.ModUserPanel);
            this.AddUserPanel.Controls.Add(this.CloseUserAddPanel);
            this.AddUserPanel.Controls.Add(this.SaveNewUser);
            this.AddUserPanel.Controls.Add(this.RepeatPasswordLabel);
            this.AddUserPanel.Controls.Add(this.PasswordLabel);
            this.AddUserPanel.Controls.Add(this.LoginLabel);
            this.AddUserPanel.Controls.Add(this.NewUser_Grupa);
            this.AddUserPanel.Controls.Add(this.NewUserRepeatPasswd);
            this.AddUserPanel.Controls.Add(this.NewUserPasswd);
            this.AddUserPanel.Controls.Add(this.NewUserName);
            this.AddUserPanel.Controls.Add(this.DodatkoweInfo);
            this.AddUserPanel.Controls.Add(this.ChangeAvatarBtn);
            this.AddUserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddUserPanel.Location = new System.Drawing.Point(0, 0);
            this.AddUserPanel.Name = "AddUserPanel";
            this.AddUserPanel.Size = new System.Drawing.Size(635, 382);
            this.AddUserPanel.TabIndex = 23;
            this.AddUserPanel.Visible = false;
            // 
            // CloseUserAddPanel
            // 
            this.CloseUserAddPanel.Location = new System.Drawing.Point(344, 344);
            this.CloseUserAddPanel.Name = "CloseUserAddPanel";
            this.CloseUserAddPanel.Size = new System.Drawing.Size(75, 23);
            this.CloseUserAddPanel.TabIndex = 9;
            this.CloseUserAddPanel.Text = "Zamknij";
            this.CloseUserAddPanel.UseVisualStyleBackColor = true;
            this.CloseUserAddPanel.Click += new System.EventHandler(this.CloseUserAddPanel_Click);
            // 
            // SaveNewUser
            // 
            this.SaveNewUser.Location = new System.Drawing.Point(200, 344);
            this.SaveNewUser.Name = "SaveNewUser";
            this.SaveNewUser.Size = new System.Drawing.Size(123, 23);
            this.SaveNewUser.TabIndex = 8;
            this.SaveNewUser.Text = "Dodaj użytkownika";
            this.SaveNewUser.UseVisualStyleBackColor = true;
            this.SaveNewUser.Click += new System.EventHandler(this.SaveNewUser_Click);
            // 
            // RepeatPasswordLabel
            // 
            this.RepeatPasswordLabel.AutoSize = true;
            this.RepeatPasswordLabel.Location = new System.Drawing.Point(64, 112);
            this.RepeatPasswordLabel.Name = "RepeatPasswordLabel";
            this.RepeatPasswordLabel.Size = new System.Drawing.Size(78, 13);
            this.RepeatPasswordLabel.TabIndex = 7;
            this.RepeatPasswordLabel.Text = "Powtórz hasło:";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Location = new System.Drawing.Point(96, 80);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(36, 13);
            this.PasswordLabel.TabIndex = 6;
            this.PasswordLabel.Text = "Hasło";
            // 
            // LoginLabel
            // 
            this.LoginLabel.AutoSize = true;
            this.LoginLabel.Location = new System.Drawing.Point(32, 48);
            this.LoginLabel.Name = "LoginLabel";
            this.LoginLabel.Size = new System.Drawing.Size(105, 13);
            this.LoginLabel.TabIndex = 5;
            this.LoginLabel.Text = "Nazwa użytkownika:";
            // 
            // NewUser_Grupa
            // 
            this.NewUser_Grupa.FormattingEnabled = true;
            this.NewUser_Grupa.Items.AddRange(new object[] {
            "Grupa uprawnień:",
            "Przeglądanie (browse)",
            "Inwentaryzacja (invmgr)",
            "Administracja (root)"});
            this.NewUser_Grupa.Location = new System.Drawing.Point(384, 40);
            this.NewUser_Grupa.Name = "NewUser_Grupa";
            this.NewUser_Grupa.Size = new System.Drawing.Size(161, 21);
            this.NewUser_Grupa.TabIndex = 4;
            this.NewUser_Grupa.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // NewUserRepeatPasswd
            // 
            this.NewUserRepeatPasswd.Location = new System.Drawing.Point(144, 104);
            this.NewUserRepeatPasswd.Name = "NewUserRepeatPasswd";
            this.NewUserRepeatPasswd.Size = new System.Drawing.Size(168, 20);
            this.NewUserRepeatPasswd.TabIndex = 3;
            // 
            // NewUserPasswd
            // 
            this.NewUserPasswd.Location = new System.Drawing.Point(144, 72);
            this.NewUserPasswd.Name = "NewUserPasswd";
            this.NewUserPasswd.Size = new System.Drawing.Size(168, 20);
            this.NewUserPasswd.TabIndex = 2;
            // 
            // NewUserName
            // 
            this.NewUserName.Location = new System.Drawing.Point(144, 40);
            this.NewUserName.Name = "NewUserName";
            this.NewUserName.Size = new System.Drawing.Size(168, 20);
            this.NewUserName.TabIndex = 1;
            // 
            // DodatkoweInfo
            // 
            this.DodatkoweInfo.Controls.Add(this.AddUser_InsertImg);
            this.DodatkoweInfo.Controls.Add(this.AddUser_Avatar);
            this.DodatkoweInfo.Controls.Add(this.AddUser_EMail);
            this.DodatkoweInfo.Controls.Add(this.AddUser_SurnameLabel);
            this.DodatkoweInfo.Controls.Add(this.AddUser_NameLabel);
            this.DodatkoweInfo.Controls.Add(this.AddAvatar);
            this.DodatkoweInfo.Controls.Add(this.NewUser_EMail);
            this.DodatkoweInfo.Controls.Add(this.NewUser_Nazwisko);
            this.DodatkoweInfo.Controls.Add(this.NewUser_Imie);
            this.DodatkoweInfo.Location = new System.Drawing.Point(32, 160);
            this.DodatkoweInfo.Name = "DodatkoweInfo";
            this.DodatkoweInfo.Size = new System.Drawing.Size(552, 176);
            this.DodatkoweInfo.TabIndex = 0;
            this.DodatkoweInfo.TabStop = false;
            this.DodatkoweInfo.Text = "Dodatkowe informacje";
            // 
            // AddUser_InsertImg
            // 
            this.AddUser_InsertImg.Location = new System.Drawing.Point(368, 144);
            this.AddUser_InsertImg.Name = "AddUser_InsertImg";
            this.AddUser_InsertImg.Size = new System.Drawing.Size(75, 23);
            this.AddUser_InsertImg.TabIndex = 8;
            this.AddUser_InsertImg.Text = "Wstaw obrazek";
            this.AddUser_InsertImg.UseVisualStyleBackColor = true;
            this.AddUser_InsertImg.Click += new System.EventHandler(this.AddUser_InsertImg_Click);
            // 
            // AddUser_Avatar
            // 
            this.AddUser_Avatar.AutoSize = true;
            this.AddUser_Avatar.Location = new System.Drawing.Point(376, 24);
            this.AddUser_Avatar.Name = "AddUser_Avatar";
            this.AddUser_Avatar.Size = new System.Drawing.Size(77, 13);
            this.AddUser_Avatar.TabIndex = 7;
            this.AddUser_Avatar.Text = "Obrazek konta";
            // 
            // AddUser_EMail
            // 
            this.AddUser_EMail.AutoSize = true;
            this.AddUser_EMail.Location = new System.Drawing.Point(64, 128);
            this.AddUser_EMail.Name = "AddUser_EMail";
            this.AddUser_EMail.Size = new System.Drawing.Size(37, 13);
            this.AddUser_EMail.TabIndex = 6;
            this.AddUser_EMail.Text = "e-mail:";
            // 
            // AddUser_SurnameLabel
            // 
            this.AddUser_SurnameLabel.AutoSize = true;
            this.AddUser_SurnameLabel.Location = new System.Drawing.Point(48, 88);
            this.AddUser_SurnameLabel.Name = "AddUser_SurnameLabel";
            this.AddUser_SurnameLabel.Size = new System.Drawing.Size(56, 13);
            this.AddUser_SurnameLabel.TabIndex = 5;
            this.AddUser_SurnameLabel.Text = "Nazwisko:";
            // 
            // AddUser_NameLabel
            // 
            this.AddUser_NameLabel.AutoSize = true;
            this.AddUser_NameLabel.Location = new System.Drawing.Point(72, 40);
            this.AddUser_NameLabel.Name = "AddUser_NameLabel";
            this.AddUser_NameLabel.Size = new System.Drawing.Size(29, 13);
            this.AddUser_NameLabel.TabIndex = 4;
            this.AddUser_NameLabel.Text = "Imię:";
            // 
            // AddAvatar
            // 
            this.AddAvatar.Location = new System.Drawing.Point(376, 40);
            this.AddAvatar.Name = "AddAvatar";
            this.AddAvatar.Size = new System.Drawing.Size(64, 64);
            this.AddAvatar.TabIndex = 3;
            this.AddAvatar.TabStop = false;
            // 
            // NewUser_EMail
            // 
            this.NewUser_EMail.Location = new System.Drawing.Point(112, 120);
            this.NewUser_EMail.Name = "NewUser_EMail";
            this.NewUser_EMail.Size = new System.Drawing.Size(168, 20);
            this.NewUser_EMail.TabIndex = 2;
            // 
            // NewUser_Nazwisko
            // 
            this.NewUser_Nazwisko.Location = new System.Drawing.Point(112, 80);
            this.NewUser_Nazwisko.Name = "NewUser_Nazwisko";
            this.NewUser_Nazwisko.Size = new System.Drawing.Size(168, 20);
            this.NewUser_Nazwisko.TabIndex = 1;
            // 
            // NewUser_Imie
            // 
            this.NewUser_Imie.Location = new System.Drawing.Point(112, 40);
            this.NewUser_Imie.Name = "NewUser_Imie";
            this.NewUser_Imie.Size = new System.Drawing.Size(168, 20);
            this.NewUser_Imie.TabIndex = 0;
            // 
            // NewAvatarDialog
            // 
            this.NewAvatarDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.NewAvatarDialog_FileOk);
            // 
            // UserAdm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 382);
            this.Controls.Add(this.ViewMode);
            this.Controls.Add(this.UserList);
            this.Controls.Add(this.DelUserBtn);
            this.Controls.Add(this.ModifyUserBtn);
            this.Controls.Add(this.AddUserBtn);
            this.Controls.Add(this.AddUserPanel);
            this.Name = "UserAdm";
            this.Text = "UserAdm";
            this.ModUserPanel.ResumeLayout(false);
            this.ModUserPanel.PerformLayout();
            this.UserInfo.ResumeLayout(false);
            this.UserInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).EndInit();
            this.AddUserPanel.ResumeLayout(false);
            this.AddUserPanel.PerformLayout();
            this.DodatkoweInfo.ResumeLayout(false);
            this.DodatkoweInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddAvatar)).EndInit();
            this.ResumeLayout(false);

        }

       

        #endregion

        
        private System.Windows.Forms.ListView UserList;
        

        private System.Windows.Forms.LinkLabel ZmienHaslo;
        private System.Windows.Forms.LinkLabel ZmienAvatar;
        private System.Windows.Forms.LinkLabel ZmienDane;
       
        
        private System.Windows.Forms.OpenFileDialog NewAvatarDialog;

        #region Przyciski
        private System.Windows.Forms.Button ChangeAvatarBtn;
        private System.Windows.Forms.Button CloseUserAddPanel;
        private System.Windows.Forms.Button SaveNewUser;    
        private System.Windows.Forms.Button ModifyUsrData;
        private System.Windows.Forms.Button SaveNewPasswdBtn;
        private System.Windows.Forms.Button AddUserBtn;
        private System.Windows.Forms.Button ModifyUserBtn;
        private System.Windows.Forms.Button DelUserBtn;
        private System.Windows.Forms.Button CloseModUsrPanel;
        private System.Windows.Forms.Button SaveNewAvatar;
        private System.Windows.Forms.Button AddUser_InsertImg;
        #endregion

        #region Panele
        private System.Windows.Forms.Panel AddUserPanel;
        private System.Windows.Forms.Panel ModUserPanel;
        #endregion

        #region ComboBoxy
        private System.Windows.Forms.ComboBox NewUser_Grupa;
        private System.Windows.Forms.ComboBox ViewMode;
        private System.Windows.Forms.ComboBox GrupyUpr;
        #endregion

        #region GroupBoxy
        private System.Windows.Forms.GroupBox DodatkoweInfo;
        private System.Windows.Forms.GroupBox UserInfo;
        #endregion

        #region Labelki
        private System.Windows.Forms.Label AddUser_Avatar;
        private System.Windows.Forms.Label AddUser_EMail;
        private System.Windows.Forms.Label AddUser_SurnameLabel;
        private System.Windows.Forms.Label AddUser_NameLabel;
        private System.Windows.Forms.Label PowtorzNoweHaslo;
        private System.Windows.Forms.Label NoweHaslo;
        private System.Windows.Forms.Label SurnameLabel;
        private System.Windows.Forms.Label ImieLabel;
        private System.Windows.Forms.Label RepeatPasswordLabel;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Label LoginLabel;
        private System.Windows.Forms.Label Imię;
        private System.Windows.Forms.Label NazwiskoLabel;
        private System.Windows.Forms.Label EMailAddress;
        #endregion

        #region PictureBoxy
        private System.Windows.Forms.PictureBox AddAvatar;
        private System.Windows.Forms.PictureBox Avatar;
        #endregion

        #region TextBoxy
        private System.Windows.Forms.TextBox RepeatNewPasswd;
        private System.Windows.Forms.TextBox NewPasswd;
        private System.Windows.Forms.TextBox AdresEMail;
        private System.Windows.Forms.TextBox AvatarPath;
        private System.Windows.Forms.TextBox Nazwisko;
        private System.Windows.Forms.TextBox NewUser_EMail;
        private System.Windows.Forms.TextBox NewUser_Nazwisko;
        private System.Windows.Forms.TextBox NewUser_Imie;
        private System.Windows.Forms.TextBox NewUserRepeatPasswd;
        private System.Windows.Forms.TextBox NewUserPasswd;
        private System.Windows.Forms.TextBox NewUserName;
        private System.Windows.Forms.TextBox Imie;
        #endregion
    }
}