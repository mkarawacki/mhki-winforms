﻿namespace RejestrZabytkówMHKI
{
    partial class Inventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CapturePreview = new System.Windows.Forms.PictureBox();
            this.ScanQRBtn = new System.Windows.Forms.Button();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.WynikTextBox = new System.Windows.Forms.TextBox();
            this.WynikLabel = new System.Windows.Forms.Label();
            this.AdresGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.WojewodztwoWyp_Label = new System.Windows.Forms.Label();
            this.WojewodztwoCombo = new System.Windows.Forms.ComboBox();
            this.GminaWyp = new System.Windows.Forms.TextBox();
            this.GminaWyp_Label = new System.Windows.Forms.Label();
            this.PowiatWyp = new System.Windows.Forms.TextBox();
            this.PowiatWyp_Label = new System.Windows.Forms.Label();
            this.MiastoWyp = new System.Windows.Forms.TextBox();
            this.MiastoWyp_Label = new System.Windows.Forms.Label();
            this.DataZwrotuLabel = new System.Windows.Forms.Label();
            this.DataZwrotu = new System.Windows.Forms.DateTimePicker();
            this.WypKomu = new System.Windows.Forms.TextBox();
            this.WypKomu_Label = new System.Windows.Forms.Label();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.StanCombo = new System.Windows.Forms.ComboBox();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.NazwaEksponatu = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.StopBatch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.CapturePreview)).BeginInit();
            this.AdresGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // CapturePreview
            // 
            this.CapturePreview.Location = new System.Drawing.Point(299, 50);
            this.CapturePreview.Name = "CapturePreview";
            this.CapturePreview.Size = new System.Drawing.Size(253, 248);
            this.CapturePreview.TabIndex = 0;
            this.CapturePreview.TabStop = false;
            // 
            // ScanQRBtn
            // 
            this.ScanQRBtn.Location = new System.Drawing.Point(64, 72);
            this.ScanQRBtn.Name = "ScanQRBtn";
            this.ScanQRBtn.Size = new System.Drawing.Size(75, 23);
            this.ScanQRBtn.TabIndex = 1;
            this.ScanQRBtn.Text = "Skanuj";
            this.ScanQRBtn.UseVisualStyleBackColor = true;
            this.ScanQRBtn.Click += new System.EventHandler(this.ScanQRBtn_Click);
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.AutoSize = true;
            this.PreviewLabel.Location = new System.Drawing.Point(296, 34);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(46, 13);
            this.PreviewLabel.TabIndex = 2;
            this.PreviewLabel.Text = "Podgląd";
            // 
            // WynikTextBox
            // 
            this.WynikTextBox.Location = new System.Drawing.Point(12, 128);
            this.WynikTextBox.Multiline = true;
            this.WynikTextBox.Name = "WynikTextBox";
            this.WynikTextBox.ReadOnly = true;
            this.WynikTextBox.Size = new System.Drawing.Size(244, 44);
            this.WynikTextBox.TabIndex = 3;
            // 
            // WynikLabel
            // 
            this.WynikLabel.AutoSize = true;
            this.WynikLabel.Location = new System.Drawing.Point(12, 112);
            this.WynikLabel.Name = "WynikLabel";
            this.WynikLabel.Size = new System.Drawing.Size(52, 13);
            this.WynikLabel.TabIndex = 4;
            this.WynikLabel.Text = "Eksponat";
            this.WynikLabel.Click += new System.EventHandler(this.WynikLabel_Click);
            // 
            // AdresGroupBox
            // 
            this.AdresGroupBox.Controls.Add(this.groupBox1);
            this.AdresGroupBox.Controls.Add(this.DataZwrotuLabel);
            this.AdresGroupBox.Controls.Add(this.DataZwrotu);
            this.AdresGroupBox.Controls.Add(this.WypKomu);
            this.AdresGroupBox.Controls.Add(this.WypKomu_Label);
            this.AdresGroupBox.Location = new System.Drawing.Point(12, 252);
            this.AdresGroupBox.Name = "AdresGroupBox";
            this.AdresGroupBox.Size = new System.Drawing.Size(278, 261);
            this.AdresGroupBox.TabIndex = 5;
            this.AdresGroupBox.TabStop = false;
            this.AdresGroupBox.Text = "Wypożyczono do";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WojewodztwoWyp_Label);
            this.groupBox1.Controls.Add(this.WojewodztwoCombo);
            this.groupBox1.Controls.Add(this.GminaWyp);
            this.groupBox1.Controls.Add(this.GminaWyp_Label);
            this.groupBox1.Controls.Add(this.PowiatWyp);
            this.groupBox1.Controls.Add(this.PowiatWyp_Label);
            this.groupBox1.Controls.Add(this.MiastoWyp);
            this.groupBox1.Controls.Add(this.MiastoWyp_Label);
            this.groupBox1.Location = new System.Drawing.Point(9, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(263, 156);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Adres";
            // 
            // WojewodztwoWyp_Label
            // 
            this.WojewodztwoWyp_Label.AutoSize = true;
            this.WojewodztwoWyp_Label.Location = new System.Drawing.Point(6, 123);
            this.WojewodztwoWyp_Label.Name = "WojewodztwoWyp_Label";
            this.WojewodztwoWyp_Label.Size = new System.Drawing.Size(74, 13);
            this.WojewodztwoWyp_Label.TabIndex = 7;
            this.WojewodztwoWyp_Label.Text = "Województwo";
            // 
            // WojewodztwoCombo
            // 
            this.WojewodztwoCombo.FormattingEnabled = true;
            this.WojewodztwoCombo.Items.AddRange(new object[] {
            "",
            "dolnośląskie",
            "kujawsko-pomorskie",
            "lubelskie",
            "lubuskie",
            "łódzkie",
            "małopolskie",
            "mazowieckie",
            "opolskie",
            "podkarpackie",
            "podlaskie",
            "pomorskie",
            "śląskie",
            "świętokrzyskie",
            "warmińsko-mazurskie",
            "wielkopolskie",
            "zachodniopomorskie"});
            this.WojewodztwoCombo.Location = new System.Drawing.Point(93, 120);
            this.WojewodztwoCombo.Name = "WojewodztwoCombo";
            this.WojewodztwoCombo.Size = new System.Drawing.Size(145, 21);
            this.WojewodztwoCombo.TabIndex = 6;
            this.WojewodztwoCombo.SelectedIndexChanged += new System.EventHandler(this.WojewodztwoCombo_SelectedIndexChanged);
            // 
            // GminaWyp
            // 
            this.GminaWyp.Location = new System.Drawing.Point(93, 89);
            this.GminaWyp.Name = "GminaWyp";
            this.GminaWyp.Size = new System.Drawing.Size(145, 20);
            this.GminaWyp.TabIndex = 5;
            // 
            // GminaWyp_Label
            // 
            this.GminaWyp_Label.AutoSize = true;
            this.GminaWyp_Label.Location = new System.Drawing.Point(6, 92);
            this.GminaWyp_Label.Name = "GminaWyp_Label";
            this.GminaWyp_Label.Size = new System.Drawing.Size(37, 13);
            this.GminaWyp_Label.TabIndex = 4;
            this.GminaWyp_Label.Text = "Gmina";
            // 
            // PowiatWyp
            // 
            this.PowiatWyp.Location = new System.Drawing.Point(93, 59);
            this.PowiatWyp.Name = "PowiatWyp";
            this.PowiatWyp.Size = new System.Drawing.Size(145, 20);
            this.PowiatWyp.TabIndex = 3;
            // 
            // PowiatWyp_Label
            // 
            this.PowiatWyp_Label.AutoSize = true;
            this.PowiatWyp_Label.Location = new System.Drawing.Point(6, 59);
            this.PowiatWyp_Label.Name = "PowiatWyp_Label";
            this.PowiatWyp_Label.Size = new System.Drawing.Size(39, 13);
            this.PowiatWyp_Label.TabIndex = 2;
            this.PowiatWyp_Label.Text = "Powiat";
            // 
            // MiastoWyp
            // 
            this.MiastoWyp.Location = new System.Drawing.Point(93, 30);
            this.MiastoWyp.Name = "MiastoWyp";
            this.MiastoWyp.Size = new System.Drawing.Size(145, 20);
            this.MiastoWyp.TabIndex = 1;
            // 
            // MiastoWyp_Label
            // 
            this.MiastoWyp_Label.AutoSize = true;
            this.MiastoWyp_Label.Location = new System.Drawing.Point(8, 30);
            this.MiastoWyp_Label.Name = "MiastoWyp_Label";
            this.MiastoWyp_Label.Size = new System.Drawing.Size(38, 13);
            this.MiastoWyp_Label.TabIndex = 0;
            this.MiastoWyp_Label.Text = "Miasto";
            // 
            // DataZwrotuLabel
            // 
            this.DataZwrotuLabel.AutoSize = true;
            this.DataZwrotuLabel.Location = new System.Drawing.Point(15, 240);
            this.DataZwrotuLabel.Name = "DataZwrotuLabel";
            this.DataZwrotuLabel.Size = new System.Drawing.Size(64, 13);
            this.DataZwrotuLabel.TabIndex = 9;
            this.DataZwrotuLabel.Text = "Data zwrotu";
            // 
            // DataZwrotu
            // 
            this.DataZwrotu.Location = new System.Drawing.Point(102, 238);
            this.DataZwrotu.Name = "DataZwrotu";
            this.DataZwrotu.Size = new System.Drawing.Size(145, 20);
            this.DataZwrotu.TabIndex = 7;
            this.DataZwrotu.ValueChanged += new System.EventHandler(this.DataZwrotu_ValueChanged);
            // 
            // WypKomu
            // 
            this.WypKomu.Location = new System.Drawing.Point(29, 41);
            this.WypKomu.Name = "WypKomu";
            this.WypKomu.Size = new System.Drawing.Size(218, 20);
            this.WypKomu.TabIndex = 0;
            // 
            // WypKomu_Label
            // 
            this.WypKomu_Label.AutoSize = true;
            this.WypKomu_Label.Location = new System.Drawing.Point(6, 25);
            this.WypKomu_Label.Name = "WypKomu_Label";
            this.WypKomu_Label.Size = new System.Drawing.Size(147, 13);
            this.WypKomu_Label.TabIndex = 1;
            this.WypKomu_Label.Text = "Nazwa instytucji / wydarzenia";
            // 
            // SaveBtn
            // 
            this.SaveBtn.Location = new System.Drawing.Point(477, 440);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveBtn.TabIndex = 6;
            this.SaveBtn.Text = "Zapisz";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // StanCombo
            // 
            this.StanCombo.FormattingEnabled = true;
            this.StanCombo.Items.AddRange(new object[] {
            "Wypożyczony",
            "W magazynie",
            "Ekspozycja stała",
            "Ekspozycja czasowa",
            "W serwisie"});
            this.StanCombo.Location = new System.Drawing.Point(15, 225);
            this.StanCombo.Name = "StanCombo";
            this.StanCombo.Size = new System.Drawing.Size(121, 21);
            this.StanCombo.TabIndex = 8;
            this.StanCombo.SelectedIndexChanged += new System.EventHandler(this.StanCombo_SelectedIndexChanged);
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(15, 206);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(29, 13);
            this.StatusLabel.TabIndex = 10;
            this.StatusLabel.Text = "Stan";
            // 
            // NazwaEksponatu
            // 
            this.NazwaEksponatu.AutoSize = true;
            this.NazwaEksponatu.Location = new System.Drawing.Point(16, 176);
            this.NazwaEksponatu.Name = "NazwaEksponatu";
            this.NazwaEksponatu.Size = new System.Drawing.Size(0, 13);
            this.NazwaEksponatu.TabIndex = 11;
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(24, 8);
            this.trackBar1.Maximum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(248, 45);
            this.trackBar1.TabIndex = 12;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // StopBatch
            // 
            this.StopBatch.Location = new System.Drawing.Point(344, 16);
            this.StopBatch.Name = "StopBatch";
            this.StopBatch.Size = new System.Drawing.Size(184, 23);
            this.StopBatch.TabIndex = 13;
            this.StopBatch.Text = "Zakończ inwentaryzację wsadową";
            this.StopBatch.UseVisualStyleBackColor = true;
            this.StopBatch.Click += new System.EventHandler(this.StopBatch_Click);
            // 
            // Inventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 669);
            this.Controls.Add(this.StopBatch);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.NazwaEksponatu);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.StanCombo);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.AdresGroupBox);
            this.Controls.Add(this.WynikLabel);
            this.Controls.Add(this.WynikTextBox);
            this.Controls.Add(this.PreviewLabel);
            this.Controls.Add(this.ScanQRBtn);
            this.Controls.Add(this.CapturePreview);
            this.Name = "Inventory";
            this.Text = "Inventory";
            ((System.ComponentModel.ISupportInitialize)(this.CapturePreview)).EndInit();
            this.AdresGroupBox.ResumeLayout(false);
            this.AdresGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox CapturePreview;
        private System.Windows.Forms.Button ScanQRBtn;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TextBox WynikTextBox;
        private System.Windows.Forms.Label WynikLabel;
        private System.Windows.Forms.GroupBox AdresGroupBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox MiastoWyp;
        private System.Windows.Forms.Label MiastoWyp_Label;
        private System.Windows.Forms.TextBox WypKomu;
        private System.Windows.Forms.Label WypKomu_Label;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.DateTimePicker DataZwrotu;
        private System.Windows.Forms.ComboBox StanCombo;
        private System.Windows.Forms.Label DataZwrotuLabel;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.TextBox PowiatWyp;
        private System.Windows.Forms.Label PowiatWyp_Label;
        private System.Windows.Forms.Label WojewodztwoWyp_Label;
        private System.Windows.Forms.ComboBox WojewodztwoCombo;
        private System.Windows.Forms.TextBox GminaWyp;
        private System.Windows.Forms.Label GminaWyp_Label;
        private System.Windows.Forms.Label NazwaEksponatu;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button StopBatch;
    }
}