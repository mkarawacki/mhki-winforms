﻿namespace RejestrZabytkówMHKI
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchModes = new System.Windows.Forms.TabControl();
            this.BasicSearch = new System.Windows.Forms.TabPage();
            this.SimpleSearchBtn = new System.Windows.Forms.Button();
            this.searchbox1 = new System.Windows.Forms.TextBox();
            this.AdvancedSearch = new System.Windows.Forms.TabPage();
            this.AdvSearchButton = new System.Windows.Forms.Button();
            this.RokProdCombo = new System.Windows.Forms.ComboBox();
            this.NazwaSearchBox = new System.Windows.Forms.TextBox();
            this.ProducentSearchBox = new System.Windows.Forms.TextBox();
            this.TypSearchBox = new System.Windows.Forms.TextBox();
            this.RokProdCheckBox = new System.Windows.Forms.CheckBox();
            this.NazwaCheckBox = new System.Windows.Forms.CheckBox();
            this.ProducentCheckBox = new System.Windows.Forms.CheckBox();
            this.TypCheckBox = new System.Windows.Forms.CheckBox();
            this.WynikiGrid = new System.Windows.Forms.DataGridView();
            this.SearchModes.SuspendLayout();
            this.BasicSearch.SuspendLayout();
            this.AdvancedSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WynikiGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // SearchModes
            // 
            this.SearchModes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchModes.Controls.Add(this.BasicSearch);
            this.SearchModes.Controls.Add(this.AdvancedSearch);
            this.SearchModes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SearchModes.ItemSize = new System.Drawing.Size(100, 21);
            this.SearchModes.Location = new System.Drawing.Point(10, 48);
            this.SearchModes.Margin = new System.Windows.Forms.Padding(12, 3, 12, 3);
            this.SearchModes.Name = "SearchModes";
            this.SearchModes.SelectedIndex = 0;
            this.SearchModes.ShowToolTips = true;
            this.SearchModes.Size = new System.Drawing.Size(813, 200);
            this.SearchModes.TabIndex = 0;
            // 
            // BasicSearch
            // 
            this.BasicSearch.Controls.Add(this.SimpleSearchBtn);
            this.BasicSearch.Controls.Add(this.searchbox1);
            this.BasicSearch.Location = new System.Drawing.Point(4, 25);
            this.BasicSearch.Name = "BasicSearch";
            this.BasicSearch.Padding = new System.Windows.Forms.Padding(3);
            this.BasicSearch.Size = new System.Drawing.Size(805, 171);
            this.BasicSearch.TabIndex = 0;
            this.BasicSearch.Text = "Proste wyszukiwanie";
            this.BasicSearch.UseVisualStyleBackColor = true;
            // 
            // SimpleSearchBtn
            // 
            this.SimpleSearchBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SimpleSearchBtn.FlatAppearance.BorderSize = 0;
            this.SimpleSearchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SimpleSearchBtn.Image = global::RejestrZabytkówMHKI.Properties.Resources.search;
            this.SimpleSearchBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SimpleSearchBtn.Location = new System.Drawing.Point(693, 53);
            this.SimpleSearchBtn.Margin = new System.Windows.Forms.Padding(0);
            this.SimpleSearchBtn.Name = "SimpleSearchBtn";
            this.SimpleSearchBtn.Size = new System.Drawing.Size(111, 47);
            this.SimpleSearchBtn.TabIndex = 1;
            this.SimpleSearchBtn.Text = "Szukaj";
            this.SimpleSearchBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SimpleSearchBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SimpleSearchBtn.UseVisualStyleBackColor = true;
            this.SimpleSearchBtn.Click += new System.EventHandler(this.searchbutton1_Click);
            // 
            // searchbox1
            // 
            this.searchbox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchbox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.searchbox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.searchbox1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchbox1.Location = new System.Drawing.Point(7, 59);
            this.searchbox1.Name = "searchbox1";
            this.searchbox1.Size = new System.Drawing.Size(682, 35);
            this.searchbox1.TabIndex = 0;
            this.searchbox1.TextChanged += new System.EventHandler(this.searchbox1_TextChanged);
            // 
            // AdvancedSearch
            // 
            this.AdvancedSearch.Controls.Add(this.AdvSearchButton);
            this.AdvancedSearch.Controls.Add(this.RokProdCombo);
            this.AdvancedSearch.Controls.Add(this.NazwaSearchBox);
            this.AdvancedSearch.Controls.Add(this.ProducentSearchBox);
            this.AdvancedSearch.Controls.Add(this.TypSearchBox);
            this.AdvancedSearch.Controls.Add(this.RokProdCheckBox);
            this.AdvancedSearch.Controls.Add(this.NazwaCheckBox);
            this.AdvancedSearch.Controls.Add(this.ProducentCheckBox);
            this.AdvancedSearch.Controls.Add(this.TypCheckBox);
            this.AdvancedSearch.Location = new System.Drawing.Point(4, 25);
            this.AdvancedSearch.Name = "AdvancedSearch";
            this.AdvancedSearch.Padding = new System.Windows.Forms.Padding(3);
            this.AdvancedSearch.Size = new System.Drawing.Size(805, 171);
            this.AdvancedSearch.TabIndex = 1;
            this.AdvancedSearch.Text = "Wyszukiwanie zaawansowane";
            this.AdvancedSearch.UseVisualStyleBackColor = true;
            this.AdvancedSearch.Click += new System.EventHandler(this.AdvancedSearch_Click);
            this.AdvancedSearch.Enter += new System.EventHandler(this.AdvancedSearch_Enter);
            // 
            // AdvSearchButton
            // 
            this.AdvSearchButton.FlatAppearance.BorderSize = 0;
            this.AdvSearchButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.AdvSearchButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.AdvSearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AdvSearchButton.Image = global::RejestrZabytkówMHKI.Properties.Resources.search1;
            this.AdvSearchButton.Location = new System.Drawing.Point(658, 22);
            this.AdvSearchButton.Name = "AdvSearchButton";
            this.AdvSearchButton.Size = new System.Drawing.Size(128, 128);
            this.AdvSearchButton.TabIndex = 8;
            this.AdvSearchButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AdvSearchButton.UseVisualStyleBackColor = true;
            this.AdvSearchButton.Click += new System.EventHandler(this.AdvSearchButton_Click);
            // 
            // RokProdCombo
            // 
            this.RokProdCombo.FormattingEnabled = true;
            this.RokProdCombo.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010",
            "2011",
            "2012",
            "2013"});
            this.RokProdCombo.Location = new System.Drawing.Point(161, 119);
            this.RokProdCombo.Name = "RokProdCombo";
            this.RokProdCombo.Size = new System.Drawing.Size(105, 24);
            this.RokProdCombo.TabIndex = 7;
            this.RokProdCombo.SelectedIndexChanged += new System.EventHandler(this.RokProdCombo_SelectedIndexChanged);
            // 
            // NazwaSearchBox
            // 
            this.NazwaSearchBox.Location = new System.Drawing.Point(161, 22);
            this.NazwaSearchBox.Name = "NazwaSearchBox";
            this.NazwaSearchBox.Size = new System.Drawing.Size(328, 22);
            this.NazwaSearchBox.TabIndex = 6;
            // 
            // ProducentSearchBox
            // 
            this.ProducentSearchBox.Enabled = false;
            this.ProducentSearchBox.Location = new System.Drawing.Point(161, 83);
            this.ProducentSearchBox.Name = "ProducentSearchBox";
            this.ProducentSearchBox.Size = new System.Drawing.Size(328, 22);
            this.ProducentSearchBox.TabIndex = 5;
            // 
            // TypSearchBox
            // 
            this.TypSearchBox.Location = new System.Drawing.Point(161, 55);
            this.TypSearchBox.Name = "TypSearchBox";
            this.TypSearchBox.Size = new System.Drawing.Size(328, 22);
            this.TypSearchBox.TabIndex = 4;
            // 
            // RokProdCheckBox
            // 
            this.RokProdCheckBox.AutoSize = true;
            this.RokProdCheckBox.Location = new System.Drawing.Point(35, 119);
            this.RokProdCheckBox.Name = "RokProdCheckBox";
            this.RokProdCheckBox.Size = new System.Drawing.Size(123, 20);
            this.RokProdCheckBox.TabIndex = 3;
            this.RokProdCheckBox.Text = "Rok produkcji";
            this.RokProdCheckBox.UseVisualStyleBackColor = true;
            this.RokProdCheckBox.CheckedChanged += new System.EventHandler(this.RokProdCheckBox_CheckedChanged);
            // 
            // NazwaCheckBox
            // 
            this.NazwaCheckBox.AutoSize = true;
            this.NazwaCheckBox.Location = new System.Drawing.Point(35, 24);
            this.NazwaCheckBox.Name = "NazwaCheckBox";
            this.NazwaCheckBox.Size = new System.Drawing.Size(73, 20);
            this.NazwaCheckBox.TabIndex = 2;
            this.NazwaCheckBox.Text = "Nazwa";
            this.NazwaCheckBox.UseVisualStyleBackColor = true;
            this.NazwaCheckBox.CheckedChanged += new System.EventHandler(this.NazwaCheckBox_CheckedChanged);
            // 
            // ProducentCheckBox
            // 
            this.ProducentCheckBox.AutoSize = true;
            this.ProducentCheckBox.Location = new System.Drawing.Point(35, 84);
            this.ProducentCheckBox.Name = "ProducentCheckBox";
            this.ProducentCheckBox.Size = new System.Drawing.Size(97, 20);
            this.ProducentCheckBox.TabIndex = 1;
            this.ProducentCheckBox.Text = "Producent";
            this.ProducentCheckBox.UseVisualStyleBackColor = true;
            this.ProducentCheckBox.CheckedChanged += new System.EventHandler(this.ProducentCheckBox_CheckedChanged);
            // 
            // TypCheckBox
            // 
            this.TypCheckBox.AutoSize = true;
            this.TypCheckBox.Location = new System.Drawing.Point(35, 55);
            this.TypCheckBox.Name = "TypCheckBox";
            this.TypCheckBox.Size = new System.Drawing.Size(54, 20);
            this.TypCheckBox.TabIndex = 0;
            this.TypCheckBox.Text = "Typ";
            this.TypCheckBox.UseVisualStyleBackColor = true;
            this.TypCheckBox.CheckedChanged += new System.EventHandler(this.TypCheckBox_CheckedChanged);
            // 
            // WynikiGrid
            // 
            this.WynikiGrid.AllowUserToAddRows = false;
            this.WynikiGrid.AllowUserToDeleteRows = false;
            this.WynikiGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WynikiGrid.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.WynikiGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.WynikiGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WynikiGrid.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.WynikiGrid.Location = new System.Drawing.Point(10, 255);
            this.WynikiGrid.Name = "WynikiGrid";
            this.WynikiGrid.ReadOnly = true;
            this.WynikiGrid.Size = new System.Drawing.Size(813, 213);
            this.WynikiGrid.TabIndex = 2;
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(838, 523);
            this.Controls.Add(this.WynikiGrid);
            this.Controls.Add(this.SearchModes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search";
            this.Load += new System.EventHandler(this.Search_Load);
            this.SearchModes.ResumeLayout(false);
            this.BasicSearch.ResumeLayout(false);
            this.BasicSearch.PerformLayout();
            this.AdvancedSearch.ResumeLayout(false);
            this.AdvancedSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WynikiGrid)).EndInit();
            this.ResumeLayout(false);

        }

        

        #endregion

        private System.Windows.Forms.TabControl SearchModes;
        private System.Windows.Forms.TabPage BasicSearch;
        private System.Windows.Forms.TabPage AdvancedSearch;
        private System.Windows.Forms.TextBox searchbox1;
        private System.Windows.Forms.Button SimpleSearchBtn;
        private System.Windows.Forms.TextBox NazwaSearchBox;
        private System.Windows.Forms.TextBox ProducentSearchBox;
        private System.Windows.Forms.TextBox TypSearchBox;
        private System.Windows.Forms.CheckBox RokProdCheckBox;
        private System.Windows.Forms.CheckBox NazwaCheckBox;
        private System.Windows.Forms.CheckBox ProducentCheckBox;
        private System.Windows.Forms.CheckBox TypCheckBox;
        private System.Windows.Forms.ComboBox RokProdCombo;
        private System.Windows.Forms.DataGridView WynikiGrid;
        private System.Windows.Forms.Button AdvSearchButton;
    }
}