﻿
namespace RejestrZabytkówMHKI
{
    partial class RibbonMgmt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CatPreviewLabel = new System.Windows.Forms.Label();
            this.moveCat = new System.Windows.Forms.Button();
            this.drzewo = new System.Windows.Forms.TreeView();
            this.label2 = new System.Windows.Forms.Label();
            this.PathLabel = new System.Windows.Forms.Label();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.delCat = new System.Windows.Forms.Button();
            this.addCat = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.AddExpBtn = new System.Windows.Forms.Button();
            this.ShowFullExpCard = new System.Windows.Forms.Button();
            this.wybranapozycja = new System.Windows.Forms.Label();
            this.CategoryPreview = new System.Windows.Forms.DataGridView();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.userControl11 = new RejestrZabytkówMHKI.UserControl1();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CategoryPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // CatPreviewLabel
            // 
            this.CatPreviewLabel.AutoSize = true;
            this.CatPreviewLabel.BackColor = System.Drawing.Color.Transparent;
            this.CatPreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CatPreviewLabel.Location = new System.Drawing.Point(3, 93);
            this.CatPreviewLabel.Name = "CatPreviewLabel";
            this.CatPreviewLabel.Size = new System.Drawing.Size(310, 17);
            this.CatPreviewLabel.TabIndex = 10;
            this.CatPreviewLabel.Text = "Uproszczona lista eksponatów w kategorii";
            // 
            // moveCat
            // 
            this.moveCat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moveCat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.moveCat.Location = new System.Drawing.Point(3, 60);
            this.moveCat.Name = "moveCat";
            this.moveCat.Size = new System.Drawing.Size(108, 23);
            this.moveCat.TabIndex = 12;
            this.moveCat.Text = "Przenieś kategorię";
            this.moveCat.UseVisualStyleBackColor = true;
            this.moveCat.Click += new System.EventHandler(this.moveCat_Click);
            // 
            // drzewo
            // 
            this.drzewo.BackColor = System.Drawing.Color.White;
            this.drzewo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.drzewo.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.drzewo.Location = new System.Drawing.Point(0, 19);
            this.drzewo.Name = "drzewo";
            this.drzewo.Size = new System.Drawing.Size(225, 456);
            this.drzewo.TabIndex = 9;
            this.drzewo.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.drzewo_AfterSelect);
            this.drzewo.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.drzewo_NodeMouseClick);
            this.drzewo.DoubleClick += new System.EventHandler(this.drzewo_DoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Kategorie rejestru";
            // 
            // PathLabel
            // 
            this.PathLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PathLabel.AutoSize = true;
            this.PathLabel.Location = new System.Drawing.Point(4, 536);
            this.PathLabel.Name = "PathLabel";
            this.PathLabel.Size = new System.Drawing.Size(81, 13);
            this.PathLabel.TabIndex = 15;
            this.PathLabel.Text = "Pełna ścieżka: ";
            // 
            // statusBar
            // 
            this.statusBar.AllowMerge = false;
            this.statusBar.Location = new System.Drawing.Point(0, 530);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(801, 22);
            this.statusBar.SizingGrip = false;
            this.statusBar.Stretch = false;
            this.statusBar.TabIndex = 16;
            this.statusBar.Text = "statusStrip1";
            // 
            // delCat
            // 
            this.delCat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delCat.Image = global::RejestrZabytkówMHKI.Properties.Resources.editdelete;
            this.delCat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.delCat.Location = new System.Drawing.Point(152, 8);
            this.delCat.Name = "delCat";
            this.delCat.Size = new System.Drawing.Size(108, 23);
            this.delCat.TabIndex = 11;
            this.delCat.Text = "Usuń kategorię";
            this.delCat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.delCat.UseVisualStyleBackColor = true;
            this.delCat.Click += new System.EventHandler(this.delCat_Click);
            // 
            // addCat
            // 
            this.addCat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addCat.Image = global::RejestrZabytkówMHKI.Properties.Resources.add;
            this.addCat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addCat.Location = new System.Drawing.Point(24, 8);
            this.addCat.Name = "addCat";
            this.addCat.Size = new System.Drawing.Size(108, 23);
            this.addCat.TabIndex = 13;
            this.addCat.Text = "Nowa kategoria";
            this.addCat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addCat.UseVisualStyleBackColor = true;
            this.addCat.Click += new System.EventHandler(this.addCat_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContainer1.Location = new System.Drawing.Point(0, 138);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.drzewo);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.AddExpBtn);
            this.splitContainer1.Panel2.Controls.Add(this.ShowFullExpCard);
            this.splitContainer1.Panel2.Controls.Add(this.wybranapozycja);
            this.splitContainer1.Panel2.Controls.Add(this.CategoryPreview);
            this.splitContainer1.Panel2.Controls.Add(this.moveCat);
            this.splitContainer1.Panel2.Controls.Add(this.CatPreviewLabel);
            this.splitContainer1.Panel2.Controls.Add(this.addCat);
            this.splitContainer1.Panel2.Controls.Add(this.delCat);
            this.splitContainer1.Size = new System.Drawing.Size(801, 392);
            this.splitContainer1.SplitterDistance = 150;
            this.splitContainer1.TabIndex = 17;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // AddExpBtn
            // 
            this.AddExpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddExpBtn.Location = new System.Drawing.Point(480, 8);
            this.AddExpBtn.Name = "AddExpBtn";
            this.AddExpBtn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AddExpBtn.Size = new System.Drawing.Size(160, 24);
            this.AddExpBtn.TabIndex = 17;
            this.AddExpBtn.Text = "Dodaj eksponat do kategorii";
            this.AddExpBtn.UseVisualStyleBackColor = true;
            this.AddExpBtn.Click += new System.EventHandler(this.AddExpBtn_Click);
            // 
            // ShowFullExpCard
            // 
            this.ShowFullExpCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShowFullExpCard.Location = new System.Drawing.Point(312, 8);
            this.ShowFullExpCard.Name = "ShowFullExpCard";
            this.ShowFullExpCard.Size = new System.Drawing.Size(161, 23);
            this.ShowFullExpCard.TabIndex = 16;
            this.ShowFullExpCard.Text = "Pokaż pełną kartę eksponatu";
            this.ShowFullExpCard.UseVisualStyleBackColor = true;
            this.ShowFullExpCard.Click += new System.EventHandler(this.ShowFullExpCard_Click);
            // 
            // wybranapozycja
            // 
            this.wybranapozycja.AutoSize = true;
            this.wybranapozycja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wybranapozycja.Location = new System.Drawing.Point(319, 94);
            this.wybranapozycja.Name = "wybranapozycja";
            this.wybranapozycja.Size = new System.Drawing.Size(0, 16);
            this.wybranapozycja.TabIndex = 15;
            // 
            // CategoryPreview
            // 
            this.CategoryPreview.AllowUserToAddRows = false;
            this.CategoryPreview.AllowUserToDeleteRows = false;
            this.CategoryPreview.BackgroundColor = System.Drawing.SystemColors.Control;
            this.CategoryPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CategoryPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CategoryPreview.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.CategoryPreview.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.CategoryPreview.Location = new System.Drawing.Point(0, 48);
            this.CategoryPreview.Name = "CategoryPreview";
            this.CategoryPreview.ReadOnly = true;
            this.CategoryPreview.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.CategoryPreview.RowHeadersVisible = false;
            this.CategoryPreview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.CategoryPreview.ShowEditingIcon = false;
            this.CategoryPreview.Size = new System.Drawing.Size(643, 340);
            this.CategoryPreview.TabIndex = 14;
            this.CategoryPreview.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CategoryPreview_CellContentClick);
            // 
            // elementHost1
            // 
            this.elementHost1.BackColorTransparent = true;
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(801, 552);
            this.elementHost1.TabIndex = 18;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.ChildChanged += new System.EventHandler<System.Windows.Forms.Integration.ChildChangedEventArgs>(this.elementHost1_ChildChanged);
            this.elementHost1.Child = this.userControl11;
            // 
            // RibbonMgmt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 552);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.PathLabel);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.elementHost1);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "RibbonMgmt";
            this.Text = "Rejestr Zabytków Muzeum Historii Komputerów i Informatyki - Zarządzanie rejestrem" +
    "";
            this.Load += new System.EventHandler(this.RejestrZabytków_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CategoryPreview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CatPreviewLabel;
        private System.Windows.Forms.Button delCat;
        private System.Windows.Forms.Button addCat;
        private System.Windows.Forms.Button moveCat;
        private System.Windows.Forms.TreeView drzewo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label PathLabel;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView CategoryPreview;
        private System.Windows.Forms.Label wybranapozycja;
        private System.Windows.Forms.Button ShowFullExpCard;
        private System.Windows.Forms.Button AddExpBtn;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private UserControl1 userControl11;
        
    }
}

