﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using WindowsFormsAero.TaskDialog;

namespace RejestrZabytkówMHKI
{
    public partial class UserAdm : Form
    {
        private string sel_uname,nowagrupa,grupanowego;
        private int ileroot, ilebrowse, ileinv,newuserid;
        public UserAdm()
        {
            InitializeComponent();
            
            WypelnijListView();
            
            ViewMode.SelectedItem = "Szczegóły";
            ModUserPanel.Visible = false;
            AddUserPanel.Visible = false;
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            string policzroot = "SELECT COUNT(uid) AS ileroot FROM users WHERE grupa='root'";
            string policzbrowse = "SELECT COUNT(uid) AS ilebrowse FROM users WHERE grupa='browse'";
            string policzinv = "SELECT COUNT(uid) AS ileinv FROM users as u1 WHERE grupa='invmgr'";
            MySqlCommand ile = new MySqlCommand(policzroot, link);
            link.Open();
            ileroot = Convert.ToInt16(ile.ExecuteScalar());
            ile.CommandText = policzbrowse;
            ilebrowse = Convert.ToInt16(ile.ExecuteScalar());
            ile.CommandText = policzinv;
            ileinv = Convert.ToInt16(ile.ExecuteScalar());
            link.Close();
            
            
        }
        private void WypelnijPictureBox()
        {
            MySqlConnection polaczenie1 = new MySqlConnection(login.danepolaczenia);
            string pobierzobraz = "SELECT avatar FROM users WHERE nazwa='" + sel_uname+"'";
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(new MySqlCommand(pobierzobraz, polaczenie1));
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count == 1)
            {

                Byte[] data = new Byte[0];
                data = (Byte[])(dataSet.Tables[0].Rows[0]["avatar"]);
                MemoryStream mem = new MemoryStream(data);
                Avatar.Image = Image.FromStream(mem);
                
            }
        }

        private void WypelnijListView() 
        {
            UserList.View = View.Details;
            UserList.FullRowSelect = true;
            UserList.Columns.Add("Imię", 80);
            UserList.Columns.Add("Nazwisko", 100);
            UserList.Columns.Add("Nazwa użytkownika", 100);
            UserList.Columns.Add("Adres e-mail", 100);
            UserList.Columns.Add("Grupa", 80);
            //WypelnijImageList();
            string previewquery = "SELECT imienazw,nazwa,email,grupa FROM users ORDER BY uid ASC";
            //Console.WriteLine(previewquery);
            MySqlConnection polaczenie = new MySqlConnection(login.danepolaczenia);
            MySqlCommand prevcomm = new MySqlCommand();
            prevcomm.CommandText = previewquery;
            prevcomm.Connection = polaczenie;
            DataTable dt_userlist = new DataTable();
            MySqlDataAdapter da_userlist = new MySqlDataAdapter(prevcomm);
            da_userlist.Fill(dt_userlist);

            MySqlDataReader dr_lista;
            try
            {
                polaczenie.Open();
                dr_lista = prevcomm.ExecuteReader();
                int i = 0;
                while (dr_lista.Read())
                {

                    UserList.Items.Add(new ListViewItem(new string[]{
              (!String.IsNullOrEmpty(dt_userlist.Rows[i]["imienazw"].ToString()))?dt_userlist.Rows[i]["imienazw"].ToString().Split(new String[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[0]:"" ,
              (!String.IsNullOrEmpty(dt_userlist.Rows[i]["imienazw"].ToString()))?dt_userlist.Rows[i]["imienazw"].ToString().Split(new String[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[1]:"",
              (!String.IsNullOrEmpty(dt_userlist.Rows[i]["nazwa"].ToString()))?dt_userlist.Rows[i]["nazwa"].ToString():"",
              (!String.IsNullOrEmpty(dt_userlist.Rows[i]["email"].ToString()))?dt_userlist.Rows[i]["email"].ToString():"",
              (!String.IsNullOrEmpty(dt_userlist.Rows[i]["grupa"].ToString()))?dt_userlist.Rows[i]["grupa"].ToString():""}));
                    i++;
                }
            }
            catch
            {

            }
        }

        /*private void WypelnijImageList()
        {
            //imageList.Images.Clear();
            MySqlConnection polaczenie1 = new MySqlConnection(login.danepolaczenia);
            string pobierzobraz = "SELECT matgraficzne FROM eksponaty WHERE kategoria='" + nodevalue + "' ORDER BY identyfikator ASC";
            //Console.WriteLine(pobierzobraz);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(new MySqlCommand(pobierzobraz, polaczenie1));
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            //Console.WriteLine("Obrazów dla kategorii " + nodevalue + " jest " + dataSet.Tables[0].Rows.Count);
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {

                Byte[] data = new Byte[0];
                data = (Byte[])(dataSet.Tables[0].Rows[i]["matgraficzne"]);
                MemoryStream mem = new MemoryStream(data);
                Image img = Image.FromStream(mem);

                imageList.Images.Add(img);
            }
        }*/
        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        

        private void UserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            sel_uname = UserList.SelectedItems[0].SubItems[2].Text;    
        }

        private void ViewMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tryb = ViewMode.SelectedItem.ToString();
            switch (tryb) 
            {
                case "Duże ikony": { UserList.View = View.LargeIcon; break; }
                case "Małe ikony": { UserList.View = View.SmallIcon; break; }
                case "Lista": { UserList.View = View.List; break; }
                case "Szczegóły": { UserList.View = View.Details; break; }
                default: { UserList.View = View.Details; break; }
            
            }
        }

        #region USUWANIE ZAZNACZONEGO UŻYTKOWNIKA
        private void DelUserBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(sel_uname)) 
            {
                string delusrquery = "DELETE FROM `users` WHERE nazwa='" + sel_uname + "'";
                if (MessageBox.Show("Czy na pewno chcesz usunąć tego użytkownika?", "Prośba o potwierdzenie usunięcia użytkownika" + sel_uname, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) 
                {
                    try
                    {
                        MySqlConnection deluserlink = new MySqlConnection(login.danepolaczenia);
                        MySqlCommand delusercmd = new MySqlCommand(delusrquery, deluserlink);
                        deluserlink.Open();
                        delusercmd.ExecuteNonQuery();
                        deluserlink.Close();
                        UserList.Items.RemoveByKey(sel_uname);
                        //UserList.RedrawItems(0, (int)UserList.Items.Count, false);
                        UserList.Clear();
                        WypelnijListView();
                        MySqlConnection link = new MySqlConnection(login.danepolaczenia);
                        string policzroot = "SELECT COUNT(uid) AS ileroot FROM users WHERE grupa='root'";
                        string policzbrowse = "SELECT COUNT(uid) AS ilebrowse FROM users WHERE grupa='browse'";
                        string policzinv = "SELECT COUNT(uid) AS ileinv FROM users as u1 WHERE grupa='invmgr'";
                        MySqlCommand ile = new MySqlCommand(policzroot, link);
                        link.Open();
                        ileroot = Convert.ToInt16(ile.ExecuteScalar());
                        ile.CommandText = policzbrowse;
                        ilebrowse = Convert.ToInt16(ile.ExecuteScalar());
                        ile.CommandText = policzinv;
                        ileinv = Convert.ToInt16(ile.ExecuteScalar());
                        link.Close();
                        Console.WriteLine("|| Root || = " + ileroot + "\n || Browse || =" + ilebrowse + "\n || InvMgr || = " + ileinv);
                    }
                    catch (MySqlException dbex) 
                    {
                        MessageBox.Show("Podczas usuwania użytkownika wystąpił błąd:\n"+dbex.Message, "Błąd usuwania użytkownika!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                    
                }
            }
            else 
            {
                MessageBox.Show("Wybierz użytkownika, którego konto chcesz usunąć", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); 
            }
        }
#endregion
        
        
        
        #region MODYFIKACJA DANYCH WYBRANEGO UŻYTKOWNIKA
        //otwiera panel modyfikacji danych użytkownika
        private void ModifyUserBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(sel_uname)) 
            {
                WypelnijPictureBox();
                ModUserPanel.Visible = true;
                Avatar.Visible = true;
                ChangeAvatarBtn.Visible = SaveNewAvatar.Visible = AvatarPath.Visible =false;
                SaveNewPasswdBtn.Visible = NewPasswd.Visible = RepeatNewPasswd.Visible = false;
                NoweHaslo.Visible = PowtorzNoweHaslo.Visible = false;
                ModifyUsrData.Visible = false;
                UserInfo.Visible = false;
                switch(UserList.SelectedItems[0].SubItems[4].Text)
                {
                    case "root": GrupyUpr.SelectedItem = "Administracja (root)"; break;
                    case "browse":GrupyUpr.SelectedItem = "Przeglądanie (browse)";break;
                    case "invmgr": GrupyUpr.SelectedItem = "Inwentaryzacja (invmgr)"; break;
                }
                
                ImieLabel.Text = UserList.SelectedItems[0].SubItems[0].Text;
                SurnameLabel.Text = UserList.SelectedItems[0].SubItems[1].Text;
                
            }
            else 
            {
                MessageBox.Show("Wybierz użytkownika, którego konto chcesz zmodyfikować", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); 
            }
        }

        #region zmiana hasła
       

        private void ZmienHaslo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SaveNewPasswdBtn.Visible = NewPasswd.Visible = Avatar.Visible = RepeatNewPasswd.Visible = true;
            NoweHaslo.Visible = PowtorzNoweHaslo.Visible = true;
            AvatarPath.Visible = ChangeAvatarBtn.Visible = SaveNewAvatar.Visible = false;
            //ModifyUserBtn.Visible = false;
            UserInfo.Visible = false;
            
            
        }
        private void SaveNewPasswdBtn_Click(object sender, EventArgs e)
        {
            if (NewPasswd.Text == RepeatNewPasswd.Text)
            {
                string newmd5 = CalculateMD5Hash(NewPasswd.Text);
                MySqlConnection updatepasswdlink = new MySqlConnection(login.danepolaczenia);
                string updatepasswd = "UPDATE `users` SET haslo='" + newmd5 + "' WHERE nazwa=" + sel_uname;
                MySqlCommand updatepasscmd = new MySqlCommand(updatepasswd, updatepasswdlink);
                updatepasswdlink.Open();
                updatepasscmd.ExecuteNonQuery();
                updatepasswdlink.Close();

                TaskDialog.Show("Od tej pory używaj nowego hasła by zalogować się do systemu.", "Sukces", "Zmiana hasła zakończona pomyślnie", TaskDialogButton.Retry, TaskDialogIcon.SecuritySuccess);

            }
            else
            {
                TaskDialog.Show("Upewnij się, że w obu polach wpisane jest to samo nowe hasło. Zwróć uwagę na wielkość liter.", "Błąd zmiany hasła", "Podane hasła nie są identyczne", TaskDialogButton.Retry, TaskDialogIcon.Stop);
            }
        }
        #endregion

        #region zmiana obrazka
        private void ZmienAvatar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ChangeAvatarBtn.Visible = Avatar.Visible = AvatarPath.Visible = true;
            SaveNewPasswdBtn.Visible = NewPasswd.Visible = RepeatNewPasswd.Visible = false;
            //ModifyUserBtn.Visible = false;
            NoweHaslo.Visible = PowtorzNoweHaslo.Visible = false;
            UserInfo.Visible = false;
            AvatarPath.Location = new Point((int)(RepeatNewPasswd.Location.X), RepeatNewPasswd.Location.Y+50);
            
        }


        private void ChangeAvatarBtn_Click(object sender, EventArgs e)
        {
            
            NewAvatarDialog.Filter = "Obraz w formacie JPG (*.jpg)|*.jpg|Obraz w formacie PNG (*.png)|*.png";
            NewAvatarDialog.Title = "Wybierz obraz dla konta użytkownika - maksymalna rozdzielczość 64x64 px";
            NewAvatarDialog.ShowDialog();
            if (NewAvatarDialog.ShowDialog() == DialogResult.OK)
            {
                string polozenie = NewAvatarDialog.FileName;
                AvatarPath.Text = polozenie;

                if (Avatar.Image.Width <= 64 && Avatar.Image.Height <= 64)
                {
                    Avatar.ImageLocation = NewAvatarDialog.FileName;//.Replace("\\", "\\\\");
                    SaveNewAvatar.Visible = true;
                }
                else MessageBox.Show("Dopuszczalny maksymalny rozmiar obrazka to 64 x 64 piksele", "Błąd! Obrazek jest za duży", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void AddUser_InsertImg_Click(object sender, EventArgs e)
        {
            NewAvatarDialog.Filter = "Obraz w formacie JPG (*.jpg)|*.jpg|Obraz w formacie PNG (*.png)|*.png";
            NewAvatarDialog.Title = "Wybierz obraz dla konta użytkownika - maksymalna rozdzielczość 64x64 px";
            NewAvatarDialog.ShowDialog();
            if (NewAvatarDialog.ShowDialog() == DialogResult.OK)
            {
                string polozenie = NewAvatarDialog.FileName;
                AvatarPath.Text = polozenie;

                if (Avatar.Image.Width <= 64 && Avatar.Image.Height <= 64)
                {
                    Avatar.ImageLocation = NewAvatarDialog.FileName;//.Replace("\\", "\\\\");
                    SaveNewAvatar.Visible = true;
                }
                else MessageBox.Show("Dopuszczalny maksymalny rozmiar obrazka to 64 x 64 piksele", "Błąd! Obrazek jest za duży", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void SaveNewAvatar_Click(object sender, EventArgs e)
        {
            FileStream strumien = new FileStream(AvatarPath.Text, FileMode.Open, FileAccess.Read);
            BinaryReader czytbajt = new BinaryReader(strumien);
            byte[] zdjbajt = null;
            zdjbajt = czytbajt.ReadBytes((int)strumien.Length);
            string query = "UPDATE `users` SET avatar=@IMG WHERE nazwa=" + sel_uname;
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            MySqlCommand zapiszcmd = new MySqlCommand(query, link);
            MySqlDataReader czytnikSQL;
            try
            {
                link.Open();
                zapiszcmd.Parameters.Add(new MySqlParameter("@IMG", zdjbajt));
                czytnikSQL = zapiszcmd.ExecuteReader();
                while (czytnikSQL.Read()) { }
                MessageBox.Show("Pomyślnie zmieniono obrazek dla Twojego konta użytkownika.");
            }
            catch (MySqlException sqlexc)
            {
                TaskDialog.Show("Skontaktuj się z administratorem", "Błąd operacji bazodanowych", "Wystąpił błąd bazy danych\n" + sqlexc.Message, TaskDialogButton.OK, TaskDialogIcon.SecurityError);
            }
        }

        #endregion

        #region zmiana e-maila, imienia i nazwiska

        private void ZmienDane_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ModifyUserBtn.Visible =Avatar.Visible=true;
            UserInfo.Visible = true;
            AvatarPath.Visible = ChangeAvatarBtn.Visible = SaveNewAvatar.Visible = false;
            SaveNewPasswdBtn.Visible = NewPasswd.Visible = RepeatNewPasswd.Visible = false;
            NoweHaslo.Visible = PowtorzNoweHaslo.Visible = false;
            
            UserInfo.Location = new Point(NewPasswd.Location.X,NewPasswd.Location.Y);

            
            Imie.Text = UserList.SelectedItems[0].SubItems[0].Text;//info_dt.Rows[0]["imienazw"].ToString().Split(new String[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[0];
            Nazwisko.Text = UserList.SelectedItems[0].SubItems[1].Text;//info_dt.Rows[0]["imienazw"].ToString().Split(new String[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[1];
            AdresEMail.Text = UserList.SelectedItems[0].SubItems[3].Text;//info_dt.Rows[0]["email"].ToString();
        }
        private void GrupyUpr_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (GrupyUpr.SelectedItem.ToString()) 
            {
                case "Przeglądanie (browse)": { nowagrupa = "browse";  break; }
                case "Inwentaryzacja (invmgr)": { nowagrupa = "invmgr"; break; }
                case "Administracja (root)": { nowagrupa = "root";  break; }
                case "Grupy uprawnień": 
                default: MessageBox.Show("Użytkownik musi należeć do którejś z grup!", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); break;
            }// nowagrupa = 
        }

        private void ModifyUsrData_Click(object sender, EventArgs e)
        {
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            string fillinfo = "UPDATE `users SET ";
            fillinfo+="imienazw='"+String.Concat(Imie.Text,"_",Nazwisko.Text)+"', email='"+AdresEMail.Text+"', grupa='"+nowagrupa+"' WHERE nazwa='" + sel_uname + "'";
            MySqlCommand fillinfocmd = new MySqlCommand(fillinfo, link);
            link.Open();
            fillinfocmd.ExecuteNonQuery();
            link.Close();
        }

        #endregion
        private void CloseModUsrPanel_Click(object sender, EventArgs e)
        {
            ModUserPanel.Visible = false;
        }

        

        
        #endregion

        #region DODAWANIE NOWEGO UŻYTKOWNIKA
//Włączenie widoczności panelu dodawania użytkownika
        private void AddUserBtn_Click(object sender, EventArgs e)
        {
            AddUserPanel.Visible = true;
            AddUserPanel.BringToFront();
            AddAvatar.Visible = true;
            SaveNewAvatar.Visible = false;
            Avatar.Visible = AvatarPath.Visible = true;
            ChangeAvatarBtn.Visible = false;
            //ChangeAvatarBtn.Text = "Wybierz obrazek użytkownika (avatar)";
            //ChangeAvatarBtn.BringToFront();
            NewUser_Grupa.SelectedItem = "Grupa uprawnień:";
        }
        
       
//przypisanie do grupy uprawnień
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (NewUser_Grupa.SelectedItem.ToString()) 
            {
                case "Administracja (root)": { grupanowego = "root"; newuserid = ileroot; break; }
                case "Inwentaryzacja (invmgr)": { grupanowego = "invmgr"; newuserid = 2000 + ileinv; break; }
                case "Przeglądanie (browse)": { grupanowego = "browse"; newuserid = 1000 + ilebrowse; break; }
            }
        }

        //zapisanie w bazie danych
        private void SaveNewUser_Click(object sender, EventArgs e)
        {
            

            FileStream strumien = new FileStream(NewAvatarDialog.FileName, FileMode.Open, FileAccess.Read);
            BinaryReader czytbajt = new BinaryReader(strumien);
            byte[] zdjbajt = null;
            zdjbajt = czytbajt.ReadBytes((int)strumien.Length);
            string addnewuser = "INSERT INTO `users` (`uid`,`nazwa`,`imienazw`,`haslo`,`grupa`,`email`,`avatar`) VALUES ";
            addnewuser += "(" + newuserid + "," + NewUserName.Text + "," + String.Concat(NewUser_Imie.Text, "_", NewUser_Nazwisko.Text) + "," + CalculateMD5Hash(NewUserPasswd.Text) + "," + grupanowego + "," + NewUser_EMail + ", @IMG)";
            MySqlConnection adduserlink = new MySqlConnection(login.danepolaczenia);
            MySqlCommand addcmd = new MySqlCommand(addnewuser, adduserlink);
            MySqlDataReader czytnikSQL;
            try
            {
                adduserlink.Open();
                addcmd.Parameters.Add(new MySqlParameter("@IMG", zdjbajt));
                czytnikSQL = addcmd.ExecuteReader();
                while (czytnikSQL.Read()) { }
                MessageBox.Show("Pomyślnie dodano nowego użytkownika "+NewUserName.Text+".");
            }
            catch (MySqlException sqlexc)
            {
                TaskDialog.Show("Skontaktuj się z administratorem", "Błąd operacji bazodanowych", "Przy dodawaniu nowego użytkownika wystąpił błąd bazy danych\n" + sqlexc.Message, TaskDialogButton.OK, TaskDialogIcon.SecurityError);
            }
        
        }
        //zamknięcie panelu dodawania użytkownika
        private void CloseUserAddPanel_Click(object sender, EventArgs e)
        {
            AddUserPanel.Visible = false;
        }


        #endregion

        private void NewAvatarDialog_FileOk(object sender, CancelEventArgs e)
        {

        }

        

        

    }
}
