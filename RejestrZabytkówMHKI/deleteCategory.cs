﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using Renci.SshNet.Common;

namespace RejestrZabytkówMHKI
{
    public partial class deleteCategory : Form
    {
        string kategoria,cmd;
        bool DeleteWithLeaves;
        private string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();
        MySqlConnection link;
        public deleteCategory(string cat)
        {
            kategoria = cat;
            InitializeComponent();
            CategoryName.Text = kategoria;
        }

        private string makeQueryCloseGaps(string kategoria)
        {
            string query = "";
            return query;
        }

        private void Usun_Click(object sender, EventArgs e)
        {

            try
            {
                link =  new MySqlConnection(danepolaczenia);
            link.Open();
            //wspólne
            cmd = "LOCK TABLE rejestr WRITE";
            MySqlCommand polecenie = new MySqlCommand(cmd, link);
            polecenie.ExecuteNonQuery();
            polecenie.Dispose();
            if(DeleteWithLeaves)
            {
                polecenie.CommandText = "SELECT lewy, prawy FROM rejestr WHERE skr = '" + kategoria + "' ";
                MySqlDataAdapter da = new MySqlDataAdapter(polecenie);
                DataTable dt = new DataTable();
                da.Fill(dt);
                da.Dispose();
                int lewy = Convert.ToInt16(dt.Rows[0][0]);
                int prawy = Convert.ToInt16(dt.Rows[0][1]);
                int szerokosc = prawy - lewy + 1;
                polecenie.Dispose();
                polecenie.CommandText = "DELETE FROM rejestr WHERE lewy BETWEEN "+lewy+" AND "+prawy +"";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText= "UPDATE rejestr SET prawy = prawy - "+szerokosc+" WHERE  prawy > "+prawy+"";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText = "UPDATE rejestr SET lewy = lewy - "+szerokosc+" WHERE lewy > "+prawy+"";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText= "UNLOCK TABLES";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText = "CALL x2()";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                if (MessageBox.Show("Pomyślnie usunięto kategorię " + kategoria +" wraz z jej podkategoriami", "Potwierdzenie usunięcia kategorii", MessageBoxButtons.OK) == DialogResult.OK) this.Close();
            }
            else 
            {
                polecenie.CommandText = "SELECT lewy, prawy FROM rejestr WHERE skr = '" + kategoria + "' ";
                MySqlDataAdapter da = new MySqlDataAdapter(polecenie);
                DataTable dt = new DataTable();
                da.Fill(dt);
                da.Dispose();
                int lewy = Convert.ToInt16(dt.Rows[0][0]);
                int prawy = Convert.ToInt16(dt.Rows[0][1]);
                int szerokosc = prawy - lewy + 1;
                polecenie.Dispose();
                polecenie.CommandText= "DELETE FROM rejestr WHERE lewy = "+lewy+"";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText = "UPDATE rejestr SET prawy = prawy - 1, lewy = lewy - 1 WHERE lewy BETWEEN "+lewy+" AND "+prawy+"";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText= "UPDATE rejestr SET prawy = prawy - 2 WHERE prawy > "+prawy+"";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText= "UPDATE rejestr SET lewy = lewy - 2 WHERE lewy > "+prawy+"";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                polecenie.CommandText= "UNLOCK TABLES";
                polecenie.Dispose();
                polecenie.CommandText = "CALL x2()";
                polecenie.ExecuteNonQuery();
                polecenie.Dispose();
                if (MessageBox.Show("Pomyślnie usunięto kategorię " +kategoria, "Potwierdzenie usunięcia kategorii", MessageBoxButtons.OK) == DialogResult.OK) this.Close();
            }
            
                
            }
            catch (MySqlException mysql_error)
            {
                MessageBox.Show(mysql_error.ToString());
            }

        }

        private void UsunRazemZLiscmi_CheckedChanged(object sender, EventArgs e)
        {
            if (UsunRazemZLiscmi.Checked) DeleteWithLeaves = true;
            else DeleteWithLeaves = false;
        }

        private void deleteCategory_Load(object sender, EventArgs e)
        {

        }
    }
}
