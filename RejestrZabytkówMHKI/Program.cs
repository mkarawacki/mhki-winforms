﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RejestrZabytkówMHKI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            login loginform = new login();
            Application.Run(loginform);
            if (loginform.zalogowany && login.grupa=="root")
            {
                Application.Run(new ControlPanel());
            }
            if (loginform.zalogowany && login.grupa == "browse") 
            {
                Application.Run(new Search());
            }
            if (loginform.zalogowany && login.grupa == "invmgr")
            {
                Application.Run(new Inventory());
            }
        }
    }
}
