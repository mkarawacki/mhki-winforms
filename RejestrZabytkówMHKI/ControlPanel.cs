﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsAero.TaskDialog;
using MySql.Data.MySqlClient;
using System.IO;
namespace RejestrZabytkówMHKI
{
    public partial class ControlPanel : Form
    {
        public ControlPanel()
        {
            InitializeComponent();
            ChangePasswordPanel.Visible = false;
            ChangeAvatarPanel.Visible = false;
            WypelnijPictureBox();
            ImieNazwiskoLabel.Text = login.imie_nazw.Split(new String[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[0] + "\n" + login.imie_nazw.Split(new String[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[1] +"\n ("+login.nazwauzytkownika+")";//login.nazwauzytkownika;
        }
        private void WypelnijPictureBox()
        {
            MySqlConnection polaczenie1 = new MySqlConnection(login.danepolaczenia);
            string pobierzobraz = "SELECT avatar FROM users WHERE uid=" + login.uid;
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(new MySqlCommand(pobierzobraz, polaczenie1));
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count == 1)
            {

                Byte[] data = new Byte[0];
                data = (Byte[])(dataSet.Tables[0].Rows[0]["avatar"]);
                MemoryStream mem = new MemoryStream(data);
                AvatarPictureBox.Image = NewAvatar.Image = Image.FromStream(mem);
            }
        }
        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Search szukaj = new Search();
            szukaj.Show();
        }

        private void ControlPanel_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            RejestrZabytków manager = new RejestrZabytków();
            manager.Show();
        }

        private void regInv_Click(object sender, EventArgs e)
        {
            Inventory inw = new Inventory();
            inw.Show();
        }

        private void zakończToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void UserAdmBtn_Click(object sender, EventArgs e)
        {
            UserAdm UserAdmin = new UserAdm();
            UserAdmin.Show();
        }

        private void ChangePasswdLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ChangePasswordPanel.Visible = true;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            ChangePasswordPanel.Visible = false;
        }

        private void SaveNewPasswdBtn_Click(object sender, EventArgs e)
        {
            if(NewPasswd.Text==RepeatNewPasswd.Text)
            {
                string newmd5=CalculateMD5Hash(NewPasswd.Text);
                MySqlConnection updatepasswdlink = new MySqlConnection(login.danepolaczenia);
                string updatepasswd = "UPDATE `users` SET haslo='"+newmd5+"' WHERE uid="+login.uid;
                MySqlCommand updatepasscmd = new MySqlCommand(updatepasswd, updatepasswdlink);
                updatepasswdlink.Open();
                updatepasscmd.ExecuteNonQuery();
                updatepasswdlink.Close();

                TaskDialog.Show("Od tej pory używaj nowego hasła by zalogować się do systemu.","Sukces", "Zmiana hasła zakończona pomyślnie", TaskDialogButton.Retry, TaskDialogIcon.SecuritySuccess);

            }
            else
            {
                TaskDialog.Show("Upewnij się, że w obu polach wpisane jest to samo nowe hasło. Zwróć uwagę na wielkość liter.", "Błąd zmiany hasła", "Podane hasła nie są identyczne", TaskDialogButton.Retry, TaskDialogIcon.Stop);
            }
        }

        private void ChangePasswordPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ChangeAvatarLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ChangeAvatarPanel.Visible = true;
        }

        private void ChangeAvatarBtn_Click(object sender, EventArgs e)
        {
            NewAvatarDialog.ShowDialog();
            NewAvatarDialog.Filter = "Obraz w formacie JPG (*.jpg)|*.jpg|Obraz w formacie PNG (*.png)|*.png";
            NewAvatarDialog.Title = "Wybierz obraz dla konta użytkownika";

            if (NewAvatarDialog.ShowDialog() == DialogResult.OK)
            {
                string polozenie = NewAvatarDialog.FileName;
                AvatarPath.Text = polozenie;
               
                if (NewAvatar.Image.Width <= 64 && NewAvatar.Image.Height <= 64)
                {
                    NewAvatar.ImageLocation = NewAvatarDialog.FileName;//.Replace("\\", "\\\\");
                }
                else MessageBox.Show("Dopuszczalny maksymalny rozmiar obrazka to 64 x 64 piksele", "Błąd! Obrazek jest za duży", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void SaveNewAvatar_Click(object sender, EventArgs e)
        {
            FileStream strumien = new FileStream(AvatarPath.Text, FileMode.Open, FileAccess.Read);
            BinaryReader czytbajt = new BinaryReader(strumien);
            byte[] zdjbajt = null;
            zdjbajt = czytbajt.ReadBytes((int)strumien.Length);
            string query = "UPDATE `users` SET avatar=@IMG WHERE uid=" + login.uid;
            MySqlConnection link = new MySqlConnection(login.danepolaczenia);
            MySqlCommand zapiszcmd = new MySqlCommand(query, link);
            MySqlDataReader czytnikSQL;
            try
            {
                link.Open();
                zapiszcmd.Parameters.Add(new MySqlParameter("@IMG", zdjbajt));
                czytnikSQL = zapiszcmd.ExecuteReader();
                while (czytnikSQL.Read()) { }
                MessageBox.Show("Pomyślnie zmieniono obrazek dla Twojego konta użytkownika.");
            }
            catch (MySqlException sqlexc)
            {
                TaskDialog.Show("Skontaktuj się z administratorem", "Błąd operacji bazodanowych", "Wystąpił błąd bazy danych\n" + sqlexc.Message, TaskDialogButton.OK, TaskDialogIcon.SecurityError);
            }
            ChangeAvatarPanel.Visible = false;
        }

        private void CancelAvatarChange_Click(object sender, EventArgs e)
        {
            ChangeAvatarPanel.Visible = false;
        }


        
    }
}
