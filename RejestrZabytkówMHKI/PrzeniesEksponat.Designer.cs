﻿namespace RejestrZabytkówMHKI
{
    partial class PrzeniesEksponat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CatNameCombo = new System.Windows.Forms.ComboBox();
            this.CurrCatLabel = new System.Windows.Forms.Label();
            this.CurrentCategory = new System.Windows.Forms.Label();
            this.NewCatLabel = new System.Windows.Forms.Label();
            this.SaveModBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CatNameCombo
            // 
            this.CatNameCombo.FormattingEnabled = true;
            this.CatNameCombo.Location = new System.Drawing.Point(42, 146);
            this.CatNameCombo.Name = "CatNameCombo";
            this.CatNameCombo.Size = new System.Drawing.Size(185, 21);
            this.CatNameCombo.TabIndex = 0;
            this.CatNameCombo.SelectedIndexChanged += new System.EventHandler(this.CatNameCombo_SelectedIndexChanged);
            // 
            // CurrCatLabel
            // 
            this.CurrCatLabel.AutoSize = true;
            this.CurrCatLabel.Location = new System.Drawing.Point(48, 47);
            this.CurrCatLabel.Name = "CurrCatLabel";
            this.CurrCatLabel.Size = new System.Drawing.Size(148, 13);
            this.CurrCatLabel.TabIndex = 1;
            this.CurrCatLabel.Text = "Bieżąca kategoria eksponatu:";
            // 
            // CurrentCategory
            // 
            this.CurrentCategory.AutoSize = true;
            this.CurrentCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CurrentCategory.Location = new System.Drawing.Point(134, 79);
            this.CurrentCategory.Name = "CurrentCategory";
            this.CurrentCategory.Size = new System.Drawing.Size(0, 20);
            this.CurrentCategory.TabIndex = 2;
            // 
            // NewCatLabel
            // 
            this.NewCatLabel.AutoSize = true;
            this.NewCatLabel.Location = new System.Drawing.Point(12, 119);
            this.NewCatLabel.Name = "NewCatLabel";
            this.NewCatLabel.Size = new System.Drawing.Size(271, 13);
            this.NewCatLabel.TabIndex = 3;
            this.NewCatLabel.Text = "Wybierz kategorię, do której chcesz przenieść eksponat";
            // 
            // SaveModBtn
            // 
            this.SaveModBtn.Location = new System.Drawing.Point(175, 217);
            this.SaveModBtn.Name = "SaveModBtn";
            this.SaveModBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveModBtn.TabIndex = 4;
            this.SaveModBtn.Text = "Zapisz";
            this.SaveModBtn.UseVisualStyleBackColor = true;
            this.SaveModBtn.Click += new System.EventHandler(this.SaveModBtn_Click);
            // 
            // PrzeniesEksponat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.SaveModBtn);
            this.Controls.Add(this.NewCatLabel);
            this.Controls.Add(this.CurrentCategory);
            this.Controls.Add(this.CurrCatLabel);
            this.Controls.Add(this.CatNameCombo);
            this.Name = "PrzeniesEksponat";
            this.Text = "PrzeniesEksponat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CatNameCombo;
        private System.Windows.Forms.Label CurrCatLabel;
        private System.Windows.Forms.Label CurrentCategory;
        private System.Windows.Forms.Label NewCatLabel;
        private System.Windows.Forms.Button SaveModBtn;
    }
}