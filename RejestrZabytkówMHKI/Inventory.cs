﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Emgu.CV;
using ZXing;
using ZXing.Common;
using ZXing.QrCode.Internal;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using EmguCVDemo;
using MySql.Data;
using System.Collections;
using WindowsFormsAero.TaskDialog;
//using Fluent;
namespace RejestrZabytkówMHKI
{
    public partial class Inventory : Form
    {
        private string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();
        private string id,wojewodztwo,stan,status;
        private DateTime datadzis,datazwrotu;
        private bool batchscan = false;
        Capture capture;
        bool Capturing;
        string[] status_kol = {"identyfikator","wypozyczony","magazyn","ekspo_st","eksp_cz","miasto","wojewodztwo","gmina","powiat","komu","data_wyp","data_zwr" };
        string[] defaddress = {"Katowice","Katowice","Katowice","śląskie" };
        private readonly IBarcodeReaderImage reader;
        private List<string> listaeksponatow = new List<string>();
        public Inventory()
        {
            InitializeComponent();
            SaveBtn.Enabled = false;
            AdresGroupBox.Visible = false;
            StanCombo.Enabled = false;
            datadzis = DateTime.Today;
            reader = new BarcodeReaderImage();
        

            
        }
        private string querydata() 
        {
            string zapytanie = "SELECT eksponaty.nazwa,eksponaty.identyfikator as ID,status,miasto,wojewodztwo,gmina,powiat,komu,data_start,data_end FROM eksponaty,stan,adres WHERE eksponaty.identyfikator=stan.identyfikator AND adres.identyfikator=stan.identyfikator AND stan.identyfikator='" + id + "'";
            return zapytanie;
        }

        private void DoBatchDecoding(object sender, EventArgs args) 
        {
            var timerStart = DateTime.Now.Ticks;

            var image = capture.QueryFrame();
            if (image != null)
            {
                using (image)
                {
                    // show it
                    CapturePreview.Image = image.ToBitmap();
                    // decode it
                    var result = reader.Decode(image);
                    


                    // show result
                    if (result != null)
                    {
                        Console.WriteLine(result.Text);
                        WynikTextBox.Text = result.Text;
                        id = result.Text.Split(new String[] { "\\" }, StringSplitOptions.RemoveEmptyEntries).Last();
                        listaeksponatow.Add(id);
                    }
                }
            }
        
        }

        private void DoDecoding(object sender, EventArgs args)
        {
            var timerStart = DateTime.Now.Ticks;

            var image = capture.QueryFrame();
            if (image != null)
            {
                using (image)
                {
                    // show it
                    CapturePreview.Image = image.ToBitmap();
                    // decode it
                    var result = reader.Decode(image);



                    // show result
                    if (result != null)
                    {
                        WynikTextBox.Text= result.Text;
                        id = result.Text.Split(new String[] { "\\" }, StringSplitOptions.RemoveEmptyEntries).Last();
                       
                        try
                        {
                            MySqlConnection link = new MySqlConnection(danepolaczenia);
                            MySqlCommand cmd = new MySqlCommand(querydata(), link);
                            DataTable DT = new DataTable();
                            MySqlDataAdapter DA = new MySqlDataAdapter(cmd);
                            
                            DA.Fill(DT);
                            StanCombo.Enabled = false;
                            NazwaEksponatu.Text = DT.Rows[0]["nazwa"].ToString();
                            status = DT.Rows[0]["status"].ToString();
                            StanCombo.SelectedItem = status;
                            switch (status) 
                            {
                                case "Wypożyczony": 
                                {
                                    
                                    AdresGroupBox.Visible = true;
                                    WypKomu.Enabled = MiastoWyp.Enabled = GminaWyp.Enabled = PowiatWyp.Enabled = WojewodztwoCombo.Enabled= false;
                                    if (!String.IsNullOrEmpty(DT.Rows[0]["komu"].ToString())) WypKomu.Text = DT.Rows[0]["komu"].ToString();
                                    else WypKomu.Text = "";
                                    if (!String.IsNullOrEmpty(DT.Rows[0]["miasto"].ToString())) MiastoWyp.Text = DT.Rows[0]["miasto"].ToString();
                                    else MiastoWyp.Text = "";
                                    if (!String.IsNullOrEmpty(DT.Rows[0]["gmina"].ToString())) GminaWyp.Text = DT.Rows[0]["gmina"].ToString();
                                    else GminaWyp.Text = "";
                                    if (!String.IsNullOrEmpty(DT.Rows[0]["powiat"].ToString())) PowiatWyp.Text = DT.Rows[0]["powiat"].ToString();
                                    else PowiatWyp.Text = "";
                                    if (!String.IsNullOrEmpty(DT.Rows[0]["wojewodztwo"].ToString())) WojewodztwoCombo.SelectedItem = DT.Rows[0]["wojewodztwo"].ToString();
                                    else WojewodztwoCombo.SelectedItem = "";
                                    DataZwrotu.Visible =DataZwrotuLabel.Visible= false;
                                    if (!String.IsNullOrEmpty(DT.Rows[0]["data_zwr"].ToString()))
                                    DataZwrotuLabel.Text +=": "+ DT.Rows[0]["data_zwr"].ToString();
                                }
                                break;
                                case "W magazynie":
                                {                            
                                    
                                    AdresGroupBox.Text = "Adres magazynu";
                                    AdresGroupBox.Visible = true;
                                    WypKomu.Visible = WypKomu_Label.Visible = false;
                                    MiastoWyp.Text = defaddress[0];
                                    GminaWyp.Text = defaddress[1];
                                    PowiatWyp.Text = defaddress[2];
                                    WojewodztwoCombo.SelectedItem = defaddress[3];
                                    DataZwrotu.Visible = DataZwrotuLabel.Visible = false;
                                }
                                break;
                                case "Ekspozycja stała": 
                                {
                                    
                                    AdresGroupBox.Text = "Muzeum Historii Komputerów i Informatyki";
                                    AdresGroupBox.Visible = true;
                                    WypKomu.Visible = WypKomu_Label.Visible = false;
                                    MiastoWyp.Text = defaddress[0];
                                    GminaWyp.Text = defaddress[1];
                                    PowiatWyp.Text = defaddress[2];
                                    WojewodztwoCombo.SelectedItem = defaddress[3];
                                    DataZwrotu.Visible = DataZwrotuLabel.Visible = false;
                                
                                }
                                break;
                                case "Ekspozycja czasowa":
                                {
                                    
                                    AdresGroupBox.Visible = true;
                                    AdresGroupBox.Text = "Ekspozycja czasowa";
                                    WypKomu.Visible = WypKomu_Label.Visible = true;
                                    DataZwrotu.Visible = DataZwrotuLabel.Visible = true;
                                    //string[] adres = defaddress;
                                }
                                break;
                                case "W serwisie": 
                                {
                                    
                                    
                                }
                                break;    
                        

                        }
                            }
                        catch (MySqlException sqlex) 
                        {
                            MessageBox.Show(sqlex.Message);
                        }//Console.WriteLine(id);
                        //txtTypeWebCam.Text = result.BarcodeFormat.ToString();
                    }
                }
            }
            var timerStop = DateTime.Now.Ticks;
            //labDuration.Text = new TimeSpan(timerStop - timerStart).Milliseconds.ToString("0 ms");
        }
        private void ScanQRBtn_Click(object sender, EventArgs e)
        {
            if (capture == null)
            {
                try
                {
                    capture = new Capture();
                }
                catch (TypeInitializationException ex)
                {
                    MessageBox.Show("Nie można skanować eksponatów. Nie wykryto podłączonej kamery.\n"+ex.Message);
                }
                
            }
            //Console.WriteLine("Wartość zm. Capturing przed: " + Capturing);
            if (capture != null)
            {
                if (Capturing)
                {
                    ScanQRBtn.Text = "Rozpocznij skanowanie";
                    SaveBtn.Enabled = true;
                    ScanQRBtn.Enabled = false;
                    StanCombo.Enabled = true;
                    WypKomu.Enabled = MiastoWyp.Enabled = GminaWyp.Enabled = PowiatWyp.Enabled = WojewodztwoCombo.Enabled = true;
                    //StanCombo.Visible = true;
                    if (batchscan)
                    {
                        Application.Idle -= DoBatchDecoding;
                        string[] explist = listaeksponatow.ToArray();
                        for (int i = 0; i < (int)explist.Length; i++) 
                        {
                            Console.WriteLine(explist[i]);
                        }
                    }
                    else Application.Idle -= DoDecoding;
                }
                else
                {
                    ScanQRBtn.Text = "Zatrzymaj skanowanie";
                    SaveBtn.Enabled = false;
                    StanCombo.Enabled = false;
                   // StanCombo.Visible = false;
                    if (batchscan)
                    {
                        Application.Idle += DoBatchDecoding;
                        trackBar1.Enabled = false;
                    }
                    else Application.Idle += DoDecoding;
                }
                Capturing = !Capturing;
                //Console.WriteLine("Wartość zm. Capturing po: " + Capturing);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            if (capture != null)
                capture.Dispose();
        }

        private void WynikLabel_Click(object sender, EventArgs e)
        {

        }

        private void StanCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            stan=StanCombo.SelectedItem.ToString();
            switch (stan) 
            {
                case "Wypożyczony": 
                    { 
                        
                        AdresGroupBox.Visible = true;
                        
                    }
                        break;
                case "W magazynie": 
                    { 
                        
                        StanCombo.SelectedItem = "W magazynie";
                        AdresGroupBox.Text = "Adres magazynu";
                        AdresGroupBox.Visible = true;
                        WypKomu.Visible = WypKomu_Label.Visible = false;
                        MiastoWyp.Text = defaddress[0];
                        GminaWyp.Text = defaddress[1];
                        PowiatWyp.Text = defaddress[2];
                        WojewodztwoCombo.SelectedItem = defaddress[3];
                        DataZwrotu.Visible = DataZwrotuLabel.Visible = false;
                    }
                        break;
                case "Ekspozycja stała": 
                    {
                        
                        StanCombo.SelectedItem = "Ekspozycja stała";
                        AdresGroupBox.Text = "Muzeum Historii Komputerów i Informatyki";
                        AdresGroupBox.Visible = true;
                        WypKomu.Visible = WypKomu_Label.Visible = false;
                        MiastoWyp.Text = defaddress[0];
                        GminaWyp.Text = defaddress[1];
                        PowiatWyp.Text = defaddress[2];
                        WojewodztwoCombo.SelectedItem = defaddress[3];
                        DataZwrotu.Visible = DataZwrotuLabel.Visible = false;
                    } 
                        break;
                case "Ekspozycja czasowa": 
                    {
                        
                        StanCombo.SelectedItem = "Ekspozycja czasowa";
                        AdresGroupBox.Visible = true;
                        AdresGroupBox.Text = "Ekspozycja czasowa";
                        WypKomu.Visible = WypKomu_Label.Visible = true;
                        DataZwrotu.Visible = true;
                    }
                        break;
            
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            ScanQRBtn.Enabled = true;
            SaveBtn.Enabled = false;
            datazwrotu = DataZwrotu.Value.Date;
            //Console.WriteLine(datazwrotu);
            //UpdateExpStatus(ref tabstanow);
            if (String.IsNullOrEmpty(WypKomu.Text) && WypKomu.Visible == true) { TaskDialog.Show("Wypełnij pole \"Nazwa instytucji / wydarzenia\"", "Błąd! Nie można zapisać niepełnych danych eksponatu", "Wypełnij pole \"Nazwa instytucji / wydarzenia\"", TaskDialogButton.OK, TaskDialogIcon.Warning); }
            if (String.IsNullOrEmpty(MiastoWyp.Text) && AdresGroupBox.Visible == true) { TaskDialog.Show("Wypełnij pole \"Miasto\"", "Błąd! Nie można zapisać niepełnych danych eksponatu", "Wypełnij pole \"Miasto\"", TaskDialogButton.OK, TaskDialogIcon.Warning); }
            if (String.IsNullOrEmpty(PowiatWyp.Text) && AdresGroupBox.Visible == true) { TaskDialog.Show("Wypełnij pole \"Powiat\"", "Błąd! Nie można zapisać niepełnych danych eksponatu", "Wypełnij pole \"Powiat\"", TaskDialogButton.OK, TaskDialogIcon.Warning); }
            if (String.IsNullOrEmpty(GminaWyp.Text) && AdresGroupBox.Visible == true) { TaskDialog.Show("Wypełnij pole \"Gmina\"", "Błąd! Nie można zapisać niepełnych danych eksponatu", "Wypełnij pole \"Gmina\"", TaskDialogButton.OK, TaskDialogIcon.Warning); }
            if (String.IsNullOrEmpty(wojewodztwo) && AdresGroupBox.Visible == true) { TaskDialog.Show("Wybierz województwo z listy", "Błąd! Nie można zapisać niepełnych danych eksponatu", "Wybierz województwo z listy", TaskDialogButton.OK, TaskDialogIcon.Warning); }
            UpdateExpData();
        }

        private void UpdateExpData()
        {
            string updatequery;
            MySqlConnection updatelink = new MySqlConnection(login.danepolaczenia);
            MySqlCommand updatecmd = new MySqlCommand();
            updatelink.Open();
            updatecmd.Connection = updatelink;
            switch(stan)
            {
                case "Wypożyczony":
                    {
                        updatequery = "UPDATE `stan` SET `status`='Wypożyczony', `komu`='" + WypKomu.Text + "', `data_start`='" + DateTime.Today.Date + "', `data_end`='" + datazwrotu + "', `userid`=" + login.uid + " WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                        updatequery = "UPDATE `adres` SET `miasto`='"+MiastoWyp.Text+"'"+", `powiat`='"+PowiatWyp.Text+"', `gmina`='"+GminaWyp.Text+"', `wojewodztwo`='"+wojewodztwo+"' WHERE identyfikator='"+id+"'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                    };break;
                case"W magazynie":
                    {
                        updatequery = "UPDATE `stan` SET `status`='W magazynie',`komu`='" + DBNull.Value + "', `data_start`='" + DateTime.Today.Date + "', `data_end`='" + DBNull.Value + "', `userid`=" + login.uid + " WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                        updatequery = "UPDATE `adres` SET `miasto`='" + defaddress[0] + "'" + ", `powiat`='" + defaddress[1] + "', `gmina`='" + defaddress[2] + "', `wojewodztwo`='" + defaddress[3] + "' WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                    };break;
                case"Ekspozycja stała":
                    {
                        updatequery = "UPDATE `stan` SET `status`='Ekspozycja stała', `komu`='"+DBNull.Value+"', `data_start`='" + DateTime.Today.Date + "', `data_end`='"+DBNull.Value+"', `userid`=" + login.uid + " WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                        updatequery = "UPDATE `adres` SET `miasto`='" + defaddress[0] + "'" + ", `powiat`='" + defaddress[1] + "', `gmina`='" + defaddress[2] + "', `wojewodztwo`='" + defaddress[3] + "' WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                    };break;
                case"Ekspozycja czasowa":
                    {
                        updatequery = "UPDATE `stan` SET `status`='Ekspozycja czasowa', `komu`='" + WypKomu.Text + "', `data_start`='" + DateTime.Today.Date + "', `data_end`='" + datazwrotu + "', `userid`=" + login.uid + " WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                        updatequery = "UPDATE `adres` SET `miasto`='" + MiastoWyp.Text + "'" + ", `powiat`='" + PowiatWyp.Text + "', `gmina`='" + GminaWyp.Text + "', `wojewodztwo`='" + wojewodztwo + "' WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                    };break;
                case "W serwisie": 
                    {
                        updatequery = "UPDATE `stan` SET `status`='W serwisie', `komu`='"+DBNull.Value+"', `data_start`='" + DateTime.Today.Date + "', `data_end`='" + DBNull.Value + "', `userid`=" + login.uid + " WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                        updatequery = "UPDATE `adres` SET `miasto`='" + defaddress[0] + "'" + ", `powiat`='" + defaddress[1] + "', `gmina`='" + defaddress[2] + "', `wojewodztwo`='" + defaddress[3] + "' WHERE identyfikator='" + id + "'";
                        updatecmd.CommandText = updatequery;
                        updatecmd.ExecuteNonQuery();
                    }; break;
            }
            updatelink.Close();
        }

        private void BatchUpdate() { }
        
        private void WojewodztwoCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            wojewodztwo = WojewodztwoCombo.SelectedItem.ToString();
        }

        private void DataZwrotu_ValueChanged(object sender, EventArgs e)
        {
            datazwrotu = DataZwrotu.Value.Date;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (trackBar1.Value == 0) 
            {
                batchscan = false;
                MessageBox.Show("Tryb inwentaryzacji jednostkowej");
            }
            else 
            { 
                batchscan = true;
                MessageBox.Show("Tryb inwentaryzacji wsadowej");
            }
        }

        private void StopBatch_Click(object sender, EventArgs e)
        {
            ScanQRBtn.Enabled = true;
            SaveBtn.Enabled = true;
            AdresGroupBox.Visible = true;
            trackBar1.Enabled = true;
        }
    }
}
