﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Security.Cryptography;
using System.Threading;
using System.Runtime.InteropServices;
using WindowsFormsAero.TaskDialog;
namespace RejestrZabytkówMHKI
{
    public partial class login : Form
    {
        string login_string,username,password,hashed_passwd;
        int test;
        public bool zalogowany;
        public static string nazwauzytkownika,imie_nazw,grupa;
        public static int uid;
        /*{
            get
            {
                return userid;
            }
            
        }*/
        public static string danepolaczenia = System.Configuration.ConfigurationManager.ConnectionStrings["danepolaczenia"].ToString();
        

        public void SplashScreen()
        {
            Application.Run(new SplashScreen());
        }
        public login()
        {
            Thread watek = new Thread(new ThreadStart(SplashScreen));
            watek.Start();
            Thread.Sleep(5000);
            InitializeComponent();
            watek.Abort();
            //zalogowany = false;
        }

        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            
            /*
            PasswordConnectionInfo info = new PasswordConnectionInfo("155.158.108.213", "mhki", "mhki");
            info.Timeout = TimeSpan.FromHours(2d);
            SshClient klient = new SshClient(info);
            try
            {
                klient.Connect();
            }
            catch (SshException ssh_error)
            {
                MessageBox.Show(ssh_error.Message + "\n" + ssh_error.Data);
            }

            if (!klient.IsConnected)
            {
                MessageBox.Show("Nie można nawiązać połączenia SSH");
            }
            var portFwd = new ForwardedPortLocal("127.0.0.1", 10000, "localhost", 3306);
            klient.AddForwardedPort(portFwd);
            portFwd.Start();
            if (portFwd.IsStarted)
            {
                //MessageBox.Show("Tunel SSH aktywny");
            }
            else
            {
                MessageBox.Show("Nie udało się otworzyć tunelu SSH");
            }*/
            MySqlConnection link = new MySqlConnection(danepolaczenia);
            //MySqlCommand polecenie = new MySqlCommand("set net_write_timeout=99999; set net_read_timeout=99999; set names 'cp1250'", link);
            
            username = user.Text;
            password = passwd.Text;
            hashed_passwd = CalculateMD5Hash(password);
            login_string = "SELECT COUNT(uid) FROM users WHERE nazwa='" + username + "' AND haslo='" + hashed_passwd + "'";
            MySqlCommand polecenie = new MySqlCommand(login_string, link);
            
            try
            {
                link.Open();
                test = Convert.ToInt32(polecenie.ExecuteScalar().ToString());
                link.Close();
            }
            catch (MySqlException mysql_error)
            {
                MessageBox.Show(mysql_error.ToString());
            }
            //portFwd.Stop();
            //klient.Disconnect();
            if (test == 1)
            {
                zalogowany = true;
                
                nazwauzytkownika = username;
                string logintime = "UPDATE `users` SET lastlogin='" + DateTime.Now + "' WHERE nazwa='" + username + "'";
                try
                {
                    MySqlConnection updatelink = new MySqlConnection(danepolaczenia);
                    MySqlCommand updatecmd = new MySqlCommand(logintime,updatelink);
                    updatelink.Open();
                    updatecmd.ExecuteNonQuery();
                    updatelink.Close();
                }
                catch (MySqlException mysqlexc) 
                {
                    TaskDialog.Show("Przy aktualizacji danych użytkownika wystąpił błąd bazy danych", "Treść błędu:\n" + mysqlexc.Message, "Błąd bazy danych", TaskDialogButton.OK, TaskDialogIcon.Stop);
                }
                Console.WriteLine(logintime);
                try 
                {
                    string getuserdata = "SELECT uid,imienazw,grupa FROM users WHERE nazwa='" + username + "'";
                    MySqlConnection danelink = new MySqlConnection(danepolaczenia);
                    MySqlCommand danecmd = new MySqlCommand(getuserdata,danelink);
                    MySqlDataAdapter daneDA = new MySqlDataAdapter(danecmd);
                    DataTable daneDT = new DataTable();
                    daneDA.Fill(daneDT);
                    uid = Convert.ToInt16(daneDT.Rows[0]["uid"]);
                    imie_nazw = daneDT.Rows[0]["imienazw"].ToString();
                    grupa = daneDT.Rows[0]["grupa"].ToString();
                    Console.WriteLine(uid+"\n"+ imie_nazw+"\n"+ grupa);
                    
                }
                catch (MySqlException mysqlexc) 
                {
                    TaskDialog.Show("Wystąpił błąd przy pobieraniu danych użytkownika z bazy danych.", "Treść błędu:\n"+mysqlexc.Message, "Błąd bazy danych!", TaskDialogButton.OK, TaskDialogIcon.SecurityError);
                    
                }
                this.Close();
            }
            else 
            {
                TaskDialog.Show("Podano nieprawidłowe dane logowania","Spróbuj ponownie.", "Błąd danych logowania!", TaskDialogButton.OK,TaskDialogIcon.SecurityError );
                zalogowany = false;
                
            }

            
        }

        private void login_Load(object sender, EventArgs e)
        {
            
        }
    }
}
