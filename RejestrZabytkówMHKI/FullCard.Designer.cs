﻿namespace RejestrZabytkówMHKI
{
    partial class FullCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FullCard));
            this.MainTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.strona1 = new System.Windows.Forms.TableLayoutPanel();
            this.MatGraficzneGroupBox = new System.Windows.Forms.GroupBox();
            this.MatGrafPictBox = new System.Windows.Forms.PictureBox();
            this.str1kontener1do7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.CzasPowstGrpBox = new System.Windows.Forms.GroupBox();
            this.CzasPowstLabel = new System.Windows.Forms.Label();
            this.MatPodstGrpBox = new System.Windows.Forms.GroupBox();
            this.MatPodstLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.CiezarGrpBox = new System.Windows.Forms.GroupBox();
            this.CiezarLabel = new System.Windows.Forms.Label();
            this.IloscGrpBox = new System.Windows.Forms.GroupBox();
            this.IloscLabel = new System.Windows.Forms.Label();
            this.WymiaryGrpBox = new System.Windows.Forms.GroupBox();
            this.dimTxtBox = new System.Windows.Forms.TextBox();
            this.DaneFrmGrpBox = new System.Windows.Forms.GroupBox();
            this.DaneFirmoweLabel = new System.Windows.Forms.Label();
            this.NazwaGrpBox = new System.Windows.Forms.GroupBox();
            this.NazwaLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.kontener9do10 = new System.Windows.Forms.TableLayoutPanel();
            this.AdresGrpBox = new System.Windows.Forms.GroupBox();
            this.AdresTextBox = new System.Windows.Forms.TextBox();
            this.WlascicielGrpBox = new System.Windows.Forms.GroupBox();
            this.WlascicielTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.UzytkGrpBox = new System.Windows.Forms.GroupBox();
            this.UzytkownikTextBox = new System.Windows.Forms.TextBox();
            this.MscePracyGrpBox = new System.Windows.Forms.GroupBox();
            this.MscePracyTextBox = new System.Windows.Forms.TextBox();
            this.UdostGrpBox = new System.Windows.Forms.GroupBox();
            this.UdostepnianieLabel = new System.Windows.Forms.Label();
            this.FrmOchrGrpBox = new System.Windows.Forms.GroupBox();
            this.OchronaLabel = new System.Windows.Forms.Label();
            this.str3tablica = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.PrzeznPierwGrpBox = new System.Windows.Forms.GroupBox();
            this.PrzeznTextBox = new System.Windows.Forms.TextBox();
            this.uzgrpbox = new System.Windows.Forms.GroupBox();
            this.UzytkowanieTextBox = new System.Windows.Forms.TextBox();
            this.remontyGrpBox = new System.Windows.Forms.GroupBox();
            this.RemontyTextBox = new System.Windows.Forms.TextBox();
            this.stanGrpBox = new System.Windows.Forms.GroupBox();
            this.StanTextBox = new System.Windows.Forms.TextBox();
            this.str4tablica = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.AktaGrpBox = new System.Windows.Forms.GroupBox();
            this.AktaTextBox = new System.Windows.Forms.TextBox();
            this.bibgrGrpBox = new System.Windows.Forms.GroupBox();
            this.BibliografiaTextBox = new System.Windows.Forms.TextBox();
            this.ikoGrpBox = new System.Windows.Forms.GroupBox();
            this.ZrodlaIkoTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.uwagiGrpBox = new System.Windows.Forms.GroupBox();
            this.UwagiTextBox = new System.Windows.Forms.TextBox();
            this.adnGrpBox = new System.Windows.Forms.GroupBox();
            this.AdnotacjaTextBox = new System.Windows.Forms.TextBox();
            this.opracGrpBox = new System.Windows.Forms.GroupBox();
            this.OpracowanieTextBox = new System.Windows.Forms.TextBox();
            this.zalGrpBox = new System.Windows.Forms.GroupBox();
            this.ZalacznikiTextBox = new System.Windows.Forms.TextBox();
            this.str2tablica = new System.Windows.Forms.TableLayoutPanel();
            this.histGrpBox = new System.Windows.Forms.GroupBox();
            this.HistoriaTextBox = new System.Windows.Forms.TextBox();
            this.opisGrpBox = new System.Windows.Forms.GroupBox();
            this.opisTextBox = new System.Windows.Forms.TextBox();
            this.PDFExportBtn = new System.Windows.Forms.Button();
            this.XLSExportBtn = new System.Windows.Forms.Button();
            this.ExportPDFDialog = new System.Windows.Forms.SaveFileDialog();
            this.ExportXLSDialog = new System.Windows.Forms.SaveFileDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.PrintPreviewBtn = new System.Windows.Forms.Button();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MainTablePanel.SuspendLayout();
            this.strona1.SuspendLayout();
            this.MatGraficzneGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MatGrafPictBox)).BeginInit();
            this.str1kontener1do7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.CzasPowstGrpBox.SuspendLayout();
            this.MatPodstGrpBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.CiezarGrpBox.SuspendLayout();
            this.IloscGrpBox.SuspendLayout();
            this.WymiaryGrpBox.SuspendLayout();
            this.DaneFrmGrpBox.SuspendLayout();
            this.NazwaGrpBox.SuspendLayout();
            this.kontener9do10.SuspendLayout();
            this.AdresGrpBox.SuspendLayout();
            this.WlascicielGrpBox.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.UzytkGrpBox.SuspendLayout();
            this.MscePracyGrpBox.SuspendLayout();
            this.UdostGrpBox.SuspendLayout();
            this.FrmOchrGrpBox.SuspendLayout();
            this.str3tablica.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.PrzeznPierwGrpBox.SuspendLayout();
            this.uzgrpbox.SuspendLayout();
            this.remontyGrpBox.SuspendLayout();
            this.stanGrpBox.SuspendLayout();
            this.str4tablica.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.AktaGrpBox.SuspendLayout();
            this.bibgrGrpBox.SuspendLayout();
            this.ikoGrpBox.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.uwagiGrpBox.SuspendLayout();
            this.adnGrpBox.SuspendLayout();
            this.opracGrpBox.SuspendLayout();
            this.zalGrpBox.SuspendLayout();
            this.str2tablica.SuspendLayout();
            this.histGrpBox.SuspendLayout();
            this.opisGrpBox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTablePanel
            // 
            this.MainTablePanel.BackColor = System.Drawing.Color.White;
            this.MainTablePanel.ColumnCount = 1;
            this.MainTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainTablePanel.Controls.Add(this.strona1, 0, 0);
            this.MainTablePanel.Location = new System.Drawing.Point(0, 0);
            this.MainTablePanel.Name = "MainTablePanel";
            this.MainTablePanel.RowCount = 1;
            this.MainTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 97.94344F));
            this.MainTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.056555F));
            this.MainTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 618F));
            this.MainTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 618F));
            this.MainTablePanel.Size = new System.Drawing.Size(951, 618);
            this.MainTablePanel.TabIndex = 0;
            // 
            // strona1
            // 
            this.strona1.ColumnCount = 2;
            this.strona1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.81707F));
            this.strona1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.18293F));
            this.strona1.Controls.Add(this.MatGraficzneGroupBox, 0, 1);
            this.strona1.Controls.Add(this.str1kontener1do7, 0, 0);
            this.strona1.Controls.Add(this.kontener9do10, 1, 0);
            this.strona1.Controls.Add(this.tableLayoutPanel6, 1, 1);
            this.strona1.Location = new System.Drawing.Point(0, 0);
            this.strona1.Margin = new System.Windows.Forms.Padding(0);
            this.strona1.Name = "strona1";
            this.strona1.RowCount = 2;
            this.strona1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.strona1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.strona1.Size = new System.Drawing.Size(951, 618);
            this.strona1.TabIndex = 0;
            // 
            // MatGraficzneGroupBox
            // 
            this.MatGraficzneGroupBox.AutoSize = true;
            this.MatGraficzneGroupBox.BackColor = System.Drawing.Color.White;
            this.MatGraficzneGroupBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MatGraficzneGroupBox.Controls.Add(this.MatGrafPictBox);
            this.MatGraficzneGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MatGraficzneGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.MatGraficzneGroupBox.Location = new System.Drawing.Point(5, 309);
            this.MatGraficzneGroupBox.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.MatGraficzneGroupBox.Name = "MatGraficzneGroupBox";
            this.MatGraficzneGroupBox.Size = new System.Drawing.Size(658, 309);
            this.MatGraficzneGroupBox.TabIndex = 0;
            this.MatGraficzneGroupBox.TabStop = false;
            this.MatGraficzneGroupBox.Text = "8. Materiały graficzne";
            // 
            // MatGrafPictBox
            // 
            this.MatGrafPictBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MatGrafPictBox.Location = new System.Drawing.Point(3, 16);
            this.MatGrafPictBox.Margin = new System.Windows.Forms.Padding(10);
            this.MatGrafPictBox.Name = "MatGrafPictBox";
            this.MatGrafPictBox.Size = new System.Drawing.Size(652, 290);
            this.MatGrafPictBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MatGrafPictBox.TabIndex = 0;
            this.MatGrafPictBox.TabStop = false;
            // 
            // str1kontener1do7
            // 
            this.str1kontener1do7.ColumnCount = 1;
            this.str1kontener1do7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.str1kontener1do7.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.str1kontener1do7.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.str1kontener1do7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.str1kontener1do7.Location = new System.Drawing.Point(0, 0);
            this.str1kontener1do7.Margin = new System.Windows.Forms.Padding(0);
            this.str1kontener1do7.Name = "str1kontener1do7";
            this.str1kontener1do7.RowCount = 2;
            this.str1kontener1do7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.27273F));
            this.str1kontener1do7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.72727F));
            this.str1kontener1do7.Size = new System.Drawing.Size(663, 309);
            this.str1kontener1do7.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.DaneFrmGrpBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.NazwaGrpBox, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 84);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(663, 225);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.CzasPowstGrpBox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.MatPodstGrpBox, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(331, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(332, 112);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // CzasPowstGrpBox
            // 
            this.CzasPowstGrpBox.AutoSize = true;
            this.CzasPowstGrpBox.Controls.Add(this.CzasPowstLabel);
            this.CzasPowstGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CzasPowstGrpBox.Location = new System.Drawing.Point(0, 0);
            this.CzasPowstGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.CzasPowstGrpBox.Name = "CzasPowstGrpBox";
            this.CzasPowstGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.CzasPowstGrpBox.Size = new System.Drawing.Size(332, 56);
            this.CzasPowstGrpBox.TabIndex = 0;
            this.CzasPowstGrpBox.TabStop = false;
            this.CzasPowstGrpBox.Text = "2. Czas powstania";
            this.CzasPowstGrpBox.Enter += new System.EventHandler(this.CzasPowstGrpBox_Enter);
            // 
            // CzasPowstLabel
            // 
            this.CzasPowstLabel.AutoSize = true;
            this.CzasPowstLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CzasPowstLabel.Location = new System.Drawing.Point(9, 21);
            this.CzasPowstLabel.Name = "CzasPowstLabel";
            this.CzasPowstLabel.Size = new System.Drawing.Size(0, 18);
            this.CzasPowstLabel.TabIndex = 0;
            // 
            // MatPodstGrpBox
            // 
            this.MatPodstGrpBox.AutoSize = true;
            this.MatPodstGrpBox.Controls.Add(this.MatPodstLabel);
            this.MatPodstGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MatPodstGrpBox.Location = new System.Drawing.Point(0, 56);
            this.MatPodstGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.MatPodstGrpBox.Name = "MatPodstGrpBox";
            this.MatPodstGrpBox.Size = new System.Drawing.Size(332, 56);
            this.MatPodstGrpBox.TabIndex = 1;
            this.MatPodstGrpBox.TabStop = false;
            this.MatPodstGrpBox.Text = "3. Materiał podstawowy";
            // 
            // MatPodstLabel
            // 
            this.MatPodstLabel.AutoSize = true;
            this.MatPodstLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MatPodstLabel.Location = new System.Drawing.Point(9, 25);
            this.MatPodstLabel.Name = "MatPodstLabel";
            this.MatPodstLabel.Size = new System.Drawing.Size(0, 18);
            this.MatPodstLabel.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.WymiaryGrpBox, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(331, 112);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(332, 113);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.CiezarGrpBox, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.IloscGrpBox, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(166, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(166, 113);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // CiezarGrpBox
            // 
            this.CiezarGrpBox.AutoSize = true;
            this.CiezarGrpBox.Controls.Add(this.CiezarLabel);
            this.CiezarGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CiezarGrpBox.Location = new System.Drawing.Point(0, 0);
            this.CiezarGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.CiezarGrpBox.Name = "CiezarGrpBox";
            this.CiezarGrpBox.Size = new System.Drawing.Size(166, 56);
            this.CiezarGrpBox.TabIndex = 0;
            this.CiezarGrpBox.TabStop = false;
            this.CiezarGrpBox.Text = "6. Ciężar";
            // 
            // CiezarLabel
            // 
            this.CiezarLabel.AutoSize = true;
            this.CiezarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CiezarLabel.Location = new System.Drawing.Point(10, 23);
            this.CiezarLabel.Name = "CiezarLabel";
            this.CiezarLabel.Size = new System.Drawing.Size(0, 18);
            this.CiezarLabel.TabIndex = 0;
            // 
            // IloscGrpBox
            // 
            this.IloscGrpBox.AutoSize = true;
            this.IloscGrpBox.Controls.Add(this.IloscLabel);
            this.IloscGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IloscGrpBox.Location = new System.Drawing.Point(0, 56);
            this.IloscGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.IloscGrpBox.Name = "IloscGrpBox";
            this.IloscGrpBox.Size = new System.Drawing.Size(166, 57);
            this.IloscGrpBox.TabIndex = 1;
            this.IloscGrpBox.TabStop = false;
            this.IloscGrpBox.Text = "7. Ilość";
            // 
            // IloscLabel
            // 
            this.IloscLabel.AutoSize = true;
            this.IloscLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IloscLabel.Location = new System.Drawing.Point(10, 23);
            this.IloscLabel.Name = "IloscLabel";
            this.IloscLabel.Size = new System.Drawing.Size(0, 18);
            this.IloscLabel.TabIndex = 0;
            // 
            // WymiaryGrpBox
            // 
            this.WymiaryGrpBox.AutoSize = true;
            this.WymiaryGrpBox.Controls.Add(this.dimTxtBox);
            this.WymiaryGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WymiaryGrpBox.Location = new System.Drawing.Point(0, 0);
            this.WymiaryGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.WymiaryGrpBox.Name = "WymiaryGrpBox";
            this.WymiaryGrpBox.Size = new System.Drawing.Size(166, 113);
            this.WymiaryGrpBox.TabIndex = 1;
            this.WymiaryGrpBox.TabStop = false;
            this.WymiaryGrpBox.Text = "5. Wymiary";
            // 
            // dimTxtBox
            // 
            this.dimTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dimTxtBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dimTxtBox.Location = new System.Drawing.Point(3, 16);
            this.dimTxtBox.Multiline = true;
            this.dimTxtBox.Name = "dimTxtBox";
            this.dimTxtBox.Size = new System.Drawing.Size(160, 94);
            this.dimTxtBox.TabIndex = 0;
            // 
            // DaneFrmGrpBox
            // 
            this.DaneFrmGrpBox.AutoSize = true;
            this.DaneFrmGrpBox.Controls.Add(this.DaneFirmoweLabel);
            this.DaneFrmGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DaneFrmGrpBox.Location = new System.Drawing.Point(5, 112);
            this.DaneFrmGrpBox.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.DaneFrmGrpBox.Name = "DaneFrmGrpBox";
            this.DaneFrmGrpBox.Size = new System.Drawing.Size(326, 113);
            this.DaneFrmGrpBox.TabIndex = 2;
            this.DaneFrmGrpBox.TabStop = false;
            this.DaneFrmGrpBox.Text = "4. Dane firmowe";
            this.DaneFrmGrpBox.Enter += new System.EventHandler(this.DaneFrmGrpBox_Enter);
            // 
            // DaneFirmoweLabel
            // 
            this.DaneFirmoweLabel.AutoSize = true;
            this.DaneFirmoweLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DaneFirmoweLabel.Location = new System.Drawing.Point(8, 23);
            this.DaneFirmoweLabel.Name = "DaneFirmoweLabel";
            this.DaneFirmoweLabel.Size = new System.Drawing.Size(0, 18);
            this.DaneFirmoweLabel.TabIndex = 0;
            // 
            // NazwaGrpBox
            // 
            this.NazwaGrpBox.AutoSize = true;
            this.NazwaGrpBox.Controls.Add(this.NazwaLabel);
            this.NazwaGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NazwaGrpBox.Location = new System.Drawing.Point(5, 0);
            this.NazwaGrpBox.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.NazwaGrpBox.Name = "NazwaGrpBox";
            this.NazwaGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.NazwaGrpBox.Size = new System.Drawing.Size(326, 112);
            this.NazwaGrpBox.TabIndex = 3;
            this.NazwaGrpBox.TabStop = false;
            this.NazwaGrpBox.Text = "1. Nazwa";
            // 
            // NazwaLabel
            // 
            this.NazwaLabel.AutoSize = true;
            this.NazwaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NazwaLabel.Location = new System.Drawing.Point(8, 24);
            this.NazwaLabel.Name = "NazwaLabel";
            this.NazwaLabel.Size = new System.Drawing.Size(0, 18);
            this.NazwaLabel.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.91176F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.08823F));
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(663, 84);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // kontener9do10
            // 
            this.kontener9do10.ColumnCount = 1;
            this.kontener9do10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.kontener9do10.Controls.Add(this.AdresGrpBox, 0, 0);
            this.kontener9do10.Controls.Add(this.WlascicielGrpBox, 0, 1);
            this.kontener9do10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kontener9do10.Location = new System.Drawing.Point(663, 0);
            this.kontener9do10.Margin = new System.Windows.Forms.Padding(0);
            this.kontener9do10.Name = "kontener9do10";
            this.kontener9do10.RowCount = 2;
            this.kontener9do10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.kontener9do10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.kontener9do10.Size = new System.Drawing.Size(288, 309);
            this.kontener9do10.TabIndex = 2;
            // 
            // AdresGrpBox
            // 
            this.AdresGrpBox.Controls.Add(this.AdresTextBox);
            this.AdresGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdresGrpBox.Location = new System.Drawing.Point(0, 0);
            this.AdresGrpBox.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.AdresGrpBox.Name = "AdresGrpBox";
            this.AdresGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.AdresGrpBox.Size = new System.Drawing.Size(283, 154);
            this.AdresGrpBox.TabIndex = 0;
            this.AdresGrpBox.TabStop = false;
            this.AdresGrpBox.Text = "9. Adres";
            // 
            // AdresTextBox
            // 
            this.AdresTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AdresTextBox.Location = new System.Drawing.Point(8, 17);
            this.AdresTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.AdresTextBox.Multiline = true;
            this.AdresTextBox.Name = "AdresTextBox";
            this.AdresTextBox.Size = new System.Drawing.Size(272, 131);
            this.AdresTextBox.TabIndex = 0;
            // 
            // WlascicielGrpBox
            // 
            this.WlascicielGrpBox.Controls.Add(this.WlascicielTextBox);
            this.WlascicielGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WlascicielGrpBox.Location = new System.Drawing.Point(0, 154);
            this.WlascicielGrpBox.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.WlascicielGrpBox.Name = "WlascicielGrpBox";
            this.WlascicielGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.WlascicielGrpBox.Size = new System.Drawing.Size(283, 155);
            this.WlascicielGrpBox.TabIndex = 1;
            this.WlascicielGrpBox.TabStop = false;
            this.WlascicielGrpBox.Text = "10. Właściciel i jego adres";
            // 
            // WlascicielTextBox
            // 
            this.WlascicielTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.WlascicielTextBox.Location = new System.Drawing.Point(8, 16);
            this.WlascicielTextBox.Multiline = true;
            this.WlascicielTextBox.Name = "WlascicielTextBox";
            this.WlascicielTextBox.Size = new System.Drawing.Size(272, 136);
            this.WlascicielTextBox.TabIndex = 0;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.UzytkGrpBox, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.MscePracyGrpBox, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.UdostGrpBox, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.FrmOchrGrpBox, 0, 3);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(663, 309);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(288, 309);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // UzytkGrpBox
            // 
            this.UzytkGrpBox.Controls.Add(this.UzytkownikTextBox);
            this.UzytkGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UzytkGrpBox.Location = new System.Drawing.Point(0, 0);
            this.UzytkGrpBox.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.UzytkGrpBox.Name = "UzytkGrpBox";
            this.UzytkGrpBox.Size = new System.Drawing.Size(283, 112);
            this.UzytkGrpBox.TabIndex = 0;
            this.UzytkGrpBox.TabStop = false;
            this.UzytkGrpBox.Text = "11. Użytkownik i jego adres";
            // 
            // UzytkownikTextBox
            // 
            this.UzytkownikTextBox.BackColor = System.Drawing.Color.White;
            this.UzytkownikTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UzytkownikTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UzytkownikTextBox.Location = new System.Drawing.Point(8, 16);
            this.UzytkownikTextBox.Multiline = true;
            this.UzytkownikTextBox.Name = "UzytkownikTextBox";
            this.UzytkownikTextBox.ReadOnly = true;
            this.UzytkownikTextBox.Size = new System.Drawing.Size(272, 88);
            this.UzytkownikTextBox.TabIndex = 0;
            // 
            // MscePracyGrpBox
            // 
            this.MscePracyGrpBox.Controls.Add(this.MscePracyTextBox);
            this.MscePracyGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MscePracyGrpBox.Location = new System.Drawing.Point(0, 112);
            this.MscePracyGrpBox.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.MscePracyGrpBox.Name = "MscePracyGrpBox";
            this.MscePracyGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.MscePracyGrpBox.Size = new System.Drawing.Size(283, 112);
            this.MscePracyGrpBox.TabIndex = 1;
            this.MscePracyGrpBox.TabStop = false;
            this.MscePracyGrpBox.Text = "12. Miejsce pracy (przechowywania)";
            // 
            // MscePracyTextBox
            // 
            this.MscePracyTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MscePracyTextBox.Location = new System.Drawing.Point(3, 17);
            this.MscePracyTextBox.Multiline = true;
            this.MscePracyTextBox.Name = "MscePracyTextBox";
            this.MscePracyTextBox.Size = new System.Drawing.Size(277, 87);
            this.MscePracyTextBox.TabIndex = 0;
            // 
            // UdostGrpBox
            // 
            this.UdostGrpBox.Controls.Add(this.UdostepnianieLabel);
            this.UdostGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UdostGrpBox.Location = new System.Drawing.Point(0, 224);
            this.UdostGrpBox.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.UdostGrpBox.Name = "UdostGrpBox";
            this.UdostGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.UdostGrpBox.Size = new System.Drawing.Size(283, 40);
            this.UdostGrpBox.TabIndex = 2;
            this.UdostGrpBox.TabStop = false;
            this.UdostGrpBox.Text = "13. Udostępnianie";
            this.UdostGrpBox.Enter += new System.EventHandler(this.UdostGrpBox_Enter);
            // 
            // UdostepnianieLabel
            // 
            this.UdostepnianieLabel.AutoSize = true;
            this.UdostepnianieLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UdostepnianieLabel.Location = new System.Drawing.Point(10, 17);
            this.UdostepnianieLabel.Name = "UdostepnianieLabel";
            this.UdostepnianieLabel.Size = new System.Drawing.Size(0, 18);
            this.UdostepnianieLabel.TabIndex = 0;
            // 
            // FrmOchrGrpBox
            // 
            this.FrmOchrGrpBox.Controls.Add(this.OchronaLabel);
            this.FrmOchrGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrmOchrGrpBox.Location = new System.Drawing.Point(0, 264);
            this.FrmOchrGrpBox.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.FrmOchrGrpBox.Name = "FrmOchrGrpBox";
            this.FrmOchrGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.FrmOchrGrpBox.Size = new System.Drawing.Size(283, 45);
            this.FrmOchrGrpBox.TabIndex = 3;
            this.FrmOchrGrpBox.TabStop = false;
            this.FrmOchrGrpBox.Text = "14. Formy ochrony";
            // 
            // OchronaLabel
            // 
            this.OchronaLabel.AutoSize = true;
            this.OchronaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.OchronaLabel.Location = new System.Drawing.Point(8, 20);
            this.OchronaLabel.Name = "OchronaLabel";
            this.OchronaLabel.Size = new System.Drawing.Size(0, 18);
            this.OchronaLabel.TabIndex = 0;
            // 
            // str3tablica
            // 
            this.str3tablica.ColumnCount = 2;
            this.str3tablica.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.str3tablica.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.str3tablica.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.str3tablica.Controls.Add(this.stanGrpBox, 1, 0);
            this.str3tablica.Location = new System.Drawing.Point(this.str2tablica.Location.X, this.str2tablica.Location.Y+this.str2tablica.Width);
            this.str3tablica.Margin = new System.Windows.Forms.Padding(0);
            this.str3tablica.Name = "str3tablica";
            this.str3tablica.RowCount = 1;
            this.str3tablica.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.str3tablica.Size = this.MainTablePanel.Size;
            this.str3tablica.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.remontyGrpBox, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.5F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.5F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(475, 618);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.03313F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.96687F));
            this.tableLayoutPanel8.Controls.Add(this.PrzeznPierwGrpBox, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.uzgrpbox, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(475, 114);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // PrzeznPierwGrpBox
            // 
            this.PrzeznPierwGrpBox.Controls.Add(this.PrzeznTextBox);
            this.PrzeznPierwGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrzeznPierwGrpBox.Location = new System.Drawing.Point(0, 0);
            this.PrzeznPierwGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.PrzeznPierwGrpBox.Name = "PrzeznPierwGrpBox";
            this.PrzeznPierwGrpBox.Size = new System.Drawing.Size(228, 114);
            this.PrzeznPierwGrpBox.TabIndex = 0;
            this.PrzeznPierwGrpBox.TabStop = false;
            this.PrzeznPierwGrpBox.Text = "17. Przeznaczenie pierwotne";
            // 
            // PrzeznTextBox
            // 
            this.PrzeznTextBox.BackColor = System.Drawing.Color.White;
            this.PrzeznTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PrzeznTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrzeznTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PrzeznTextBox.Location = new System.Drawing.Point(3, 16);
            this.PrzeznTextBox.Multiline = true;
            this.PrzeznTextBox.Name = "PrzeznTextBox";
            this.PrzeznTextBox.ReadOnly = true;
            this.PrzeznTextBox.ShortcutsEnabled = false;
            this.PrzeznTextBox.Size = new System.Drawing.Size(222, 95);
            this.PrzeznTextBox.TabIndex = 0;
            // 
            // uzgrpbox
            // 
            this.uzgrpbox.Controls.Add(this.UzytkowanieTextBox);
            this.uzgrpbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uzgrpbox.Location = new System.Drawing.Point(228, 0);
            this.uzgrpbox.Margin = new System.Windows.Forms.Padding(0);
            this.uzgrpbox.Name = "uzgrpbox";
            this.uzgrpbox.Size = new System.Drawing.Size(247, 114);
            this.uzgrpbox.TabIndex = 1;
            this.uzgrpbox.TabStop = false;
            this.uzgrpbox.Text = "18. Użytkowanie obecne";
            // 
            // UzytkowanieTextBox
            // 
            this.UzytkowanieTextBox.BackColor = System.Drawing.Color.White;
            this.UzytkowanieTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UzytkowanieTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UzytkowanieTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UzytkowanieTextBox.Location = new System.Drawing.Point(3, 16);
            this.UzytkowanieTextBox.Multiline = true;
            this.UzytkowanieTextBox.Name = "UzytkowanieTextBox";
            this.UzytkowanieTextBox.ReadOnly = true;
            this.UzytkowanieTextBox.Size = new System.Drawing.Size(241, 95);
            this.UzytkowanieTextBox.TabIndex = 0;
            // 
            // remontyGrpBox
            // 
            this.remontyGrpBox.Controls.Add(this.RemontyTextBox);
            this.remontyGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.remontyGrpBox.Location = new System.Drawing.Point(3, 117);
            this.remontyGrpBox.Name = "remontyGrpBox";
            this.remontyGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.remontyGrpBox.Size = new System.Drawing.Size(469, 498);
            this.remontyGrpBox.TabIndex = 1;
            this.remontyGrpBox.TabStop = false;
            this.remontyGrpBox.Text = "19. Remonty, zmiany konstrukcyjne, modernizacje";
            // 
            // RemontyTextBox
            // 
            this.RemontyTextBox.BackColor = System.Drawing.Color.White;
            this.RemontyTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemontyTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemontyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RemontyTextBox.Location = new System.Drawing.Point(0, 13);
            this.RemontyTextBox.Multiline = true;
            this.RemontyTextBox.Name = "RemontyTextBox";
            this.RemontyTextBox.ReadOnly = true;
            this.RemontyTextBox.Size = new System.Drawing.Size(469, 485);
            this.RemontyTextBox.TabIndex = 0;
            // 
            // stanGrpBox
            // 
            this.stanGrpBox.Controls.Add(this.StanTextBox);
            this.stanGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stanGrpBox.Location = new System.Drawing.Point(475, 0);
            this.stanGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.stanGrpBox.Name = "stanGrpBox";
            this.stanGrpBox.Size = new System.Drawing.Size(476, 618);
            this.stanGrpBox.TabIndex = 1;
            this.stanGrpBox.TabStop = false;
            this.stanGrpBox.Text = "20. Stan zachowania i potrzeby konserwatorskie";
            // 
            // StanTextBox
            // 
            this.StanTextBox.BackColor = System.Drawing.Color.White;
            this.StanTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StanTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StanTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.StanTextBox.Location = new System.Drawing.Point(3, 16);
            this.StanTextBox.Multiline = true;
            this.StanTextBox.Name = "StanTextBox";
            this.StanTextBox.ReadOnly = true;
            this.StanTextBox.Size = new System.Drawing.Size(470, 599);
            this.StanTextBox.TabIndex = 0;
            // 
            // str4tablica
            // 
            this.str4tablica.ColumnCount = 2;
            this.str4tablica.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.str4tablica.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.str4tablica.Controls.Add(this.tableLayoutPanel9, 0, 0);
            this.str4tablica.Controls.Add(this.tableLayoutPanel10, 1, 0);
            this.str4tablica.Location = new System.Drawing.Point(0, 1661);
            this.str4tablica.Margin = new System.Windows.Forms.Padding(0);
            this.str4tablica.Name = "str4tablica";
            this.str4tablica.RowCount = 1;
            this.str4tablica.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.str4tablica.Size = this.MainTablePanel.Size;
            this.str4tablica.TabIndex = 0;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.AktaGrpBox, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.bibgrGrpBox, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.ikoGrpBox, 0, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(475, 618);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // AktaGrpBox
            // 
            this.AktaGrpBox.Controls.Add(this.AktaTextBox);
            this.AktaGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AktaGrpBox.Location = new System.Drawing.Point(0, 0);
            this.AktaGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.AktaGrpBox.Name = "AktaGrpBox";
            this.AktaGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.AktaGrpBox.Size = new System.Drawing.Size(475, 203);
            this.AktaGrpBox.TabIndex = 0;
            this.AktaGrpBox.TabStop = false;
            this.AktaGrpBox.Text = "21. Akta archiwalne (rodzaj akt, numer i miejsce przechowywania)";
            // 
            // AktaTextBox
            // 
            this.AktaTextBox.BackColor = System.Drawing.Color.White;
            this.AktaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AktaTextBox.Location = new System.Drawing.Point(8, 17);
            this.AktaTextBox.Multiline = true;
            this.AktaTextBox.Name = "AktaTextBox";
            this.AktaTextBox.ReadOnly = true;
            this.AktaTextBox.Size = new System.Drawing.Size(460, 180);
            this.AktaTextBox.TabIndex = 0;
            // 
            // bibgrGrpBox
            // 
            this.bibgrGrpBox.Controls.Add(this.BibliografiaTextBox);
            this.bibgrGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bibgrGrpBox.Location = new System.Drawing.Point(0, 203);
            this.bibgrGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.bibgrGrpBox.Name = "bibgrGrpBox";
            this.bibgrGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.bibgrGrpBox.Size = new System.Drawing.Size(475, 203);
            this.bibgrGrpBox.TabIndex = 1;
            this.bibgrGrpBox.TabStop = false;
            this.bibgrGrpBox.Text = "22. Bibliografia";
            // 
            // BibliografiaTextBox
            // 
            this.BibliografiaTextBox.BackColor = System.Drawing.Color.White;
            this.BibliografiaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BibliografiaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BibliografiaTextBox.Location = new System.Drawing.Point(8, 17);
            this.BibliografiaTextBox.Multiline = true;
            this.BibliografiaTextBox.Name = "BibliografiaTextBox";
            this.BibliografiaTextBox.ReadOnly = true;
            this.BibliografiaTextBox.Size = new System.Drawing.Size(460, 180);
            this.BibliografiaTextBox.TabIndex = 0;
            // 
            // ikoGrpBox
            // 
            this.ikoGrpBox.Controls.Add(this.ZrodlaIkoTextBox);
            this.ikoGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ikoGrpBox.Location = new System.Drawing.Point(0, 406);
            this.ikoGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.ikoGrpBox.Name = "ikoGrpBox";
            this.ikoGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.ikoGrpBox.Size = new System.Drawing.Size(475, 212);
            this.ikoGrpBox.TabIndex = 2;
            this.ikoGrpBox.TabStop = false;
            this.ikoGrpBox.Text = "23. Źródła ikonograficzne (rodzaj, miejsce przechowywania)";
            // 
            // ZrodlaIkoTextBox
            // 
            this.ZrodlaIkoTextBox.BackColor = System.Drawing.Color.White;
            this.ZrodlaIkoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ZrodlaIkoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ZrodlaIkoTextBox.Location = new System.Drawing.Point(8, 17);
            this.ZrodlaIkoTextBox.Multiline = true;
            this.ZrodlaIkoTextBox.Name = "ZrodlaIkoTextBox";
            this.ZrodlaIkoTextBox.ReadOnly = true;
            this.ZrodlaIkoTextBox.Size = new System.Drawing.Size(460, 190);
            this.ZrodlaIkoTextBox.TabIndex = 0;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.uwagiGrpBox, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.adnGrpBox, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.opracGrpBox, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.zalGrpBox, 0, 3);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(475, 0);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 4;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.34582F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.65418F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 153F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 99F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(476, 618);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // uwagiGrpBox
            // 
            this.uwagiGrpBox.Controls.Add(this.UwagiTextBox);
            this.uwagiGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uwagiGrpBox.Location = new System.Drawing.Point(0, 0);
            this.uwagiGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.uwagiGrpBox.Name = "uwagiGrpBox";
            this.uwagiGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.uwagiGrpBox.Size = new System.Drawing.Size(476, 147);
            this.uwagiGrpBox.TabIndex = 0;
            this.uwagiGrpBox.TabStop = false;
            this.uwagiGrpBox.Text = "24. Uwagi";
            // 
            // UwagiTextBox
            // 
            this.UwagiTextBox.BackColor = System.Drawing.Color.White;
            this.UwagiTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UwagiTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UwagiTextBox.Location = new System.Drawing.Point(8, 17);
            this.UwagiTextBox.Multiline = true;
            this.UwagiTextBox.Name = "UwagiTextBox";
            this.UwagiTextBox.ReadOnly = true;
            this.UwagiTextBox.Size = new System.Drawing.Size(460, 120);
            this.UwagiTextBox.TabIndex = 0;
            // 
            // adnGrpBox
            // 
            this.adnGrpBox.Controls.Add(this.AdnotacjaTextBox);
            this.adnGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adnGrpBox.Location = new System.Drawing.Point(0, 147);
            this.adnGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.adnGrpBox.Name = "adnGrpBox";
            this.adnGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.adnGrpBox.Size = new System.Drawing.Size(476, 218);
            this.adnGrpBox.TabIndex = 1;
            this.adnGrpBox.TabStop = false;
            this.adnGrpBox.Text = "25. Adnotacja o inspekcjach, informacje o zmianach (daty, imiona i nazwiska wypeł" +
    "niających)";
            // 
            // AdnotacjaTextBox
            // 
            this.AdnotacjaTextBox.BackColor = System.Drawing.Color.White;
            this.AdnotacjaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AdnotacjaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AdnotacjaTextBox.Location = new System.Drawing.Point(8, 17);
            this.AdnotacjaTextBox.Multiline = true;
            this.AdnotacjaTextBox.Name = "AdnotacjaTextBox";
            this.AdnotacjaTextBox.ReadOnly = true;
            this.AdnotacjaTextBox.Size = new System.Drawing.Size(460, 190);
            this.AdnotacjaTextBox.TabIndex = 0;
            // 
            // opracGrpBox
            // 
            this.opracGrpBox.Controls.Add(this.OpracowanieTextBox);
            this.opracGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opracGrpBox.Location = new System.Drawing.Point(0, 365);
            this.opracGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.opracGrpBox.Name = "opracGrpBox";
            this.opracGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.opracGrpBox.Size = new System.Drawing.Size(476, 153);
            this.opracGrpBox.TabIndex = 2;
            this.opracGrpBox.TabStop = false;
            this.opracGrpBox.Text = "26. Opracowanie karty ewidencyjnej (autor, data i podpis)";
            // 
            // OpracowanieTextBox
            // 
            this.OpracowanieTextBox.BackColor = System.Drawing.Color.White;
            this.OpracowanieTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.OpracowanieTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.OpracowanieTextBox.Location = new System.Drawing.Point(8, 17);
            this.OpracowanieTextBox.Multiline = true;
            this.OpracowanieTextBox.Name = "OpracowanieTextBox";
            this.OpracowanieTextBox.ReadOnly = true;
            this.OpracowanieTextBox.Size = new System.Drawing.Size(460, 130);
            this.OpracowanieTextBox.TabIndex = 0;
            // 
            // zalGrpBox
            // 
            this.zalGrpBox.Controls.Add(this.ZalacznikiTextBox);
            this.zalGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zalGrpBox.Location = new System.Drawing.Point(0, 518);
            this.zalGrpBox.Margin = new System.Windows.Forms.Padding(0);
            this.zalGrpBox.Name = "zalGrpBox";
            this.zalGrpBox.Padding = new System.Windows.Forms.Padding(0);
            this.zalGrpBox.Size = new System.Drawing.Size(476, 100);
            this.zalGrpBox.TabIndex = 3;
            this.zalGrpBox.TabStop = false;
            this.zalGrpBox.Text = "27. Załączniki";
            // 
            // ZalacznikiTextBox
            // 
            this.ZalacznikiTextBox.BackColor = System.Drawing.Color.White;
            this.ZalacznikiTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ZalacznikiTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ZalacznikiTextBox.Location = new System.Drawing.Point(8, 17);
            this.ZalacznikiTextBox.Multiline = true;
            this.ZalacznikiTextBox.Name = "ZalacznikiTextBox";
            this.ZalacznikiTextBox.ReadOnly = true;
            this.ZalacznikiTextBox.Size = new System.Drawing.Size(460, 80);
            this.ZalacznikiTextBox.TabIndex = 0;
            // 
            // str2tablica
            // 
            this.str2tablica.ColumnCount = 2;
            this.str2tablica.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39F));
            this.str2tablica.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61F));
            this.str2tablica.Controls.Add(this.histGrpBox, 0, 0);
            this.str2tablica.Controls.Add(this.opisGrpBox, 1, 0);
            this.str2tablica.Location = new System.Drawing.Point(0, 623);
            this.str2tablica.Margin = new System.Windows.Forms.Padding(0);
            this.str2tablica.Name = "str2tablica";
            this.str2tablica.RowCount = 1;
            this.str2tablica.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.str2tablica.Size = this.MainTablePanel.Size;
            this.str2tablica.TabIndex = 0;
            // 
            // histGrpBox
            // 
            this.histGrpBox.Controls.Add(this.HistoriaTextBox);
            this.histGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.histGrpBox.Location = new System.Drawing.Point(3, 3);
            this.histGrpBox.Name = "histGrpBox";
            this.histGrpBox.Size = new System.Drawing.Size(364, 612);
            this.histGrpBox.TabIndex = 0;
            this.histGrpBox.TabStop = false;
            this.histGrpBox.Text = "15. Historia obiektu";
            // 
            // HistoriaTextBox
            // 
            this.HistoriaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.HistoriaTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HistoriaTextBox.Location = new System.Drawing.Point(3, 16);
            this.HistoriaTextBox.Multiline = true;
            this.HistoriaTextBox.Name = "HistoriaTextBox";
            this.HistoriaTextBox.Size = new System.Drawing.Size(358, 593);
            this.HistoriaTextBox.TabIndex = 0;
            // 
            // opisGrpBox
            // 
            this.opisGrpBox.Controls.Add(this.opisTextBox);
            this.opisGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opisGrpBox.Location = new System.Drawing.Point(373, 3);
            this.opisGrpBox.Name = "opisGrpBox";
            this.opisGrpBox.Size = new System.Drawing.Size(575, 612);
            this.opisGrpBox.TabIndex = 1;
            this.opisGrpBox.TabStop = false;
            this.opisGrpBox.Text = "16. Opis i charakterystyka techniczna";
            // 
            // opisTextBox
            // 
            this.opisTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.opisTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opisTextBox.Location = new System.Drawing.Point(3, 16);
            this.opisTextBox.Multiline = true;
            this.opisTextBox.Name = "opisTextBox";
            this.opisTextBox.Size = new System.Drawing.Size(569, 593);
            this.opisTextBox.TabIndex = 0;
            // 
            // PDFExportBtn
            // 
            this.PDFExportBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.PDFExportBtn.Location = new System.Drawing.Point(864, 16);
            this.PDFExportBtn.Name = "PDFExportBtn";
            this.PDFExportBtn.Size = new System.Drawing.Size(112, 23);
            this.PDFExportBtn.TabIndex = 2;
            this.PDFExportBtn.Text = "Eksportuj do PDF";
            this.PDFExportBtn.UseVisualStyleBackColor = true;
            this.PDFExportBtn.Click += new System.EventHandler(this.PDFExportBtn_Click);
            // 
            // XLSExportBtn
            // 
            this.XLSExportBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.XLSExportBtn.Location = new System.Drawing.Point(744, 16);
            this.XLSExportBtn.Name = "XLSExportBtn";
            this.XLSExportBtn.Size = new System.Drawing.Size(104, 23);
            this.XLSExportBtn.TabIndex = 3;
            this.XLSExportBtn.Text = "Eksportuj do XLS";
            this.XLSExportBtn.UseVisualStyleBackColor = true;
            this.XLSExportBtn.Click += new System.EventHandler(this.XLSExportBtn_Click);
            // 
            // ExportPDFDialog
            // 
            this.ExportPDFDialog.DefaultExt = "pdf";
            this.ExportPDFDialog.Filter = "Dokument PDF|*.pdf";
            this.ExportPDFDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ExportPDFDialog_FileOk);
            // 
            // ExportXLSDialog
            // 
            this.ExportXLSDialog.DefaultExt = "xlsx";
            this.ExportXLSDialog.Filter = "Arkusz kalkulacyjny MS Excel 2007-2013|*.xlsx";
            this.ExportXLSDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ExportXLSDialog_FileOk);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // PrintPreviewBtn
            // 
            this.PrintPreviewBtn.Location = new System.Drawing.Point(512, 16);
            this.PrintPreviewBtn.Name = "PrintPreviewBtn";
            this.PrintPreviewBtn.Size = new System.Drawing.Size(75, 23);
            this.PrintPreviewBtn.TabIndex = 4;
            this.PrintPreviewBtn.Text = "Podgląd wydruku";
            this.PrintPreviewBtn.UseVisualStyleBackColor = true;
            // 
            // PrintBtn
            // 
            this.PrintBtn.Location = new System.Drawing.Point(408, 16);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(75, 23);
            this.PrintBtn.TabIndex = 5;
            this.PrintBtn.Text = "Drukuj";
            this.PrintBtn.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.str4tablica);
            this.panel1.Controls.Add(this.str3tablica);
            this.panel1.Controls.Add(this.str2tablica);
            this.panel1.Controls.Add(this.MainTablePanel);
            this.panel1.Location = new System.Drawing.Point(0, 128);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(968, 568);
            this.panel1.TabIndex = 6;
            // 
            // FullCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 709);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PrintBtn);
            this.Controls.Add(this.PrintPreviewBtn);
            this.Controls.Add(this.XLSExportBtn);
            this.Controls.Add(this.PDFExportBtn);
            this.Name = "FullCard";
            this.Text = "FullCard";
            this.Load += new System.EventHandler(this.FullCard_Load);
            this.MainTablePanel.ResumeLayout(false);
            this.strona1.ResumeLayout(false);
            this.strona1.PerformLayout();
            this.MatGraficzneGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MatGrafPictBox)).EndInit();
            this.str1kontener1do7.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.CzasPowstGrpBox.ResumeLayout(false);
            this.CzasPowstGrpBox.PerformLayout();
            this.MatPodstGrpBox.ResumeLayout(false);
            this.MatPodstGrpBox.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.CiezarGrpBox.ResumeLayout(false);
            this.CiezarGrpBox.PerformLayout();
            this.IloscGrpBox.ResumeLayout(false);
            this.IloscGrpBox.PerformLayout();
            this.WymiaryGrpBox.ResumeLayout(false);
            this.WymiaryGrpBox.PerformLayout();
            this.DaneFrmGrpBox.ResumeLayout(false);
            this.DaneFrmGrpBox.PerformLayout();
            this.NazwaGrpBox.ResumeLayout(false);
            this.NazwaGrpBox.PerformLayout();
            this.kontener9do10.ResumeLayout(false);
            this.AdresGrpBox.ResumeLayout(false);
            this.AdresGrpBox.PerformLayout();
            this.WlascicielGrpBox.ResumeLayout(false);
            this.WlascicielGrpBox.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.UzytkGrpBox.ResumeLayout(false);
            this.UzytkGrpBox.PerformLayout();
            this.MscePracyGrpBox.ResumeLayout(false);
            this.MscePracyGrpBox.PerformLayout();
            this.UdostGrpBox.ResumeLayout(false);
            this.UdostGrpBox.PerformLayout();
            this.FrmOchrGrpBox.ResumeLayout(false);
            this.FrmOchrGrpBox.PerformLayout();
            this.str3tablica.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.PrzeznPierwGrpBox.ResumeLayout(false);
            this.PrzeznPierwGrpBox.PerformLayout();
            this.uzgrpbox.ResumeLayout(false);
            this.uzgrpbox.PerformLayout();
            this.remontyGrpBox.ResumeLayout(false);
            this.remontyGrpBox.PerformLayout();
            this.stanGrpBox.ResumeLayout(false);
            this.stanGrpBox.PerformLayout();
            this.str4tablica.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.AktaGrpBox.ResumeLayout(false);
            this.AktaGrpBox.PerformLayout();
            this.bibgrGrpBox.ResumeLayout(false);
            this.bibgrGrpBox.PerformLayout();
            this.ikoGrpBox.ResumeLayout(false);
            this.ikoGrpBox.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.uwagiGrpBox.ResumeLayout(false);
            this.uwagiGrpBox.PerformLayout();
            this.adnGrpBox.ResumeLayout(false);
            this.adnGrpBox.PerformLayout();
            this.opracGrpBox.ResumeLayout(false);
            this.opracGrpBox.PerformLayout();
            this.zalGrpBox.ResumeLayout(false);
            this.zalGrpBox.PerformLayout();
            this.str2tablica.ResumeLayout(false);
            this.histGrpBox.ResumeLayout(false);
            this.histGrpBox.PerformLayout();
            this.opisGrpBox.ResumeLayout(false);
            this.opisGrpBox.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MainTablePanel;
        private System.Windows.Forms.TableLayoutPanel strona1;
        private System.Windows.Forms.GroupBox MatGraficzneGroupBox;
        private System.Windows.Forms.TableLayoutPanel str1kontener1do7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.GroupBox CzasPowstGrpBox;
        private System.Windows.Forms.GroupBox MatPodstGrpBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox CiezarGrpBox;
        private System.Windows.Forms.GroupBox IloscGrpBox;
        private System.Windows.Forms.GroupBox WymiaryGrpBox;
        private System.Windows.Forms.GroupBox DaneFrmGrpBox;
        private System.Windows.Forms.GroupBox NazwaGrpBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label DaneFirmoweLabel;
        private System.Windows.Forms.Label NazwaLabel;
        private System.Windows.Forms.TableLayoutPanel kontener9do10;
        private System.Windows.Forms.GroupBox AdresGrpBox;
        private System.Windows.Forms.GroupBox WlascicielGrpBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.GroupBox UzytkGrpBox;
        private System.Windows.Forms.GroupBox MscePracyGrpBox;
        private System.Windows.Forms.GroupBox UdostGrpBox;
        private System.Windows.Forms.GroupBox FrmOchrGrpBox;
        private System.Windows.Forms.PictureBox MatGrafPictBox;
        private System.Windows.Forms.TableLayoutPanel str2tablica;
        private System.Windows.Forms.GroupBox histGrpBox;
        private System.Windows.Forms.TextBox HistoriaTextBox;
        private System.Windows.Forms.GroupBox opisGrpBox;
        private System.Windows.Forms.TextBox opisTextBox;
        private System.Windows.Forms.TableLayoutPanel str3tablica;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.GroupBox PrzeznPierwGrpBox;
        private System.Windows.Forms.GroupBox uzgrpbox;
        private System.Windows.Forms.GroupBox remontyGrpBox;
        private System.Windows.Forms.GroupBox stanGrpBox;
        private System.Windows.Forms.TableLayoutPanel str4tablica;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.GroupBox AktaGrpBox;
        private System.Windows.Forms.GroupBox bibgrGrpBox;
        private System.Windows.Forms.GroupBox ikoGrpBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.GroupBox uwagiGrpBox;
        private System.Windows.Forms.GroupBox adnGrpBox;
        private System.Windows.Forms.GroupBox opracGrpBox;
        private System.Windows.Forms.GroupBox zalGrpBox;
        private System.Windows.Forms.Label MatPodstLabel;
        private System.Windows.Forms.TextBox dimTxtBox;
        private System.Windows.Forms.Label CzasPowstLabel;
        private System.Windows.Forms.Label CiezarLabel;
        private System.Windows.Forms.Label IloscLabel;
        private System.Windows.Forms.TextBox AdresTextBox;
        private System.Windows.Forms.TextBox WlascicielTextBox;
        private System.Windows.Forms.TextBox MscePracyTextBox;
        private System.Windows.Forms.Label UdostepnianieLabel;
        private System.Windows.Forms.Label OchronaLabel;
        private System.Windows.Forms.TextBox PrzeznTextBox;
        private System.Windows.Forms.TextBox UzytkowanieTextBox;
        private System.Windows.Forms.TextBox StanTextBox;
        private System.Windows.Forms.TextBox AktaTextBox;
        private System.Windows.Forms.TextBox BibliografiaTextBox;
        private System.Windows.Forms.TextBox ZrodlaIkoTextBox;
        private System.Windows.Forms.TextBox UwagiTextBox;
        private System.Windows.Forms.TextBox AdnotacjaTextBox;
        private System.Windows.Forms.TextBox OpracowanieTextBox;
        private System.Windows.Forms.TextBox ZalacznikiTextBox;
        private System.Windows.Forms.TextBox UzytkownikTextBox;
        private System.Windows.Forms.TextBox RemontyTextBox;
        private System.Windows.Forms.Button PDFExportBtn;
        private System.Windows.Forms.Button XLSExportBtn;
        private System.Windows.Forms.SaveFileDialog ExportPDFDialog;
        private System.Windows.Forms.SaveFileDialog ExportXLSDialog;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Button PrintPreviewBtn;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.Panel panel1;


    }
}